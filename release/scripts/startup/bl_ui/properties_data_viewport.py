# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>
import bpy
from bpy.types import Panel


class DataButtonsPanel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        engine = context.engine
        return context.viewport and (engine in cls.COMPAT_ENGINES)


class DATA_PT_viewport(DataButtonsPanel, Panel):
    bl_label = "Viewport"
    COMPAT_ENGINES = {'BLENDER_RENDER', 'CYCLES'}

    def draw(self, context):
        layout = self.layout

        ob = context.object
        viewport = context.viewport
        
        layout.prop(viewport, "type")
        
        if viewport.type == 'PLANE':
            pass
        
        elif viewport.type == 'SPHERE':
            layout.prop(viewport, "radius")
        


classes = (
    DATA_PT_viewport,
)

if __name__ == "__main__":  # only for live edit.
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
