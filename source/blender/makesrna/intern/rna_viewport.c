/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Contributor(s): Jeroen Bakker, Monique Dewanchand
 *
 * ***** END GPL LICENSE BLOCK *****
 */

/** \file blender/makesrna/intern/rna_viewport.c
 *  \ingroup RNA
 */

#include <stdlib.h>

#include "RNA_define.h"
#include "RNA_enum_types.h"

#include "rna_internal.h"

#include "DNA_viewport_types.h"

#include "WM_types.h"

const EnumPropertyItem rna_enum_viewport_type_items[] = {
	{VIEWPORT_TYPE_PLANE, "PLANE", ICON_NONE, "Plane", ""},
	{VIEWPORT_TYPE_SPHERE, "SPHERE", ICON_NONE, "Sphere", ""},
	{0, NULL, 0, NULL, NULL}
};

#ifdef RNA_RUNTIME

#include "MEM_guardedalloc.h"

#include "BKE_main.h"
#include "DEG_depsgraph.h"

#include "DNA_object_types.h"

#include "WM_api.h"

#else


static void rna_def_viewport(BlenderRNA *brna)
{
	StructRNA *srna;
	PropertyRNA *prop;

	srna = RNA_def_struct(brna, "Viewport", "ID");
	RNA_def_struct_ui_text(srna, "Viewport", "Viewport data-block for canvas compositing");
	RNA_def_struct_ui_icon(srna, ICON_NONE);

	prop = RNA_def_property(srna, "type", PROP_ENUM, PROP_NONE);
	RNA_def_property_enum_items(prop, rna_enum_viewport_type_items);
	RNA_def_property_ui_text(prop, "Type", "Type of viewport");

	prop = RNA_def_property(srna, "radius", PROP_FLOAT, PROP_DISTANCE);
	RNA_def_property_float_sdna(prop, NULL, "radius");
	RNA_def_property_range(prop, 0.0f, FLT_MAX);
	RNA_def_property_ui_text(prop, "Radius", "Radius of the viewport");

	/* common */
	rna_def_animdata_common(srna);
}

void RNA_def_viewport(BlenderRNA *brna)
{
	rna_def_viewport(brna);
}

#endif
