/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) Blender Foundation.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): J.Bakker & M.Dewanchand.
 *
 * ***** END GPL LICENSE BLOCK *****
 */

/** \file blender/blenkernel/intern/viewport.c
 *  \ingroup bke
 */

#include "DNA_object_types.h"
#include "DNA_viewport_types.h"

#include "BLI_math.h"
#include "BLI_utildefines.h"

#include "BKE_animsys.h"
#include "BKE_global.h"
#include "BKE_main.h"
#include "BKE_viewport.h"

void BKE_viewport_init(Viewport *viewport)
{
	BLI_assert(MEMCMP_STRUCT_OFS_IS_ZERO(viewport, id));

	viewport->type = VIEWPORT_TYPE_PLANE;
	viewport->radius = 100.0f;
}

void *BKE_viewport_add(Main *bmain, const char *name)
{
	Viewport *viewport;

	viewport =  BKE_libblock_alloc(bmain, ID_VP, name, 0);

	BKE_viewport_init(viewport);

	return viewport;
}

void BKE_viewport_copy_data(
	Main *UNUSED(bmain), Viewport *UNUSED(viewport_dst), const Viewport *UNUSED(viewport_src), const int UNUSED(flag))
{
}

Viewport *BKE_viewport_copy(Main *bmain, const Viewport *viewport)
{
	Viewport *viewport_copy;
	BKE_id_copy_ex(bmain, &viewport->id, (ID **)&viewport_copy, 0, false);
	return viewport_copy;
}

void BKE_viewport_make_local(Main *bmain, Viewport *viewport, const bool lib_local)
{
	BKE_id_make_local_generic(bmain, &viewport->id, true, lib_local);
}

void BKE_viewport_free(Viewport *viewport)
{
	BKE_animdata_free((ID *)viewport, false);
}
