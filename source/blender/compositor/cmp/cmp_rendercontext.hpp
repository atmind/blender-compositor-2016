#ifndef CMP_RENDERCONTEXT_HPP
#define CMP_RENDERCONTEXT_HPP

#include "DNA_scene_types.h"

namespace Compositor {
	struct RenderContext {
		/**
		 * The scene that started the compositor.
		 */
		Scene* scene;

		/**
		 * The name of the multiview.
		 */
		const char *view_name;

		/**
		 * The current frame that is being calculated.
		 */
		int frame_number;

		/**
		 * Are we rendering or editing
		 */
		bool is_rendering;

		/*
		* Should OpenCL be used
		*/
		bool use_open_cl;

		/*
		* Number of samples to taken into account
		*/
		int num_samples;

		/**
		 * Tile size
		 */
		int tile_size;
	};
}
#endif
