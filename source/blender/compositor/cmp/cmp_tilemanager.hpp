#ifndef CMP_TILEMANAGER_HPP
#define CMP_TILEMANAGER_HPP
#include <list>
#include "device_task.hpp"
#include "cmp_output.hpp"

namespace Compositor {
	struct TileManager {
		Output* output;
		int default_num_samples;
    int tile_size;

		TileManager(Output* output, int default_num_samples, int tile_size);

		/**
		 * Fills an empty list of tiles with tiles that needs to be calculated.
		 * list will be in order..
		 */
		void generate_tiles(std::list<Compositor::Device::ComputeTask*>& result);

		void delete_tiles(std::list<Compositor::Device::ComputeTask*>& tiles);
	};
}
#endif
