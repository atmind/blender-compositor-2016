namespace Compositor {
	struct Node;
}
#ifndef CMP_NODE_HPP
#define CMP_NODE_HPP

#include "DNA_node_types.h"
#include "cmp_nodesocket.hpp"
#include "cmp_rendercontext.hpp"
#include <vector>
#include <list>
#include <string>

#include "DNA_node_types.h"
#include "BKE_node.h"
#include "RNA_access.h"

#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "util/util_types.h"
#include "kernel/kernel_globals.h"

#include "IMB_imbuf_types.h"

namespace Compositor {

	struct Node {

		bNodeTree * node_tree;

		/**
		 * Reference to the (optional) bNode for this Node instance.
		 * This is for debugging and possible (not likely) future enhancements.
		 */
		bNode* b_node;
		bNodeSocket * b_socket;

	/**
	 * Type of the node
	 */
		int type;

		/**
		 * needed memory on the stack to store results
		 */
		int result_size;

		/**
		 * given offset on the stack to store the results
		 */
		int result_offset;

		/**
		 * needed memory on the ray_stack to store results
		 */
		int ray_size;

		/**
		 * Giiven offset by the compiler to store the results
		 */
		int ray_offset;

		/**
		 * offset on the ray stack to be used as the incoming ray
		 */
		int input_ray_offset;

		/**
		 * Index of the output socket of the node that is calculated by this node.
		 *
		 * For every (used) output a new node instance will be created.
		 */
		int output;

		/**
		 * Index of the BufferUser in the program
		 */
		int buffer_user_index;

		/**
		* Index of the viewport in the Program.
		*/
		int viewport_index;
		
		/**
		 * Has this node already been added to the program.
		 */
		int added_to_program;

		/** 
		 * The list of input sockets of this node
		 */
		std::vector<NodeSocket*> inputs;


	private:
		Node();

		/**
		 * The list of sockets that uses this node (eg. this nodes feeds data to these input sockets.)
		 */
		std::list<NodeSocket*> users;

	public:
		Node(bNodeTree* node_tree, bNode *node, bNodeSocket* socket, RenderContext * render_context);
		Node(bNodeTree* node_tree, bNodeSocket* socket, int type);
		Node(int type, Node* connected_node);

		void register_user(NodeSocket * socket);
		void unregister_user(NodeSocket * socket);
		std::list<NodeSocket*> get_users();

		/**
		 * Is this node muted?
		 */
		bool is_muted() const;
		~Node();
	};
}
#endif
