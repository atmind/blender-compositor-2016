extern "C" {
	#include "BKE_node.h"
	#include "BLI_threads.h"
}
#include "PIL_time.h"

#include "COM_compositor.h"

#include "cmp_unroll.hpp"
#include "cmp_output.hpp"
#include "cmp_rendercontext.hpp"
#include "cmp_tilemanager.hpp"
#include "device/device.hpp"
#include "device/device_opencl.hpp"
#include <iostream>
#include "cmp_compiler.hpp"


#define FREE_OBJECTS delete render_context;\
if(node != NULL){ delete node; node = NULL;}\
BLI_mutex_unlock(&s_compositorMutex);

static ThreadMutex s_compositorMutex;

extern "C" {

static Compositor::RenderContext *render_context = NULL;
static Compositor::Node* node = NULL;
static Compositor::Device::Device *device = NULL;

void COM_execute(RenderData *rd, Scene *scene, bNodeTree *editingtree, int rendering,
								 const ColorManagedViewSettings *viewSettings, const ColorManagedDisplaySettings *displaySettings,
								 const char *viewName) {

	if (editingtree->test_break(editingtree->tbh)) { return; }
	BLI_mutex_lock(&s_compositorMutex);
	double start_time = PIL_check_seconds_timer();

	// Create render context
	render_context = new Compositor::RenderContext();
	render_context->view_name = viewName;
	render_context->frame_number = rd ? rd->cfra:-1;
	render_context->is_rendering = rendering;
	render_context->scene = scene;

	// TODO: Get these settings from user settings
	render_context->num_samples = editingtree->edit_settings.samples;
	render_context->tile_size = editingtree->edit_settings.tile_size;
	if (render_context->tile_size == 0) render_context->tile_size = 256;
	render_context->use_open_cl = (editingtree->flag & NTREE_COM_OPENCL) != 0;

	// UNROLL editingtree
	node = Compositor::unroll(editingtree, render_context);
	if (node != NULL) {
		// ALLOCATE output
		Compositor::Output output(editingtree, node, rd, viewName, viewSettings, displaySettings);
		if (output.buffer == NULL) {
				FREE_OBJECTS
				return;
		}

		//Call Compiler
		Compositor::Compiler* compiler;
		compiler = new Compositor::Compiler(node, render_context);
		compiler->compile();

		//Create start and end compute tasks
		Compositor::Device::ComputeTask start_task(&output, Compositor::Device::START);
		Compositor::Device::ComputeTask end_task(&output, Compositor::Device::END);

		// Generate Tiles
		Compositor::TileManager tile_manager(&output, render_context->num_samples, render_context->tile_size);
		std::list<Compositor::Device::ComputeTask*> tiles;
		tile_manager.generate_tiles(tiles);

		// When CPU and more than 8 samples, add previes tiles with 1 sample
		// TODO: make this an UI option
		if (!render_context->use_open_cl && render_context->num_samples > 8) {
			tile_manager.default_num_samples = 1;
			tile_manager.generate_tiles(tiles);
		}

		// Schedule
		device = Compositor::Device::Device::create_device(&compiler->program, &output, render_context);
		// execute the preview task

		device->start();

		// schedule the final tasks
		device->add_task(&start_task);
		device->add_tasks(tiles);
		device->add_task(&end_task);
		device->wait();
		device->stop();

		Compositor::Device::Device::destroy_device(device);
		device = NULL;
		tile_manager.delete_tiles(tiles);
		delete compiler;
	}
	double end_time = PIL_check_seconds_timer();
	std::cout << "Compositor::Total execution time: " << (end_time - start_time) << "\n";
	FREE_OBJECTS
}

void COM_initialize(void) {
	BLI_mutex_init(&s_compositorMutex);
	Compositor::Device::DeviceOpenCL::init_opencl();
}

void COM_deinitialize(void) {
	Compositor::Device::DeviceOpenCL::deinit_opencl();
	BLI_mutex_end(&s_compositorMutex);
}
}
