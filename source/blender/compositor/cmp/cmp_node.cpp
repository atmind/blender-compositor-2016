#include "cmp_node.hpp"

#include "BKE_node.h"
#include "DNA_node_types.h"
#include "RNA_access.h"
#include <iostream>
#include "MEM_guardedalloc.h"

extern "C" {
	#include "IMB_imbuf.h"
}
#include <cassert>

namespace Compositor {

	Node::Node() {
		this->type = -1;
		this->output = 0;
		this->node_tree = NULL;
		this->b_node = NULL;
		this->b_socket = NULL;
		this->buffer_user_index = -1;
		this->viewport_index = -1;
	}
	/**
	 * This constructor is used for conversion nodes.
	 * The parameter connected_node is created by the node socket
	 * where the need for the conversion is detected.
	 */
	Node::Node(int type, Node* connected_node): Node() {
		this->type = type;
		this->inputs.push_back(new NodeSocket(this, connected_node));
	}

	Node::Node(bNodeTree* node_tree, bNodeSocket* socket, int type): Node() {
		this->type = type;
		this->node_tree = node_tree;
		this->b_socket = socket;
	}

	Node::Node(bNodeTree* node_tree, bNode *node, bNodeSocket* socket, RenderContext * render_context): Node() {
		this->node_tree = node_tree;
		this->b_node = node;
		this->b_socket = socket;
		this->type = node->type;

		for (bNodeSocket *socket = (bNodeSocket *)node->inputs.first; socket; socket = socket->next) {
			this->inputs.push_back(new NodeSocket(this, socket, render_context));
		}

		if (socket != NULL) {
			int output_index = 0;
			for (bNodeSocket *output_socket = (bNodeSocket *)node->outputs.first; output_socket; output_socket = output_socket->next) {
				if (socket == output_socket) {
					break;
				}
				output_index ++;
			}
			this->output = output_index;
		}
	}

	Node::~Node() {
		for(auto node_socket: this->inputs){
			delete node_socket;
		}

		for (auto node_socket: this->users) {
			node_socket->connected_node = NULL;
		}
	}

	bool Node::is_muted() const {
		if (this->b_node) {
			return this->b_node->flag & NODE_MUTED;
		}
		return false;
	}
	
	void Node::register_user(NodeSocket* user) {
		this->users.push_back(user);
	}
	void Node::unregister_user(NodeSocket* user) {
			this->users.remove(user);
	}
	std::list<NodeSocket*> Node::get_users() {
		return this->users;
	}
}

