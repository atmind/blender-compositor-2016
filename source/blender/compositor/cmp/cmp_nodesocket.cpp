#include "cmp_nodesocket.hpp"
#include "cmp_node.hpp"

#include "DNA_node_types.h"
#include "BKE_node.h"

#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "kernel/operations/op_types.h"

#include <algorithm>

namespace Compositor {

	NodeSocket::NodeSocket(Node* node) {
		this->node = node;
		this->b_socket = NULL;
		this->connected_node = NULL;
	}

	NodeSocket::NodeSocket(Node* node, Node* connected_node): NodeSocket(node) {
		this->set_connected_node(connected_node);
	}

	NodeSocket::NodeSocket(Node* node, bNodeSocket * socket, RenderContext * render_context): NodeSocket(node) {
		this->b_socket = socket;

		bNodeTree* node_tree = node->node_tree;
		for (bNodeLink *link = (bNodeLink*)node_tree->links.first; link; link = (bNodeLink*)link->next) {
			if (link->tosock == socket) {
				Node * connected_node = new Node(node_tree, link->fromnode, link->fromsock, render_context);

				/** Skip muted nodes */
				if (connected_node->is_muted()) {
					// use internal links to find correct input socket. and its connected_node
					bNode* b_node = connected_node->b_node;

					for (bNodeLink *b_link = (bNodeLink *)b_node->internal_links.first; b_link; b_link = b_link->next) {
						if (b_link->tosock == link->fromsock) {
							for (NodeSocket* inputsocket: connected_node->inputs) {
								if (inputsocket->b_socket == b_link->fromsock) {
									Node* prev_connected_node = connected_node;
									connected_node = inputsocket->connected_node;
									inputsocket->connected_node = NULL;
									delete prev_connected_node;
									break;
								}
							}
						}
					}
				}

				/** Check data conversion */
				int	from_type = link->fromsock->type;
				int to_type = link->tosock->type;

				if (to_type != from_type) {
					int conversion_type = OP_VALUE_TO_COLOR;
					// Data types differ, add conversion.
					if (from_type == SOCK_FLOAT && to_type == SOCK_RGBA) {
						conversion_type = OP_VALUE_TO_COLOR;

					} 
					else if (from_type == SOCK_FLOAT && to_type == SOCK_VECTOR) {
						conversion_type = OP_VALUE_TO_VECTOR;

					}
					else if (from_type == SOCK_VECTOR && to_type == SOCK_FLOAT) {
						conversion_type = OP_VECTOR_TO_VALUE;

					}
					else if (from_type == SOCK_VECTOR && to_type == SOCK_RGBA) {
						conversion_type = OP_VECTOR_TO_COLOR;

					}
					else if (from_type == SOCK_RGBA && to_type == SOCK_FLOAT) {
						conversion_type = OP_COLOR_TO_VALUE;

					}
					else if (from_type == SOCK_RGBA && to_type == SOCK_VECTOR) {
						conversion_type = OP_COLOR_TO_VECTOR;

					}
					connected_node = new Node(conversion_type, connected_node);
				}
				this->set_connected_node(connected_node);
				return;
			}
		}

		// No link
		Node* connected_node;
		switch(socket->type) {
			case SOCK_FLOAT:
				connected_node = new Node(node_tree, socket, CMP_NODE_VALUE);
				this->set_connected_node(connected_node);
				break;

			case SOCK_RGBA:
				connected_node = new Node(node_tree, socket, CMP_NODE_RGB);
				this->set_connected_node(connected_node);
				break;
		}
	}

	NodeSocket::~NodeSocket() {
		if(this->connected_node != NULL){
			delete this->connected_node;
		}
	}
	
	void NodeSocket::set_connected_node(Node * node) {
		if (this->connected_node) {
			this->connected_node->unregister_user(this);
		}
		this->connected_node = node;
		if (node != NULL) {
			this->connected_node->register_user(this);
		}
	}

	Node* NodeSocket::get_connected_node() const {
		return this->connected_node;
	}

}
