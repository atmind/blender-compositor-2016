#include "cmp_compiler.hpp"
#include "cmp_node.hpp"
#include <iostream>

COMP_NAMESPACE_BEGIN

bool compare_value(Node* a, Node* b) {
	float value_a, value_b;
	PointerRNA ptr;
	
	if (a->type != CMP_NODE_VALUE && b->type != CMP_NODE_VALUE) {
		return false;
	}

	RNA_pointer_create((ID *)a->node_tree, &RNA_NodeSocket, a->b_socket, &ptr);
	value_a = RNA_float_get(&ptr, "default_value");
	RNA_pointer_create((ID *)b->node_tree, &RNA_NodeSocket, b->b_socket, &ptr);
	value_b = RNA_float_get(&ptr, "default_value");

	return value_a == value_b;
}

bool compare_rgba(Node* a, Node* b) {
	float value_a[4], value_b[4];
	PointerRNA ptr;
	
	if (a->type != CMP_NODE_RGB && b->type != CMP_NODE_RGB) {
		return false;
	}

	RNA_pointer_create((ID *)a->node_tree, &RNA_NodeSocket, a->b_socket, &ptr);
	RNA_float_get_array(&ptr, "default_value", value_a);
	RNA_pointer_create((ID *)b->node_tree, &RNA_NodeSocket, b->b_socket, &ptr);
	RNA_float_get_array(&ptr, "default_value", value_b);

	return (value_a[0] == value_b[0]) &&
	       (value_a[1] == value_b[1]) &&
	       (value_a[2] == value_b[2]) &&
	       (value_a[3] == value_b[3]);
}


typedef struct OptimizeConfiguration {
	int node_type;
	std::function<bool(Node*, Node*)> compare_operator;
} OptimizeConfiguration;

static OptimizeConfiguration *OPTIMIZE_CONFIGURATION = new OptimizeConfiguration[3] {
	{CMP_NODE_VALUE, compare_value},
	{CMP_NODE_RGB,   compare_rgba },
	{-1,             NULL         }
};


OptimizeConfiguration* get_optimization_configuration(Node* node) {
	int index = 0;
	
	while(true) {
		OptimizeConfiguration * config = &OPTIMIZE_CONFIGURATION[index++];

		// check node_type
		if (node->type == config->node_type) {
			return config;
		}

		if (config->node_type == -1) {
			break;
		}
	}
	
	return NULL;
}

bool can_be_optimized(Node* node) {
	return (get_optimization_configuration(node) != NULL);
}

void build_optimizable_nodes_list(std::list<Node*> *optimizable_nodes, Node* node) {
	if (can_be_optimized(node)) {
		optimizable_nodes->push_back(node);
	}

	for (auto input_socket: node->inputs) {
		build_optimizable_nodes_list(optimizable_nodes, input_socket->connected_node);
	}
}

Node* find_first_similar(std::list<Node*> *optimizable_nodes, Node* node) {
	OptimizeConfiguration* config = get_optimization_configuration(node);
	
	for (auto node2: *optimizable_nodes) {
		if (config->compare_operator(node, node2)) {
										return node2;
		}
	}
				return NULL;
}

void optimize_nodetree(Node* node) {
	std::list<Node*> optimizable_nodes;

	build_optimizable_nodes_list(&optimizable_nodes, node);

	while (optimizable_nodes.size() != 0) {
		std::cout << "Checking node for optimization: " << optimizable_nodes.size() << " left\n";
		Node* node_to_check = optimizable_nodes.front();
		optimizable_nodes.pop_front();
		Node* first_similar = find_first_similar(&optimizable_nodes, node_to_check);
		if (first_similar) {
			std::cout << " - merging two nodes\n";
			// link node_to_check to first similar and free node_to_check
			for(auto user: node_to_check->get_users()) {
				user->set_connected_node(first_similar);
			}
			free(node_to_check);
		}
	}
}

COMP_NAMESPACE_END
