#include "cmp_compiler.hpp"
#include "make_program.hpp"
#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "kernel/operations/op_types.h"
#include "BKE_node.h"
#include <iostream>
#include "util/util_math.h"
#include <algorithm>
#include "BKE_camera.h"
#include "DNA_camera_types.h"
#include "DNA_viewport_types.h"
#include "BLI_math_matrix.h"
#include "BLI_math_vector.h"
#include "BLI_math_rotation.h"
#include "MEM_guardedalloc.h"
#include "nodetree_optimizer.hpp"

#include "BKE_image.h"
extern "C" {
	#  include "BLI_listbase.h"
	#  include "RE_pipeline.h"
	#  include "RE_shader_ext.h"
	#  include "RE_render_ext.h"
	#  include "DNA_movieclip_types.h"
	#  include "BKE_movieclip.h"
	#  include "BKE_tracking.h"
	#  include "IMB_imbuf.h"
	#include "IMB_imbuf_types.h"
}

COMP_NAMESPACE_BEGIN

/* forward declarations of local functions */
void initialize_resources(Node* node);

Buffer::~Buffer() {
	switch(this->type) {
		case Buffer_type::IMAGE:
		case Buffer_type::MOVIECLIP:
		{
			assert(ibuf != NULL);
			IMB_freeImBuf(ibuf);
			data = NULL;
			break;
		}
		case Buffer_type::RENDERLAYER:
		{
			break;
		}
		case Buffer_type::GUARDED_MALLOC:
		{
			MEM_freeN(data);
			data = NULL;
			break;
		}
	}
}


// class Program
void Program::add(uint4 a) {
	this->program.push_back(a);
}

uint4* Program::as_uint4_array() {
	return &this->program[0];
}
int Program::size() {
	return this->program.size();
}

int Program::get_buffer_index(Buffer* buffer) {
	int index = this->data_indexer.get_index(buffer->data);
	if (index == -1) {
		this->buffers.push_back(buffer);
		index = this->data_indexer.add(buffer->data);
	}
	return index;
}

int Program::get_buffer_user_index(BufferUser* buffer_user) {
	int index = this->buffer_users.size();
	this->buffer_users.push_back(buffer_user);
	return index;
}

Buffer* Program::get_buffer(int index){
	return this->buffers[index];
}

int Program::get_viewport_index(Object* camera_object) {
	const float input_origin[4] = {0.0, 0.0, 0.0, 1.0};
	const float input_direction[4] = {0.0, 0.0, -1.0, 0.0};
	const float input_up[4] = {0.0, 1.0, 0.0, 0.0};
	const float input_side[4] = {1.0, 0.0, 0.0, 0.0};
	float output_origin[4];
	float output_direction[4];
	float output_up[4];
	float output_side[4];
	float up_size;
	float side_size;

	int index = this->camera_indexer.get_index(camera_object);
	if (index == -1) {
		// Make viewport
		Viewport * viewport = new Viewport();

		// Calculate the world translation vector and store in output_origin
		mul_v4_m4v4(output_origin, camera_object->obmat, input_origin);
		viewport->position = make_float3(output_origin[0], output_origin[1], output_origin[2]);

		// Calculate the direction from the output_direction
		mul_v4_m4v4(output_direction, camera_object->obmat, input_direction);
		normalize_v3(output_direction);
		viewport->direction = make_float3(output_direction[0], output_direction[1], output_direction[2]);

		// Calculate the upvector from the output_direction
		mul_v4_m4v4(output_up, camera_object->obmat, input_up);
		up_size = len_v3(output_up);
		normalize_v3(output_up);
		viewport->up_vector = make_float3(output_up[0], output_up[1], output_up[2]);

		// Calculate the side_vector
		// side_vector = direction X up_vector
		viewport->side_vector = cross(viewport->direction, viewport->up_vector);
		mul_v4_m4v4(output_side, camera_object->obmat, input_side);
		side_size = len_v3(output_side);

		if (camera_object && camera_object->type == OB_CAMERA) {
			Camera* camera = (Camera*) camera_object->data;
			viewport->type = camera->type;
			float plane_offset = 0;
			switch (camera->type) {
				case CAM_PERSP:
					viewport->lens = camera->lens;
					viewport->sensor_size = BKE_camera_sensor_size(camera->sensor_fit, camera->sensor_x, camera->sensor_y);
					viewport->field_of_view = focallength_to_fov(viewport->lens, viewport->sensor_size);
					viewport->scale = 1.0; 
					plane_offset = 0.5/tan(viewport->field_of_view/2.0);
 					break;

				case CAM_ORTHO:
					viewport->scale = camera->ortho_scale;
					break;
			}
			viewport->plane_position = viewport->position + viewport->direction * plane_offset;
		}
		else if (camera_object && camera_object->type == OB_VIEWPORT) {
			::Viewport *dnavp = (::Viewport*)camera_object->data;
			switch(dnavp->type) {
				case VIEWPORT_TYPE_PLANE:
				{
					viewport->type = CAM_PLANE;
					viewport->scale_x = side_size;
					viewport->scale_y = up_size;
					viewport->plane_position = viewport->position;
					break;
				}
				// TODO: Spherical
			}
		}
		this->viewports.push_back(*viewport);
		index = this->camera_indexer.add(camera_object);
	}
	return index;
}

std::vector<Buffer*> Program::get_buffers() {
	return this->buffers;
}

std::vector<BufferUser*> Program::get_buffer_users() {
	return this->buffer_users;
}

std::vector<Viewport> Program::get_viewports(){
	return this->viewports;
}

void Program::print() {
	int index = 0;
	for (uint4 a: this->program) {
		std::cout << "Compositor::Program line " << index << ": [" << a.x << ", " << a.y << ", " << a.z << ", " << a.w << "]\n";
		index ++;
	}
}

Program::~Program(){
	for (auto buffer:this->buffers){
		delete buffer;
	}
}

/// class Compiler
Compiler::Compiler(Node* node, RenderContext* context) {
	this->node = node;
	this->context = context;
}

void Compiler::compile() {
	MemBlockManager stack = {CVM_STACK_SIZE};
	MemBlockManager ray_stack = {CVM_RAY_STACK_SIZE};

	optimize_nodetree(this->node);

	initialize_resources(this->node);
	this->set_result_offset(&stack, this->node);
	std::cout << stack << "\n";
	this->set_ray_offset(&ray_stack, this->node);
	std::cout << ray_stack << "\n";
	this->set_input_ray_offset(this->node, 0);

	make_program(&this->program, this->node);
	this->program.print();
}

/** Determines where each node can write its result on the MemBlock.
		Parent nodes can reuse the offset of the child nodes.
**/
void Compiler::set_result_offset(MemBlockManager *memblock, Node* node) {
	for (auto socket : node->inputs) {
		this->set_result_offset(memblock, socket->connected_node);
	}
	for (auto socket : node->inputs) {
		memblock->free_usage(socket->connected_node->result_offset, socket->connected_node->result_size);
	}

	node->result_offset = memblock->find_free_space(node->result_size, node->get_users().size());
	std::cout << "Compositor::Compiler::determine_result_offset set offset to " << node->result_offset << " with size " << node->result_size << "\n";
}

/** Determines where each node can read the ray from.
		Parent nodes set the Ray whilst child nodes read the ray set by the parent.
**/
void Compiler::set_ray_offset(MemBlockManager * memblock, Node* node) {
	if (node->ray_size){
		node->ray_offset = memblock->find_free_space(node->ray_size, 1);
		std::cout << "Compositor::Compiler::determine_ray_offset set ray_offset to " << node->ray_offset << " with size " << node->ray_size << "\n";
	}
	for (auto nodesocket: node->inputs){
		this->set_ray_offset(memblock, nodesocket->connected_node);
	}
	memblock->free_usage(node->ray_offset, node->ray_size);
}

void Compiler::set_input_ray_offset(Node* node, uint input_ray_offset) {
	node->input_ray_offset = input_ray_offset;
	std::cout << "Compositor::Compiler::update_input_ray_offset set input_ray_offset to " << node->input_ray_offset << ".\n";

	uint temp_ray_offset;
	uint input_socket_index = 0;

	for (auto socket : node->inputs) {
		temp_ray_offset = input_ray_offset;
		switch (node->type) {
			case CMP_NODE_VIEWER:
				temp_ray_offset = node->ray_offset;
				break;
			case CMP_NODE_BLUR:
				if (input_socket_index == 0) {
					temp_ray_offset = node->ray_offset;
				}
				break;
		}
		this->set_input_ray_offset(socket->connected_node, temp_ray_offset);
		input_socket_index ++;
	}
}

Program* Compiler::get_program() {
	return &this->program;
}

int Compiler::get_viewport_index(Object* camera_object){
	if (camera_object == NULL) {
		camera_object = this->context->scene->camera;
	}
	return this->program.get_viewport_index(camera_object);
}

void Compiler::initialize_resources(Node* node) {
	Buffer* buffer = NULL;
	BufferUser* buffer_user = NULL;
	node->result_offset = 0;
	node->result_size = 1;
	node->ray_offset = 0;
	node->ray_size = 0;
	node->input_ray_offset = 0;
	node->viewport_index = -1;
	node->added_to_program = false;

	switch (node->type) {
		case CMP_NODE_VIEWER:
			node->result_size = 0;
			node->ray_size = 1;
			node->viewport_index = this->get_viewport_index(NULL);
			break;

		case CMP_NODE_BLUR:
			node->ray_size = 1;
			break;

		case CMP_NODE_R_LAYERS: {
			// TODO: support more passes
			short layer_id = node->b_node->custom1;
			Scene* scene = (Scene*)node->b_node->id;
			const char *view_name = this->context->view_name;
			NodeImageLayer *storage = (NodeImageLayer*) node->b_socket->storage;
			NodeCMPImageData *node_storage = (NodeCMPImageData*)node->b_node->storage;
			Render *re = (scene) ? RE_GetSceneRender(scene) : NULL;
			RenderResult *rr = NULL;
			float *floatbuffer = NULL;
			if (re)
				rr = RE_AcquireResultRead(re);
			if (rr) {
				ViewLayer *view_layer = (ViewLayer *)BLI_findlink(&scene->view_layers, layer_id);
				if (view_layer) {

					RenderLayer *rl = RE_GetRenderLayer(rr, view_layer->name);
					if (rl) {
						RenderPass *rp = (RenderPass*)BLI_findstring(&rl->passes, storage->pass_name, offsetof(RenderPass, name));
						floatbuffer = RE_RenderLayerGetPass(rl, storage->pass_name, view_name);
						buffer = new Buffer();
						buffer_user = new BufferUser();
						buffer->type = Buffer_type::RENDERLAYER;
						buffer->data = floatbuffer;
						buffer->components = rp->channels;
						buffer->width = rl->rectx;
						buffer->height = rl->recty;
						buffer_user->viewport = node_storage->viewport;
						buffer_user->interpolation = (InterpolationType)node_storage->interpolation;
						buffer_user->extension = (ExtensionType)node_storage->extension;
					}
				}
			}
			if (re) {
				RE_ReleaseResult(re);
				re = NULL;
			}
			break;
		}

		case CMP_NODE_IMAGE: {
			node->viewport_index = this->get_viewport_index(NULL);
			//TODO need to support Multilayer
			Image *image = (Image*)node->b_node->id;
			NodeCMPImage * node_storage = (NodeCMPImage *)node->b_node->storage;
			ImageUser *imageuser = &node_storage->iuser;
			NodeCMPImageData *image_data = &node_storage->image_data;
			BKE_image_user_frame_calc(imageuser, this->context->frame_number, 0);

			if(image){
				ImBuf *ibuf;
				ibuf = BKE_image_acquire_ibuf(image, imageuser, NULL);
				if (ibuf == NULL || (ibuf->rect == NULL && ibuf->rect_float == NULL)) {
					BKE_image_release_ibuf(image, ibuf, NULL);
				} else{
					buffer = new Buffer();
					buffer->type = Buffer_type::IMAGE;
					if (ibuf->rect_float == NULL || ibuf->userflags & IB_RECT_INVALID) {
						IMB_float_from_rect(ibuf);
						ibuf->userflags &= ~IB_RECT_INVALID;
					}
					buffer->width = ibuf->x;
					buffer->height = ibuf->y;
					buffer->data = ibuf->rect_float;
					buffer->components = ibuf->channels;
					buffer->ibuf = ibuf;
					buffer_user = new BufferUser();
					buffer_user->viewport = image_data->viewport;
					buffer_user->interpolation = (InterpolationType)image_data->interpolation;
					buffer_user->extension = (ExtensionType)image_data->extension;
				}
			}
			break;
		}

		case CMP_NODE_MOVIECLIP: {
			node->viewport_index = this->get_viewport_index(NULL);

			MovieClip *movieClip = (MovieClip *)node->b_node->id;
			MovieClipUser *movieClipUser = (MovieClipUser *)node->b_node->storage;

			if (movieClip) {
				BKE_movieclip_user_set_frame(movieClipUser, this->context->frame_number);
				ImBuf *ibuf;
				if (this->context->is_rendering) {
					ibuf = BKE_movieclip_get_ibuf_flag(movieClip, movieClipUser, movieClip->flag, MOVIECLIP_CACHE_SKIP);
				} else {
					ibuf = BKE_movieclip_get_ibuf(movieClip, movieClipUser);
				}
				if (ibuf) {
					buffer = new Buffer();
					buffer->type = Buffer_type::MOVIECLIP;
					BKE_movieclip_get_size(movieClip, movieClipUser, &buffer->width, &buffer->height);
					if (ibuf->rect_float == NULL || ibuf->userflags & IB_RECT_INVALID) {
						IMB_float_from_rect(ibuf);
						ibuf->userflags &= ~IB_RECT_INVALID;
					}
					buffer->data = ibuf->rect_float;
					buffer->components = ibuf->channels;
					buffer->ibuf = ibuf;
					buffer_user = new BufferUser();
					buffer_user->viewport = NULL;
					buffer_user->interpolation = INTERPOLATION_LINEAR;
					buffer_user->extension = EXTENSION_CLIP;

				}
			}
			break;
		}
		default:
			// ERROR unknown node type
			std::cout << "Compositor::Compiler::update_op_settings unknown node type\n";
			break;
	}

	// Convert to float4 as float3 uses the same space as a float4
	if (buffer && buffer->components == 3) {
		Buffer* tmp = new Buffer();
		tmp->width = buffer->width;
		tmp->height = buffer->height;
		tmp->type = Buffer_type::GUARDED_MALLOC;
		float *buffer3_data = buffer->data;
		size_t total_buffer_size = buffer->width*buffer->height*4*sizeof(float);
		float* buffer_data = (float*)MEM_mallocN(total_buffer_size, "Compositor-float4-texture");
		int index3 = 0;
		int index4 = 0;
		for (int i = 0; i < buffer->width * buffer->height ; i ++) {
			buffer_data[index4] = buffer3_data[index3];
			buffer_data[index4+1] = buffer3_data[index3+1];
			buffer_data[index4+2] = buffer3_data[index3+2];
			buffer_data[index4+3] = 1.0;
			index3 += 3;
			index4 += 4;
		}
		tmp->data = buffer_data;
		tmp->components = 4;
		delete buffer;
		buffer = tmp;
	}
	if (buffer){
		uint buffer_index = this->program.get_buffer_index(buffer);

		// Note: retrieve actual stored instance in program
		buffer_user->buffer = this->program.get_buffer(buffer_index);
		// There is a different buffer pointing to the same address.
		// We use that one and remove the one created just now.
		if (buffer != buffer_user->buffer) {
			delete buffer;
		}

		node->buffer_user_index = this->program.get_buffer_user_index(buffer_user);
		node->viewport_index = this->get_viewport_index(buffer_user->viewport);
	}

	for (auto socket: node->inputs) {
		initialize_resources(socket->connected_node);
	}
}

COMP_NAMESPACE_END
