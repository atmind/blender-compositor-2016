#ifndef _MAKE_PROGRAM_HPP_
#define _MAKE_PROGRAM_HPP_
COMP_NAMESPACE_BEGIN
#include "cmp_compiler.hpp"

void make_program(Program* program, Node* node);

COMP_NAMESPACE_END
#endif