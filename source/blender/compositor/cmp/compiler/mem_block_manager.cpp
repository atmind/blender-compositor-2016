#include "mem_block_manager.hpp"

#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "kernel/operations/op_types.h"
#include "util/util_math.h"

#include <iostream>

COMP_NAMESPACE_BEGIN

MemBlockManager::MemBlockManager(uint size) {
	this->usage = new uint[size];
	this->size = size;
	std::fill_n(this->usage, size, 0);
	this->max_usage = 0;
}
MemBlockManager::~MemBlockManager() {
	free(this->usage);
}

int MemBlockManager::find_free_space(const uint size, const uint num_users) {
	int index = 0;
	int start_offset = 0;
	int selection_size = 0;
	while (index < CVM_STACK_SIZE) {
		// Break as we found a gap where we fit.
		if (selection_size == size) {
			// found space, mark as being used;
			std::fill_n(&this->usage[start_offset], size, num_users);
			// return the start offset of the found space.
			this->max_usage = max(this->max_usage, index);
			return start_offset;
		}

		if (this->usage[index] == 0) {
			// place on stack is not being used atm.
			if (selection_size == 0) {
				// is the start of new possible space, so remember the start_offset
				start_offset = index;
			}
			selection_size ++;
		} else {
			selection_size = 0;
			start_offset = 0;
		}
		index ++;
	}

	// No space found on the stack to allocate, for now return -1,
	return -1;
}

std::ostream& operator<<(std::ostream& stream, const MemBlockManager& memblock){
	stream << "Compositor::MemBlockManager size " << memblock.size << " max_usage " << memblock.max_usage;
	return stream;
}

void MemBlockManager::free_usage(const uint offset, const uint size) {
	for (uint index = 0 ; index < size ; index ++) {
		this->usage[offset+index]--;
	}
}
COMP_NAMESPACE_END