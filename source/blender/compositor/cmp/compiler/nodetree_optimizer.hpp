#ifndef __CMP_NODETREE_OPTIMIZER_HPP__
#define __CMP_NODETREE_OPTIMIZER_HPP__

COMP_NAMESPACE_BEGIN

void optimize_nodetree(Node* node);

COMP_NAMESPACE_END
#endif
