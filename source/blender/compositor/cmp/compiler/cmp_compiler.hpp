#ifndef __CMP_COMPILER_HPP__
#define __CMP_COMPILER_HPP__

#include "cmp_node.hpp"

#define _KERNEL_CPU_
#include "util/util_types.h"
#include "util/util_viewport.h"
#include <iostream>
#include "cmp_rendercontext.hpp"
#include "mem_block_manager.hpp"
#include "DNA_object_types.h"

COMP_NAMESPACE_BEGIN

enum class Buffer_type {IMAGE, MOVIECLIP, RENDERLAYER, GUARDED_MALLOC};

struct Buffer {
	float* data = NULL;
	MemoryPTR data_ptr;
	int components = 0;
	int width = 0;
	int height = 0;
	Buffer_type type;

	/**
	 * ImBuf to free when destructor is called
	 */
	ImBuf* ibuf = NULL;

	~Buffer();
};

struct BufferUser {
	Buffer* buffer;
	Object* viewport;
	InterpolationType interpolation;
	ExtensionType extension;
};


template<typename T> struct Indexer {
	std::vector<T> indices;

	int get_index(T el) {
		int index = 0;
		for (auto other: indices) {
			if (other == el) {
				return index;
			}
			index ++;
		}
		return -1;
	}

	int add(T el) {
		this->indices.push_back(el);
		return indices.size() - 1;
	}
};

struct Program {
private:
	std::vector<uint4> program;

	Indexer<float*> data_indexer;
	std::vector<Buffer*> buffers;

	Indexer<Object*> camera_indexer;
	std::vector<Viewport> viewports;

	std::vector<BufferUser*> buffer_users;

public:
	~Program();
	void add(uint4);
	uint4* as_uint4_array();
	int size();
	void print();
	int get_buffer_index(Buffer* buffer);
	int get_buffer_user_index(BufferUser* buffer_user);
	Buffer* get_buffer(int index);
	int get_viewport_index(Object* camera_object);
	std::vector<Buffer*> get_buffers();
	std::vector<BufferUser*> get_buffer_users();
	std::vector<Viewport> get_viewports();
};

struct Compiler {
	/**
	 * Reference to the unrolled node tree
	 */
	Node* node;
	Program program;
	RenderContext* context;

public:
	Compiler(Node *node, RenderContext *context);
	void compile();
	Program* get_program();

private:
	void set_result_offset(MemBlockManager *stack, Node* node);
	void set_ray_offset(MemBlockManager * stack, Node* node);
	void set_input_ray_offset(Node* node, uint input_ray_offset);
	void initialize_resources(Node* node);
	int get_viewport_index(Object* camera_object);
};

COMP_NAMESPACE_END
#endif
