#ifndef _MEM_BLOCK_MANAGER_HPP_
#define _MEM_BLOCK_MANAGER_HPP_

#define _KERNEL_CPU_
#include "util/util_types.h"

#include <iostream>

COMP_NAMESPACE_BEGIN
struct MemBlockManager {
private:
	uint *usage;
	uint size;
	uint max_usage;

public:
	MemBlockManager(uint size);
	~MemBlockManager();
	void free_usage(const uint offset, const uint size);
	int find_free_space(const uint size, const uint num_users);
	friend std::ostream& operator<<(std::ostream& stream, const MemBlockManager& memblock);
};
COMP_NAMESPACE_END
#endif