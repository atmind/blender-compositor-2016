#include "cmp_compiler.hpp"
#include "kernel/operations/op_types.h"
#include "util/util_math.h"

COMP_NAMESPACE_BEGIN


void make_program(Program* program, Node* node) {
	float temp[4];
	
	// Early break. node has already been compiled.
	if (node->added_to_program) {
		return;
	}

	switch(node->type) {
		case CMP_NODE_VIEWER:
			program->add(make_uint4(OP_OUTPUT_RAY, node->ray_offset, node->viewport_index, 0));
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(OP_OUTPUT, node->inputs[0]->connected_node->result_offset, 0, 0));
			program->add(make_uint4(OP_END, 0, 0, 0));
			break;

		case CMP_NODE_RGB:
			PointerRNA ptr;
			RNA_pointer_create((ID *)node->node_tree, &RNA_NodeSocket, node->b_socket, &ptr);
			RNA_float_get_array(&ptr, "default_value", temp);

			program->add(make_uint4(OP_COLOR, node->result_offset, 0, 0));
			program->add(make_uint4(
				float_as_uint(temp[0]),
				float_as_uint(temp[1]),
				float_as_uint(temp[2]),
				float_as_uint(temp[3])
			));
			break;

		case CMP_NODE_VALUE:
			RNA_pointer_create((ID *)node->node_tree, &RNA_NodeSocket, node->b_socket, &ptr);
			temp[0] = RNA_float_get(&ptr, "default_value");

			program->add(make_uint4(OP_VALUE, node->result_offset, float_as_uint(temp[0]), 0));
			break;

		case CMP_NODE_MIX_RGB:
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(OP_MIX, node->result_offset, node->b_node->custom1, (node->b_node->custom2 & 1)));
			program->add(make_uint4(
				node->inputs[1]->connected_node->result_offset,
				node->inputs[2]->connected_node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				(node->b_node->custom2 & 2)));
			break;

		case CMP_NODE_ALPHAOVER:
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(OP_ALPHA_OVER, node->result_offset, 0, 0));
			program->add(make_uint4(
				node->inputs[1]->connected_node->result_offset,
				node->inputs[2]->connected_node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				0));
			break;

		case CMP_NODE_IMAGE:
		case CMP_NODE_R_LAYERS:
		case CMP_NODE_MOVIECLIP:
			{
				if (node->buffer_user_index != -1) {
					BufferUser* buffer_user = program->get_buffer_users()[node->buffer_user_index];
					if (buffer_user->buffer->components == 1){
						program->add(make_uint4(OP_IMAGE_1, node->result_offset, node->input_ray_offset, node->buffer_user_index));
						program->add(make_uint4(node->viewport_index, 0, 0, 0));
					} else if (buffer_user->buffer->components == 4){
						program->add(make_uint4(OP_IMAGE_4, node->result_offset, node->input_ray_offset, node->buffer_user_index));
						program->add(make_uint4(node->viewport_index, 0, 0, 0));
					}
				} else {
					// No buffer so we use a simple color.
					// TODO: do based bnode type and use OP_VALUE for single float channels.
					program->add(make_uint4(OP_COLOR, node->result_offset, 0, 0));
					program->add(make_uint4(0, 0, 0, 0));
				}
				
				// extract alpha channel from combined pass
				// NOTE: it will store the alpha in the same result_offset.
				// TODO: Old compositor also compared the renderpass we are not doing that...
				if (node->type == CMP_NODE_R_LAYERS && STREQ(node->b_socket->name, "Alpha")) {
					program->add(make_uint4(
						OP_SEPARATE_A, 
						node->result_offset,
						node->result_offset,
						0));
				}
				break;
			}

		case CMP_NODE_BLUR:
			{
				make_program(program, node->inputs[1]->connected_node);
				NodeBlurData * blur_data = (NodeBlurData*)node->b_node->storage;
				program->add(make_uint4(OP_BLUR_RAY, node->ray_offset, node->input_ray_offset, 0));
				program->add(make_uint4(
					float_as_uint(blur_data->percentx/100.0f),
					float_as_uint(blur_data->percenty/100.0f),
					node->inputs[1]->connected_node->result_offset,
					0));
				make_program(program, node->inputs[0]->connected_node);
				program->add(make_uint4(OP_BLUR, node->result_offset, 0, 0));
				program->add(make_uint4(
					node->inputs[0]->connected_node->result_offset,
					node->inputs[0]->connected_node->input_ray_offset,
					blur_data->filtertype,
					0));
				break;
			}

		case CMP_NODE_COLOR_MATTE:
		{
			NodeChroma * matte_data = (NodeChroma*)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_COLOR_MATTE, 
				node->result_offset, 
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset
			));
			program->add(make_uint4(
				float_as_uint(matte_data->t1),
				float_as_uint(matte_data->t2),
				float_as_uint(matte_data->t3),
				0));

			break;
		}

		case CMP_NODE_CHROMA_MATTE:
		{
			NodeChroma * matte_data = (NodeChroma*)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_CHROMA_MATTE, 
				node->result_offset, 
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset
			));
			program->add(make_uint4(
				float_as_uint(matte_data->t1),
				float_as_uint(matte_data->t2),
				float_as_uint(matte_data->fstrength),
				0));

			break;
		}

		case CMP_NODE_MATH:
		{
			int op_type = node->b_node->custom1;
			make_program(program, node->inputs[0]->connected_node);
			if (!ELEM(op_type, NODE_MATH_SIN, NODE_MATH_COS, NODE_MATH_TAN, NODE_MATH_ASIN, NODE_MATH_ACOS, NODE_MATH_ATAN, NODE_MATH_ROUND, NODE_MATH_ABS)) {
				make_program(program, node->inputs[1]->connected_node);
			}
			program->add(make_uint4(
				OP_MATH, 
				node->result_offset,
				node->b_node->custom1, 
				node->b_node->custom2));
			program->add(make_uint4(
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset,
				0, 0));
			break;
		}

		// NOTE: Conversion has no UI node. hence its operation is used.
		case OP_VALUE_TO_COLOR:
		case OP_VALUE_TO_VECTOR:
		case OP_VECTOR_TO_VALUE:
		case OP_VECTOR_TO_COLOR:
		case OP_COLOR_TO_VALUE:
		case OP_COLOR_TO_VECTOR:
		{
			make_program(program, node->inputs[0]->connected_node);
			program->add(make_uint4(
				node->type, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_RGBTOBW:
		{
			make_program(program, node->inputs[0]->connected_node);
			program->add(make_uint4(
				OP_COLOR_TO_VALUE, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_SEPRGBA:
		{
			make_program(program, node->inputs[0]->connected_node);
			int op_type;
			switch(node->output) {
				case 0:
				{
					op_type = OP_SEPARATE_R;
					break;
				}

				case 1:
				{
					op_type = OP_SEPARATE_G;
					break;
				}

				case 2:
				{
					op_type = OP_SEPARATE_B;
					break;
				}

				case 3:
				{
					op_type = OP_SEPARATE_A;
					break;
				}

			}
			program->add(make_uint4(
				op_type, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_COMBRGBA:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_COMBINE_RGBA, 
				node->result_offset,
				0, 
				0));
			program->add(make_uint4(
				node->inputs[0]->connected_node->result_offset, 
				node->inputs[1]->connected_node->result_offset, 
				node->inputs[2]->connected_node->result_offset, 
				node->inputs[3]->connected_node->result_offset));
			break;
		}

		case CMP_NODE_SEPHSVA:
		{
			make_program(program, node->inputs[0]->connected_node);
			int op_type;
			switch(node->output) {
				case 0:
				{
					op_type = OP_SEPARATE_H;
					break;
				}

				case 1:
				{
					op_type = OP_SEPARATE_S;
					break;
				}

				case 2:
				{
					op_type = OP_SEPARATE_V;
					break;
				}

				case 3:
				{
					op_type = OP_SEPARATE_A;
					break;
				}

			}
			program->add(make_uint4(
				op_type, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_SEPYUVA:
		{
			make_program(program, node->inputs[0]->connected_node);
			int op_type;
			switch(node->output) {
				case 0:
				{
					op_type = OP_SEPARATE_YUV_Y;
					break;
				}

				case 1:
				{
					op_type = OP_SEPARATE_YUV_U;
					break;
				}

				case 2:
				{
					op_type = OP_SEPARATE_YUV_V;
					break;
				}

				case 3:
				{
					op_type = OP_SEPARATE_A;
					break;
				}

			}
			program->add(make_uint4(
				op_type, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_COMBHSVA:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_COMBINE_HSVA, 
				node->result_offset,
				0, 
				0));
			program->add(make_uint4(
				node->inputs[0]->connected_node->result_offset, 
				node->inputs[1]->connected_node->result_offset, 
				node->inputs[2]->connected_node->result_offset, 
				node->inputs[3]->connected_node->result_offset));
			break;
		}

		case CMP_NODE_HUE_SAT:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_HSV, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				0));
			program->add(make_uint4(
				node->inputs[1]->connected_node->result_offset, 
				node->inputs[2]->connected_node->result_offset, 
				node->inputs[3]->connected_node->result_offset, 
				node->inputs[4]->connected_node->result_offset));
			break;
		}

		case CMP_NODE_BRIGHTCONTRAST:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_BRIGHTNESS_CONTRAST, 
				node->result_offset,
				0, 
				0));
			program->add(make_uint4(
				node->inputs[0]->connected_node->result_offset, 
				node->inputs[1]->connected_node->result_offset, 
				node->inputs[2]->connected_node->result_offset, 
				0));
			break;
		}

		case CMP_NODE_GAMMA:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			program->add(make_uint4(
				OP_GAMMA, 
				node->result_offset,
				node->inputs[0]->connected_node->result_offset, 
				node->inputs[1]->connected_node->result_offset));
			break;
		}

		case CMP_NODE_COLORBALANCE:
		{
			NodeColorBalance *n = (NodeColorBalance *)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			if (node->b_node->custom1) {
				program->add(make_uint4(
					OP_COLORBALANCE_LGG, 
					node->result_offset,
					node->inputs[0]->connected_node->result_offset, 
					node->inputs[1]->connected_node->result_offset));
				program->add(make_uint4(
					float_as_uint(2.0f - n->lift[0]),
					float_as_uint(2.0f - n->lift[1]),
					float_as_uint(2.0f - n->lift[2]),
					0
				));
				program->add(make_uint4(
					float_as_uint((n->gamma[0] != 0.0f) ? 1.0f / n->gamma[0] : 1000000.0f),
					float_as_uint((n->gamma[1] != 0.0f) ? 1.0f / n->gamma[1] : 1000000.0f),
					float_as_uint((n->gamma[2] != 0.0f) ? 1.0f / n->gamma[2] : 1000000.0f),
					0
				));
				program->add(make_uint4(
					float_as_uint(2.0f - n->gain[0]),
					float_as_uint(2.0f - n->gain[1]),
					float_as_uint(2.0f - n->gain[2]),
					0
				));
			}
			else {
				program->add(make_uint4(
					OP_COLORBALANCE_CDL, 
					node->result_offset,
					node->inputs[0]->connected_node->result_offset, 
					node->inputs[1]->connected_node->result_offset));
				program->add(make_uint4(
					float_as_uint(n->offset_basis + n->offset[0]),
					float_as_uint(n->offset_basis + n->offset[1]),
					float_as_uint(n->offset_basis + n->offset[2]),
					0
				));
				program->add(make_uint4(
					float_as_uint(n->power[0]),
					float_as_uint(n->power[1]),
					float_as_uint(n->power[2]),
					0
				));
				program->add(make_uint4(
					float_as_uint(n->slope[0]),
					float_as_uint(n->slope[1]),
					float_as_uint(n->slope[2]),
					0
				));
			}
			break;
		}

		case CMP_NODE_COLOR_SPILL:
		{
			NodeColorspill *data = (NodeColorspill*)node->b_node->storage;
			uint channel = node->b_node->custom1 - 1;
			uint method = node->b_node->custom2;
			if (data->unspill == 0) {
				switch (channel) {
					case 0:
					{
						data->uspillr = 1.0f;
						data->uspillg = 0.0f;
						data->uspillb = 0.0f;
						break;
					}
					case 1:
					{
						data->uspillr = 0.0f;
						data->uspillg = 1.0f;
						data->uspillb = 0.0f;
						break;
					}
					case 2:
					{
						data->uspillr = 0.0f;
						data->uspillg = 0.0f;
						data->uspillb = 1.0f;
						break;
					}
				}
			}

			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}

			program->add(make_uint4(
				OP_COLOR_SPILL,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset));
			program->add(make_uint4(
				method,
				channel,
				data->limchan,
				0));
			program->add(make_uint4(
				float_as_uint(data->uspillr),
				float_as_uint(data->uspillg),
				float_as_uint(data->uspillb),
				float_as_uint(data->limscale)));
			break;
		}

		case CMP_NODE_SETALPHA:
		{
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}

			program->add(make_uint4(
				OP_SET_ALPHA,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset));
			break;
		}
		case CMP_NODE_CHANNEL_MATTE:
		{
			NodeChroma *data = (NodeChroma*)node->b_node->storage;
			uint colorspace = node->b_node->custom1;
			uint matte_channel = node->b_node->custom2 - 1;
			
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}

			program->add(make_uint4(
				OP_CHANNEL_MATTE,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				0));
			program->add(make_uint4(
				colorspace,
				matte_channel,
				data->algorithm,
				data->channel - 1));
			program->add(make_uint4(
				float_as_uint(data->t1),
				float_as_uint(data->t2),
				float_as_uint(data->t1 - data->t2),
				0));
			break;
		}

		case CMP_NODE_DIFF_MATTE:
		{
			NodeChroma *data = (NodeChroma*)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}

			program->add(make_uint4(
				OP_DIFFERENCE_MATTE,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset));
			program->add(make_uint4(
				float_as_uint(data->t1),
				float_as_uint(data->t2),
				0,
				0));
			break;
		}

		case CMP_NODE_DIST_MATTE:
		{
			uint op_type;
			NodeChroma *data = (NodeChroma*)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}
			if (data->channel == 1) {
				op_type = OP_DISTANCE_MATTE_LINEAR;
			} else {
				op_type = OP_DISTANCE_MATTE_YCC;
			}

			program->add(make_uint4(
				op_type,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				node->inputs[1]->connected_node->result_offset));
			program->add(make_uint4(
				float_as_uint(data->t1),
				float_as_uint(data->t2),
				0,
				0));
			break;
		}

		case CMP_NODE_LUMA_MATTE:
		{
			NodeChroma *data = (NodeChroma*)node->b_node->storage;
			for(auto nodesocket:node->inputs){
				make_program(program, nodesocket->connected_node);
			}

			program->add(make_uint4(
				OP_LUMINANCE_MATTE,
				node->result_offset,
				node->inputs[0]->connected_node->result_offset,
				0));
			program->add(make_uint4(
				float_as_uint(data->t1),
				float_as_uint(data->t2),
				0,
				0));
			break;
		}

		default:
			std::cout << "Compositor::Compiler unknown node type\n";
			break;
	}
	
	// Mark node to be already added to the program
	node->added_to_program = true;
}

COMP_NAMESPACE_END
