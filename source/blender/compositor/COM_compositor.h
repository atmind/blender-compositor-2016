/*
 * Copyright 2011, Blender Foundation.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Contributor:
 *		Jeroen Bakker
 *		Monique Dewanchand
 */

#ifndef __COM_COMPOSITOR_H__
#define __COM_COMPOSITOR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "DNA_color_types.h"
#include "DNA_node_types.h"

void COM_execute(RenderData *rd, Scene *scene, bNodeTree *editingtree, int rendering,
                 const ColorManagedViewSettings *viewSettings, const ColorManagedDisplaySettings *displaySettings,
                 const char *viewName);

/**
 * @brief Initialize the compositor. This is called once during blender startup (WM_init)
 */
void COM_initialize(void);

/**
 * @brief Deinitialize the compositor caches and allocated memory.
 */
void COM_deinitialize(void);


#ifdef __cplusplus
}
#endif

#endif  /* __COM_COMPOSITOR_H__ */
