#ifndef _KERNEL_VIEWPORT_H_
#define _KERNEL_VIEWPORT_H_
COMP_NAMESPACE_BEGIN
// ASPECT RATIO as parameter?

comp_device_inline comp_device_global comp_device_struct Viewport* kernel_viewport(KernelGlobals *kg, uint id) {
	return &((comp_device_global comp_device_struct Viewport*)(kg->buffers[kg->viewport_ptr.index]+kg->viewport_ptr.offset))[id];
}

/**
 * Function to convert an x, y of a viewport to a ray.
 */
comp_device_inline void viewport_to_ray(comp_in(comp_device_struct KernelGlobals, kg), int viewport_id, comp_in(float2, coordinates), comp_device_struct Ray* ray) {
	comp_device_global comp_device_struct Viewport* vp = kernel_viewport(&kg, viewport_id);
	switch (vp->type) {
		case CAM_ORTHO:
		{
			// Worldposition of the pixel we are calculating.
			// ((((y-0.5)/aspect_ratio)*up_vector) + (x-0.5)*side_vector) * Scale + viewportPosition
			float ry = (coordinates.y-0.5)/kg.aspect_ratio;
			float rx = (coordinates.x-0.5);
			float3 unscaled = ry*vp->up_vector + rx*vp->side_vector;
			float3 scaled = unscaled * vp->scale;
			float3 worldposition = scaled + vp->position;

			ray->P = worldposition;
			ray->D = vp->direction;
			ray->side = vp->side_vector;
			break;
		}
		case CAM_PERSP:
		{
			//plane_position
			// (((y-0.5)/aspect_ratio*up_vector) + (x-0.5)*side_vector) * scale + vp->plane_position) - vp->position
			float ry = ((coordinates.y-0.5)/kg.aspect_ratio);
			float rx = (coordinates.x-0.5);
			float3 unscaled = ry*vp->up_vector + rx*vp->side_vector;
			float3 scaled = unscaled * vp->scale;
			float3 worldposition = scaled + vp->plane_position;
			float3 direction = normalize(worldposition - vp->position);
			ray->D = direction;
			ray->P = vp->position;
			ray->side = vp->side_vector;
			break;
		}
		default:
			ray->P = make_float3(coordinates.x, coordinates.y, 0.0);
			ray->D = make_float3(0.0, 0.0, 1.0);
			ray->side = make_float3(1.0, 0.0, 0.0);
	}
}

/**
 * Function to ray to an x, y for a viewport.
 */
comp_device_inline float2 ray_to_viewport(comp_in(comp_device_struct KernelGlobals, kg), comp_device_struct Ray* ray, int viewport_id) {
	comp_device_global comp_device_struct Viewport* vp = kernel_viewport(&kg, viewport_id);
	switch (vp->type) {
		case CAM_ORTHO:
		case CAM_PERSP:
		{
			float distance;
			if (ray_plane_intersect(ray, vp->plane_position, vp->direction, &distance)) {
				float3 worldpositionOfIntersection = ray->P + (distance * ray->D);
				float3 scaled = worldpositionOfIntersection - vp->position;
				float3 unscaled = scaled / vp->scale;

				// Use vector projections to calculate the rx and ry
				return make_float2(
				                   dot(unscaled, vp->side_vector) + 0.5, 
				                   dot(unscaled, vp->up_vector) * kg.aspect_ratio + 0.5);
			} else {
				return make_float2(0.0, 0.0);
			}
			break;
		}
		case CAM_PLANE:
		{
			float distance;
			if (ray_plane_intersect(ray, vp->plane_position, vp->direction, &distance)) {
				float3 worldpositionOfIntersection = ray->P + (distance * ray->D);
				float3 scaled = worldpositionOfIntersection - vp->position;

				// Use vector projections to calculate the rx and ry
				return make_float2(
				                  dot(scaled, vp->side_vector), 
				                  dot(scaled, vp->up_vector)) 
				/ make_float2(
				              vp->scale_x, 
				              vp->scale_y)
				+ 0.5f;
			} else {
				return make_float2(0.0, 0.0);
			}
			break;
		}
		default:
			return make_float2(ray->P.x, ray->P.y);
	}

}

COMP_NAMESPACE_END
#endif
