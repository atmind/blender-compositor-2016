#ifndef _KERNEL_GLOBALS_H_
#define _KERNEL_GLOBALS_H_

#ifdef _KERNEL_CPU_
#  include "util/util_vector.h"
#endif
#include "util/util_viewport.h"

COMP_NAMESPACE_BEGIN

// Start section - the next structs are shared with the Host
typedef struct comp_attribute_packed MemoryPTR {
	uint index;
	uint padding;
	ulong offset;
} MemoryPTR;

typedef struct comp_attribute_packed TextureInfo {
	MemoryPTR data_ptr;
	uint components;
	uint width;
	uint height;
	uint interpolation;
	uint extension;
	uint padding;
} TextureInfo;

typedef struct HostKernelGlobals {
	MemoryPTR texture_ptr;
	MemoryPTR viewport_ptr;
	uint num_samples;
	uint frame_number;
	uint2 output_dimension;
	float aspect_ratio;
	uint seed;
} HostKernelGlobals;
// End section


#ifdef _KERNEL_CPU_
typedef struct KernelGlobals {
	uint4 *_program;
	char* buffers[1];

	MemoryPTR texture_ptr;
	MemoryPTR viewport_ptr;
	int num_samples;
	int frame_number;
	float aspect_ratio;
	uint seed;
	float2 pixel_size;
} KernelGlobals;
#endif

#ifdef _KERNEL_OPENCL_
typedef struct KernelGlobals {
	__local uint4 *_program;
	__global char* buffers[1];

	MemoryPTR texture_ptr;
	MemoryPTR viewport_ptr;
	int num_samples;
	int frame_number;
	float aspect_ratio;
	uint seed;
	float2 pixel_size;
} KernelGlobals;

#endif

COMP_NAMESPACE_END

#endif
