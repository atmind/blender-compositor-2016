#include "kernel/kernels/opencl/kernel_opencl_defines.h"
#include "kernel/kernel_compat_opencl.h"
#include "kernel/kernel.h"

__kernel void compositor_sample(__write_only image2d_t result, HostKernelGlobals host_kg,
	__global uint4 program[],
	uint program_size, __local uint4 local_program[], __global char *buffer1
) {
	event_t event = async_work_group_copy(local_program, program, program_size, 0);
	float4 res;

	int2 coords = (int2)(get_global_id(0), get_global_id(1));
	struct CompositorData __attribute__((aligned)) cd;

	cd.coord.x = coords.x/(float)host_kg.output_dimension.x;
	cd.coord.y = coords.y/(float)host_kg.output_dimension.y;

	struct KernelGlobals __attribute__((aligned)) kg;
	kg._program = local_program;
	kg.buffers[0] = buffer1;
	kg.num_samples = host_kg.num_samples;
	kg.frame_number = host_kg.frame_number;
	kg.texture_ptr.index = host_kg.texture_ptr.index;
	kg.texture_ptr.offset = host_kg.texture_ptr.offset;
	kg.viewport_ptr.index = host_kg.viewport_ptr.index;
	kg.viewport_ptr.offset = host_kg.viewport_ptr.offset;
	kg.aspect_ratio = host_kg.aspect_ratio;
	kg.seed = host_kg.seed + coords.x* 123 + coords.y *123567;
	kg.pixel_size = make_float2(1.0/(float)host_kg.output_dimension.x, 1.0/(float)host_kg.output_dimension.y);

	wait_group_events(1, &event);

	eval_program(kg, cd, &res);
	write_imagef(result, coords, res);
}
