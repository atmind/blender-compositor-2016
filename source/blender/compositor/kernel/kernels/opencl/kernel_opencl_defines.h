#define _KERNEL_OPENCL_
#define COMP_NAMESPACE_BEGIN
#define COMP_NAMESPACE_END

#define comp_device_global __global
#define comp_device_inline
#define comp_device_noinline
#define comp_device
#define comp_device_struct struct
#define comp_get_ref(var) *var
#define comp_in(type, var) type var
#define comp_out(type, var) type *var
#define comp_inout(type, var) type *var
#define comp_inout_array(type, var) type var
#define kernel_assert(cond)
#define UINT(value) ((uint)(value))
#define comp_attribute_packed __attribute__ ((packed))

#define ATTR_FALLTHROUGH ((void)0)
//#define COMP_OPENCL_COPY_TO_LOCAL
//#undef COMP_OPENCL_COPY_TO_LOCAL

