#ifndef __KERNELS_H__
#define __KERNELS_H__

#define KERNEL_NAME_JOIN(x, y, z) x ## _ ## y ## _ ## z
#define KERNEL_NAME_EVAL(arch)  KERNEL_NAME_JOIN(kernel, arch, compositor_sample)
#define KERNEL_FUNCTION_FULL_NAME KERNEL_NAME_EVAL(KERNEL_ARCH)

#define KERNEL_ARCH cpu
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#define KERNEL_ARCH cpu_sse2
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#define KERNEL_ARCH cpu_sse3
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#define KERNEL_ARCH cpu_sse41
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#define KERNEL_ARCH cpu_avx
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#define KERNEL_ARCH cpu_avx2
#include "kernel/kernels/cpu/kernel_cpu.h"
#undef KERNEL_ARCH

#endif
