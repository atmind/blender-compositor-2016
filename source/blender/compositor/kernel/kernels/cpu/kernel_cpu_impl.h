#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "util/util_optimization.h"
#include "kernel/kernel_compat_cpu.h"

#include "kernel/kernel.h"


COMP_NAMESPACE_BEGIN
void KERNEL_FUNCTION_FULL_NAME(comp_inout(KernelGlobals, kg), float x, float y, float* output) {
	CompositorData cd;
	// TODO: put X, Y in range -.5 and .5, use pixel size or width, height? aspect ratio?
	cd.coord.x = x;
	cd.coord.y = y;
	float4 result;
	eval_program(kg, cd, result);
	output[0] = result.x;
	output[1] = result.y;
	output[2] = result.z;
	output[3] = result.w;
}
COMP_NAMESPACE_END
