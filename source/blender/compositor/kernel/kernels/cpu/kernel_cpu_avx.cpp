#include "kernel/kernels/cpu/kernels.h"
#define _KERNEL_CPU_
#define __KERNEL_AVX__
#define KERNEL_ARCH cpu_avx
#include "kernel/kernels/cpu/kernel_cpu_impl.h"
