#ifndef _KERNEL_CPU_
  #define _KERNEL_CPU_
#endif

 #define ATTR_FALLTHROUGH ((void)0)

// TODO: where does cycles include assert.h
#include <assert.h>
#define comp_device_noinline static
#define comp_device_inline static inline
#define comp_device static inline
#define comp_addr_space
#define comp_device_struct
#define comp_get_ref(var) var
#define comp_in(type, var) type & var
#define comp_out(type, var) type & var
#define comp_inout(type, var) type & var
#define comp_inout_array(type, var) type var
#define comp_device_global

#define kernel_assert(cond) assert(cond)
#define comp_attribute_packed
#define UINT(value) (uint)value

// MacOS does not support ulong.
typedef unsigned long ulong;
