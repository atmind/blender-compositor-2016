#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "kernel/kernel_compat_cpu.h"
#include "kernel/kernel_globals.h"

COMP_NAMESPACE_BEGIN
void KERNEL_FUNCTION_FULL_NAME(comp_inout(KernelGlobals, kg), float x, float y, float * output);
COMP_NAMESPACE_END
