#include "kernel/kernels/cpu/kernels.h"
#define _KERNEL_CPU_
#define __KERNEL_SSE3__
#define KERNEL_ARCH cpu_sse3
#include "kernel/kernels/cpu/kernel_cpu_impl.h"
