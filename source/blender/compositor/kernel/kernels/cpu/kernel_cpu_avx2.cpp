#include "kernel/kernels/cpu/kernels.h"
#define _KERNEL_CPU_
#define __KERNEL_AVX2__
#define KERNEL_ARCH cpu_avx2
#include "kernel/kernels/cpu/kernel_cpu_impl.h"
