COMP_NAMESPACE_BEGIN

struct CompositorData {
	float2 coord;
	int sample_number;
};

struct Ray {
	float3 P;		/* origin */
	float3 D;		/* direction */
	// float3 up; /* up vector */
	float3 side; /* side vector */

	float store_float1; /* store to transfer data from ray manipulator to operation */
};

COMP_NAMESPACE_END
