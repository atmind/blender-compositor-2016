COMP_NAMESPACE_BEGIN

comp_device_inline float get_random_float(KernelGlobals * kg) //uniform between 0-1
{
  kg->seed = ((kg->seed) * 16807 ) % 2147483647;
  return  (float)(kg->seed) * 4.6566129e-10;
}

COMP_NAMESPACE_END
