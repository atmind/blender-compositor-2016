COMP_NAMESPACE_BEGIN

#define tex_fetch(type, info, pixel_offset) ((comp_device_global type*)(kg->buffers[info->data_ptr.index]+info->data_ptr.offset))[(pixel_offset)]
comp_device_inline comp_device_global comp_device_struct TextureInfo* kernel_tex_info(KernelGlobals *kg, uint id) {
	return &((comp_device_global comp_device_struct TextureInfo*)(kg->buffers[kg->texture_ptr.index]+kg->texture_ptr.offset))[id];
}

comp_device_inline int wrap_periodic(int x, int width)
{
	x %= width;
	if(x < 0)
		x += width;
	return x;
}

comp_device_inline int wrap_clamp(int x, int width)
{
	return clamp(x, 0, width-1);
}

comp_device_inline float frac(float x, int *ix)
{
	int i = float_to_int(x) - ((x < 0.0f)? 1: 0);
	*ix = i;
	return x - (float)i;
}


comp_device_inline float kernel_tex_image_interp(comp_device_struct KernelGlobals *kg, int id, float x, float y){
	comp_device_global comp_device_struct TextureInfo *info = kernel_tex_info(kg, id);
    int width = (int)info->width;
    int height = (int)info->height;

	int ix, iy, nix, niy;
	if (info->interpolation == INTERPOLATION_CLOSEST) {
		frac(x * width, &ix);
		frac(y * height, &iy);
		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0 || y > 1.0) {
					return 0.0f;
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return 0.0f;
		}
		return tex_fetch(float, info, ix +iy*width);
	}
	else if(info->interpolation == INTERPOLATION_LINEAR) {
		float tx = frac(x*width - 0.5f, &ix);
		float ty = frac(y*height - 0.5f, &iy);

		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);

				nix = wrap_periodic(ix+1, width);
				niy = wrap_periodic(iy+1, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0f || y > 1.0f) {
					return 0.0f;
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				nix = wrap_clamp(ix+1, width);
				niy = wrap_clamp(iy+1, height);

				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return 0.0f;
		}

		float r = (1.0f - ty)*(1.0f - tx)*tex_fetch(float, info, ix + iy*width);
		r += (1.0f - ty)*tx*tex_fetch(float, info, nix + iy*width);
		r += ty*(1.0f - tx)*tex_fetch(float, info, ix + niy*width);
		r += ty*tx*tex_fetch(float, info, nix + niy*width);

		return r;
	}
	else {
		/* Bicubic b-spline interpolation. */
		float tx = frac(x*width - 0.5f, &ix);
		float ty = frac(y*height - 0.5f, &iy);
		int pix, piy, nnix, nniy;
		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);

				pix = wrap_periodic(ix-1, width);
				piy = wrap_periodic(iy-1, height);

				nix = wrap_periodic(ix+1, width);
				niy = wrap_periodic(iy+1, height);

				nnix = wrap_periodic(ix+2, width);
				nniy = wrap_periodic(iy+2, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0f || y > 1.0f) {
					return 0.0f;
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				pix = wrap_clamp(ix-1, width);
				piy = wrap_clamp(iy-1, height);

				nix = wrap_clamp(ix+1, width);
				niy = wrap_clamp(iy+1, height);

				nnix = wrap_clamp(ix+2, width);
				nniy = wrap_clamp(iy+2, height);

				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return 0.0f;
		}

		const int xc[4] = {pix, ix, nix, nnix};
		const int yc[4] = {width * piy,
											 width * iy,
											 width * niy,
											 width * nniy};
		float u[4], v[4];
		/* Some helper macro to keep code reasonable size,
		 * let compiler to inline all the matrix multiplications.
		 */
#define DATA(x, y) (tex_fetch(float, info, xc[x] + yc[y]))
#define TERM(col) \
		(v[col] * (u[0] * DATA(0, col) + \
							 u[1] * DATA(1, col) + \
							 u[2] * DATA(2, col) + \
							 u[3] * DATA(3, col)))
#define SET_CUBIC_SPLINE_WEIGHTS(u, t) \
	{ \
		u[0] = (((-1.0f/6.0f)* t + 0.5f) * t - 0.5f) * t + (1.0f/6.0f); \
		u[1] =  ((      0.5f * t - 1.0f) * t       ) * t + (2.0f/3.0f); \
		u[2] =  ((     -0.5f * t + 0.5f) * t + 0.5f) * t + (1.0f/6.0f); \
		u[3] = (1.0f / 6.0f) * t * t * t; \
	} (void)0

		SET_CUBIC_SPLINE_WEIGHTS(u, tx);
		SET_CUBIC_SPLINE_WEIGHTS(v, ty);

		/* Actual interpolation. */
		return TERM(0) + TERM(1) + TERM(2) + TERM(3);

#undef SET_CUBIC_SPLINE_WEIGHTS
#undef TERM
#undef DATA
	}


	return 0.0f;
}

comp_device_inline float4 kernel_tex_image_interp_4(comp_device_struct KernelGlobals *kg, int id, float x, float y){
	comp_device_global comp_device_struct TextureInfo *info = kernel_tex_info(kg, id);
    int width = (int)info->width;
    int height = (int)info->height;

	int ix, iy, nix, niy;
	if (info->interpolation == INTERPOLATION_CLOSEST) {
		frac(x*width, &ix);
		frac(y*height, &iy);
		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0f || y > 1.0f) {
					return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		}
		return tex_fetch(float4, info, ix +iy*width);
	}
	else if(info->interpolation == INTERPOLATION_LINEAR) {
		float tx = frac(x*width - 0.5f, &ix);
		float ty = frac(y*height - 0.5f, &iy);

		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);

				nix = wrap_periodic(ix+1, width);
				niy = wrap_periodic(iy+1, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0f || y > 1.0f) {
					return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				nix = wrap_clamp(ix+1, width);
				niy = wrap_clamp(iy+1, height);

				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		}

		float4 r = (1.0f - ty)*(1.0f - tx)*tex_fetch(float4, info, ix + iy*width);
		r += (1.0f - ty)*tx*tex_fetch(float4, info, nix + iy*width);
		r += ty*(1.0f - tx)*tex_fetch(float4, info, ix + niy*width);
		r += ty*tx*tex_fetch(float4, info, nix + niy*width);

		return r;
	}
	else {
		/* Bicubic b-spline interpolation. */
		float tx = frac(x*width - 0.5f, &ix);
		float ty = frac(y*height - 0.5f, &iy);
		int pix, piy, nnix, nniy;
		switch(info->extension) {
			case EXTENSION_REPEAT:
				ix = wrap_periodic(ix, width);
				iy = wrap_periodic(iy, height);

				pix = wrap_periodic(ix-1, width);
				piy = wrap_periodic(iy-1, height);

				nix = wrap_periodic(ix+1, width);
				niy = wrap_periodic(iy+1, height);

				nnix = wrap_periodic(ix+2, width);
				nniy = wrap_periodic(iy+2, height);
				break;
			case EXTENSION_CLIP:
				if(x < 0.0f || y < 0.0f || x > 1.0f || y > 1.0f) {
					return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
				}
				ATTR_FALLTHROUGH;
			case EXTENSION_EXTEND:
				pix = wrap_clamp(ix-1, width);
				piy = wrap_clamp(iy-1, height);

				nix = wrap_clamp(ix+1, width);
				niy = wrap_clamp(iy+1, height);

				nnix = wrap_clamp(ix+2, width);
				nniy = wrap_clamp(iy+2, height);

				ix = wrap_clamp(ix, width);
				iy = wrap_clamp(iy, height);
				break;
			default:
				kernel_assert(0);
				return make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		}

		const int xc[4] = {pix, ix, nix, nnix};
		const int yc[4] = {width * piy,
											 width * iy,
											 width * niy,
											 width * nniy};
		float u[4], v[4];
		/* Some helper macro to keep code reasonable size,
		 * let compiler to inline all the matrix multiplications.
		 */
#define DATA(x, y) (tex_fetch(float4, info, xc[x] + yc[y]))
#define TERM(col) \
		(v[col] * (u[0] * DATA(0, col) + \
							 u[1] * DATA(1, col) + \
							 u[2] * DATA(2, col) + \
							 u[3] * DATA(3, col)))
#define SET_CUBIC_SPLINE_WEIGHTS(u, t) \
	{ \
		u[0] = (((-1.0f/6.0f)* t + 0.5f) * t - 0.5f) * t + (1.0f/6.0f); \
		u[1] =  ((      0.5f * t - 1.0f) * t       ) * t + (2.0f/3.0f); \
		u[2] =  ((     -0.5f * t + 0.5f) * t + 0.5f) * t + (1.0f/6.0f); \
		u[3] = (1.0f / 6.0f) * t * t * t; \
	} (void)0

		SET_CUBIC_SPLINE_WEIGHTS(u, tx);
		SET_CUBIC_SPLINE_WEIGHTS(v, ty);

		/* Actual interpolation. */
		return TERM(0) + TERM(1) + TERM(2) + TERM(3);

#undef SET_CUBIC_SPLINE_WEIGHTS
#undef TERM
#undef DATA
	}


	return make_float4(1.0f, 0.5f, 0.25f, 1.0f);
}

COMP_NAMESPACE_END
