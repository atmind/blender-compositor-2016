uint4 node1;
read_program_line(node1);

float size_scale = stack_load_float(node1.z);
float size_x = uint_as_float(node1.x) * size_scale;
//float size_y = uint_as_float(node1.y) * size_scale;
float len = get_random_float(&kg);
ray_stack[ray_offset].P = ray_stack[input_ray_offset].P;

//Bending the direction input ray
float3 direction = ray_stack[input_ray_offset].D;
// TODO: size_x is not correct.
float3 d1 = rotate_around_axis(direction, ray_stack[input_ray_offset].side, size_x * len);
float3 d2 = rotate_around_axis(d1, direction, M_2PI_F * get_random_float(&kg));
ray_stack[ray_offset].D = d2;
ray_stack[ray_offset].store_float1 = len;
