uint4 data;
float4 colora;
float4 colorb;
float3 colora3;
float3 colorb3;
float value;

read_program_line(data);

colora = stack_load_float4(data.x);
colorb = stack_load_float4(data.y);
colora3 = make_float3(colora.x, colora.y, colora.z);
colorb3 = make_float3(colorb.x, colorb.y, colorb.z);
value = stack_load_float(data.z);
float3 mixed_color3;

float alpha = colora.w;
if (premultiply_alpha) {
	alpha *= colorb.w;
}

// TODO: move to more generic methods. see svm_color_util.h
switch (mix_type) {
	case MIX_NODE_ADD:
		mixed_color3 = mix(colora3, colora3 + colorb3, value);
		break;
	case MIX_NODE_SUB:
		mixed_color3 = mix(colora3, colora3 - colorb3, value);
		break;
	case MIX_NODE_MULT:
		mixed_color3 = mix(colora3, colora3 * colorb3, value);
		break;
	case MIX_NODE_DIFF:
		mixed_color3 = mix(colora3, fabs(colora3 - colorb3), value);
		break;
	case MIX_NODE_BLEND:
	default:
		mixed_color3 = mix(colora3, colorb3, value);
		break;
}
if (data.w) {
	mixed_color3 = clamp(mixed_color3, 0.0f, 1.0f);
}


float4 mixed_color4 = make_float4(mixed_color3.x,mixed_color3.y,mixed_color3.z, alpha);
stack_store_float4(result_offset, mixed_color4);
stack_store_float(result_offset, 1.0);
