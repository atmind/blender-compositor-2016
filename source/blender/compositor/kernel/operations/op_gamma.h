float4 color = stack_load_float4(program_line.z);
float gamma = stack_load_float(program_line.w);

if (color.x > 0.0f) {
	color.x = pow(color.x, gamma);	
}
if (color.y > 0.0f) {
	color.y = pow(color.y, gamma);	
}
if (color.z > 0.0f) {
	color.z = pow(color.z, gamma);	
}

stack_store_float4(result_offset, color);
