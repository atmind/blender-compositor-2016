#define colorbalance_cdl(in, offset, power, slope)\
{\
	float x = in * slope + offset;\
	if (x < 0.0f) x = 0.0f; \
	in = pow(x, power);\
}

float4 offset;
float4 power;
float4 slope;

read_program_line_float(offset);
read_program_line_float(power);
read_program_line_float(slope);

float value = stack_load_float(program_line.z);
float4 color = stack_load_float4(program_line.w);
float4 result;

result = color;
colorbalance_cdl(result.x, offset.x, power.x, slope.x);
colorbalance_cdl(result.y, offset.y, power.y, slope.y);
colorbalance_cdl(result.z, offset.z, power.z, slope.z);

stack_store_float4(result_offset, mix(color, result, value));

#undef colorbalance_cdl