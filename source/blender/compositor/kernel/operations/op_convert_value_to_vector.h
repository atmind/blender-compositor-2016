
float value = stack_load_float(program_line.z);
float3 vector = make_float3(value, value, value);
stack_store_float3(result_offset, vector);
