uint4 data;
read_program_line(data);

float4 result = make_float4(
	stack_load_float(data.x),
	stack_load_float(data.y),
	stack_load_float(data.z),
	stack_load_float(data.w)
);
stack_store_float4(result_offset, result);