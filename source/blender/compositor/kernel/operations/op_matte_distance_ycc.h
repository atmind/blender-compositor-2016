float4 data;
read_program_line_float(data);
const float tolerance = data.x;
const float falloff = data.y;

float4 color = stack_load_float4(input_color_offset);
float4 color_ycc = linear_to_ycc_itu_bt601(color);
float4 color_key_ycc = linear_to_ycc_itu_bt601(stack_load_float4(input_key_offset));

float distance = length_v2(make_float2(color_key_ycc.y, color_key_ycc.z) - make_float2(color_ycc.y, color_ycc.z));
color.w = clamp((distance - tolerance) / falloff, 0.0f , color.w);

stack_store_float4(result_offset, color);
stack_store_float(result_offset, color.w);