
uint4 data;
read_program_line(data);
float value1 = stack_load_float(data.x);
float value2 = stack_load_float(data.y);
float result;

switch (operation_type) {
	case MATH_ADD:
	{
		result = value1 + value2;
		break;
	}

	case MATH_SUB:
	{
		result = value1 - value2;
		break;
	}

	case MATH_MUL:
	{
		result = value1 * value2;
		break;
	}

	case MATH_DIVIDE:
	{
		if (value2 != 0.0) {

			result = value1 / value2;
		}
		else {
			result = 0.0;

		}
		break;
	}

	case MATH_SIN:
	{
		result = sin(value1);
		break;
	}

	case MATH_COS:
	{
		result = cos(value1);
		break;
	}

	case MATH_TAN:
	{
		result = tan(value1);
		break;
	}

	case MATH_ASIN:
	{
		result = asin(value1);
		break;
	}

	case MATH_ACOS:
	{
		result = acos(value1);
		break;
	}

	case MATH_ATAN:
	{
		result = atan(value1);
		break;
	}

	case MATH_POW:
	{
		if (value1 >= 0.0) {
			result = pow(value1, value2);

		}
		else {
			float y_mod_1 = mod(value2, 1);
			/* if input value is not nearly an integer, fall back to zero, nicer than straight rounding */
			if (y_mod_1 > 0.999f || y_mod_1 < 0.001f) {
				result = pow(value1, floor(value2 + 0.5f));

			}
			else {
				result = 0.0;

			}
		}
		break;
	}

	case MATH_LOG:
	{
		if (value1 > 0.0 && value2 > 0.0) {
			result = log(value1) / log(value2);
		}
		else {
			result = 0.0;
		}
		break;
	}

	case MATH_MIN:
	{
		result = min(value1, value2);
		break;
	}

	case MATH_MAX:
	{
		result = max(value1, value2);
		break;
	}

	case MATH_ROUND:
	{
		result = round(value1);
		break;
	}

	case MATH_LESS:
	{
		result = value1 < value2? 1.0f: 0.0f;
		break;
	}

	case MATH_GREATER:
	{
		result = value1 > value2? 1.0f: 0.0f;
		break;
	}

	case MATH_MOD:
	{
		if (value2 == 0.0f) {
			result = 0.0f;

		}
		else {
			result = mod(value1, value2);
		}
		break;
	}

	case MATH_ABS:
	{
		result = abs(value1);
	}

	default:
	{
		result = -1;
		break;
	}
}

// Clamping
if (program_line.w) {
	result = clamp(result, 0.0f, 1.0f);

}

stack_store_float(result_offset, result);
