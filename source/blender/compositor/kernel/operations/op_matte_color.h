float4 node1;
read_program_line_float(node1);
float4 color = stack_load_float4(input_color_offset);
float4 key_color = stack_load_float4(input_key_offset);

float4 color_hsv = linear_to_hsv(color);
float4 key_color_hsv = linear_to_hsv(key_color);

float h_wrap;
float value;
if (
		/* do hue last because it needs to wrap, and does some more checks  */

		/* sat */ (fabs(color_hsv.y - key_color_hsv.y) < node1.y) &&
		/* val */ (fabs(color_hsv.z - key_color_hsv.z) < node1.z) &&

		/* multiply by 2 because it wraps on both sides of the hue,
		 * otherwise 0.5 would key all hue's */

		/* hue */ ((h_wrap = 2.0f * fabs(color_hsv.x - key_color_hsv.x)) < node1.x || (2.0f - h_wrap) < node1.x)
		)
{
	value = 0.0f; /* make transparent */
}
else {
	value = color.w;
}
color.w = value;

stack_store_float4(result_offset, color);
stack_store_float(result_offset, value);
