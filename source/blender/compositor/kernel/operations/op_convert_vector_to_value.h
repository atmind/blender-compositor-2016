float3 vector = stack_load_float3(program_line.z);
float value = (vector.x + vector.y + vector.z) / 3.0f;
stack_store_float(result_offset, value);
