#define colorbalance_lgg(in, lift_lgg, gamma_inv, gain)\
{\
	float x = (((_linear_to_srgb(in) - 1.0f) * lift_lgg) + 1.0f) * gain;\
	if (x < 0.0f) x = 0.0f; \
	in = pow(_srgb_to_linear(x), gamma_inv);\
}

float4 lift_lgg;
float4 gamma_inv;
float4 gain;

read_program_line_float(lift_lgg);
read_program_line_float(gamma_inv);
read_program_line_float(gain);

float value = stack_load_float(program_line.z);
float4 color = stack_load_float4(program_line.w);
float4 result;

result = color;
colorbalance_lgg(result.x, lift_lgg.x, gamma_inv.x, gain.x);
colorbalance_lgg(result.y, lift_lgg.y, gamma_inv.y, gain.y);
colorbalance_lgg(result.z, lift_lgg.z, gamma_inv.z, gain.z);

stack_store_float4(result_offset, mix(color, result, value));

#undef colorbalance_lgg