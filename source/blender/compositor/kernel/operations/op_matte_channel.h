uint4 data;
float4 dataf;
read_program_line(data);
read_program_line_float(dataf);

float4 color = stack_load_float4(input_color_offset);

const uint color_space = data.x;
const uint channel = data.y;
const uint method = data.z;
const uint limit_channel = data.w;

const float limit_max = dataf.x;
const float limit_min = dataf.y;
const float limit_range = dataf.z;

float4 converted_color;
switch (color_space) {
	case CHANNEL_MATTE_CS_RGB:
	{
		converted_color = color;
		break;
	}
	case CHANNEL_MATTE_CS_HSV: 
	{
		converted_color = linear_to_hsv(color);
		break;
	}
	case CHANNEL_MATTE_CS_YUV: 
	{
		converted_color = linear_to_yuv(color);
		break;
	}
	case CHANNEL_MATTE_CS_YCC: 
	{
		converted_color = linear_to_ycc_itu_bt601(color);
		break;
	}
}
float channel_value;
float limit_value1;
float limit_value2;

switch (method) {
	case 0: /* Single */
	{
		channel_value = component(converted_color, channel);
		limit_value1 = limit_value2 = component(converted_color, limit_channel);
		break;
	}
	
	case 1: /* MAX */
	{
		channel_value = component(converted_color, channel);
		limit_value1 = component(converted_color, (channel+1) % 3);
		limit_value2 = component(converted_color, (channel+2) % 3);
		break;
	}
}

float alpha = 1.0 - (channel_value - max(limit_value1, limit_value1));
if (alpha > limit_max) {
	alpha = color.w;
}
else if (alpha < limit_min) {
	alpha = 0.0f;
}
else {
	alpha = (alpha - limit_min) / limit_range;
}

color.w = alpha;
stack_store_float4(result_offset, color);
stack_store_float(result_offset, color.w);
