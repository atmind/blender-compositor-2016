
float value = stack_load_float(program_line.z);
float4 color = make_float4(value, value, value, 1.0);
stack_store_float4(result_offset, color);
