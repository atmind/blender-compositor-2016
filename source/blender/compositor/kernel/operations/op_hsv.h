uint4 data;
read_program_line(data);
float4 color = stack_load_float4(program_line.z);
float h = stack_load_float(data.x);
float s = stack_load_float(data.y);
float v = stack_load_float(data.z);
float value = stack_load_float(data.w);

float4 hsv = linear_to_hsv(color);

hsv.x = hsv.x + (h-0.5f);
if (hsv.x > 1.0f) hsv.x -= 1.0f;
if (hsv.x < 0.0f) hsv.x += 1.0f;
hsv.y *= s;
hsv.z *= v;
color = mix(color, hsv_to_linear(hsv), value);

stack_store_float4(result_offset, color);
