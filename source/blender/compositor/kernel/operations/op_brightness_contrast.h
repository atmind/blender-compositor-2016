uint4 data;
read_program_line(data);

float4 color = stack_load_float4(data.x);
float brightness = stack_load_float(data.y) / 100.0f;
float contrast = stack_load_float(data.z);
float delta = contrast / 200.0f;
float a, b;
float alpha = color.w;

a = 1.0f - delta * 2.0f;
/*
 * The algorithm is by Werner D. Streidt
 * (http://visca.com/ffactory/archives/5-99/msg00021.html)
 * Extracted of OpenCV demhist.c
 */
if (contrast > 0) {
	a = 1.0f / a;
	b = a * (brightness - delta);
}
else {
	delta *= -1;
	b = a * (brightness + delta);
}

color = color * a + b;
color.w = alpha;

// TODO: premul
stack_store_float4(result_offset, color);
