#define GAUSSFAC 1.6
#define TWO_GAUSSFAC2 (2.0 * GAUSSFAC * GAUSSFAC)
uint4 node1;
read_program_line(node1);
float4 color1 = stack_load_float4(node1.x);
float4 color2;

// Read the ray float1 that is used for sampling the Image input
float len = ray_stack[node1.y].store_float1;
float value = 0.0;
switch (node1.z) {
	 case FALLOFF_FILTER_GAUSS:
	 {
	 	float x = len * 3.0f * GAUSSFAC;
	 	value = 1.0f / sqrt((float)M_PI * TWO_GAUSSFAC2) * exp(-x*x / TWO_GAUSSFAC2);
	 	break;
	 }

	case FALLOFF_FILTER_TENT:
	{
		value = 1.0 - len;
		break;
	}

	case FALLOFF_FILTER_BOX:
	{
		value = 1.0;
		break;
	}

	default:
	{
		value = 0.0;
		break;
	}
}
color2 = color1 * value;
color2.w = color1.w;

stack_store_float4(result_offset, color2);
stack_store_float(result_offset, 1.0);
