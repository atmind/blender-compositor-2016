float4 data;
read_program_line_float(data);

float4 color = stack_load_float4(input_color_offset);
float4 key_color = stack_load_float4(input_key_offset);

float4 color_ycc = linear_to_ycc_itu_bt601(color);
float4 key_color_ycc = linear_to_ycc_itu_bt601(key_color);

float alpha;

float acceptance = data.x;
float cutoff = data.y;
float gain = data.z;

color_ycc = (color_ycc * 2.0f) - 1.0f;
key_color_ycc = (key_color_ycc * 2.0f) - 1.0f;

float theta = atan2(key_color_ycc.z, key_color_ycc.y);
float x_angle = color_ycc.y * cos(theta) + color_ycc.z * sin(theta);
float z_angle = color_ycc.z * cos(theta) - color_ycc.y * sin(theta);
float kfg = x_angle- (fabs(z_angle) / tan(acceptance / 2.0f));

if (kfg > 0.0f) {
	alpha = 1.0f - (kfg / gain);
	float beta = atan2(z_angle, x_angle);
	
	if (fabs(beta) < (cutoff / 2.0f)) {
		alpha = 0.0f;
	}
	
	alpha = min(alpha, color.w);
}
else 
{
	alpha = color.w;
}

color.w = alpha;

stack_store_float4(result_offset, color);
stack_store_float(result_offset, color.w);
