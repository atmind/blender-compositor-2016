float3 vector = stack_load_float3(program_line.z);
stack_store_float4(result_offset, make_float4(vector.x, vector.y, vector.z, 1.0f));