uint4 data;
float4 colorBackground;
float4 colorForeground;
float4 result;
float value;

read_program_line(data);

colorBackground = stack_load_float4(data.x);
colorForeground = stack_load_float4(data.y);
value = stack_load_float(data.z);


if (colorForeground.w <= 0.0f) {
	result = colorBackground;
}
else if (value == 1.0f && colorForeground.w >= 1.0f) {
	result = colorForeground;
}
else {
	float premul = value * colorForeground.w;
	float mul = 1.0f - premul;
	result = mul*colorBackground + premul*colorForeground;
	result.w = mul*colorBackground.w + value * colorForeground.w;
}

stack_store_float4(result_offset, result);
stack_store_float(result_offset, 1.0);
