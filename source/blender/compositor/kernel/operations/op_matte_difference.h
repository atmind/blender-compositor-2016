float4 data;
read_program_line_float(data);
const float tolerance = data.x;
const float falloff = data.y;

float4 color = stack_load_float4(input_color_offset);
float4 color_key = stack_load_float4(input_key_offset);

float4 tmp = fabs(color_key - color);
float difference = (tmp.x + tmp.y + tmp.z) / 3.0f;
color.w = clamp((difference - tolerance) / falloff, 0.0f , color.w);

stack_store_float4(result_offset, color);
stack_store_float(result_offset, color.w);