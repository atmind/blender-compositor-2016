#ifndef _OPERATIONS_H_
#define _OPERATIONS_H_

#include "kernel/operations/op_types.h"

COMP_NAMESPACE_BEGIN
#define stack_load_float4(a) stack4[a]
#define stack_store_float4(a, f) stack4[a] = f
// NOTE: vectors are stored in the stack4
#define stack_load_float3(a) make_float3(stack4[a].x, stack4[a].y, stack4[a].z)
#define stack_store_float3(a, f) stack4[a] = make_float4(f.x, f.y, f.z, 0.0f);
#define stack_load_float(a) stack[a]
#define stack_store_float(a, f) stack[a] = f

#define read_program_line(result) result = kg._program[ip++]
#define read_program_line_float(result) { uint4 _program_line; read_program_line(_program_line); result = make_float4(uint_as_float(_program_line.x), uint_as_float(_program_line.y), uint_as_float(_program_line.z), uint_as_float(_program_line.w)); }

comp_device_inline void eval_program(
	comp_in(comp_device_struct KernelGlobals, kg), comp_in(comp_device_struct CompositorData, cd), comp_out(float4, result)
) {
	float4 stack4[CVM_STACK_SIZE];
	float stack[CVM_STACK_SIZE];
	comp_device_struct Ray ray_stack[CVM_RAY_STACK_SIZE];
	float total_weight = 0.0;
	comp_get_ref(result) = make_float4(0.0, 0.0, 0.0, 0.0);
	for (int _sample_number = 0 ; _sample_number < kg.num_samples; _sample_number ++) {
		cd.sample_number = _sample_number;
		// Instruction pointer of the program.
		int ip = 0;
		bool program_not_finished = true;
		while (program_not_finished) {
			uint4 program_line;
			read_program_line(program_line);
			/*
				program_line.x = operation type (uint)
				program_line.y = result_offset (uint)
			*/
			uint result_offset = program_line.y;
			switch(program_line.x) {
				case OP_END:
					program_not_finished = false;
					break;
				case OP_COLOR:
				{
#include "kernel/operations/op_color.h"
					break;
				}

				case OP_VALUE:
				{
					float value = uint_as_float(program_line.z);
#include "kernel/operations/op_value.h"
					break;
				}

				case OP_OUTPUT_RAY:
				{
					uint ray_offset = program_line.y;
					uint viewport_id = program_line.z;
#include "kernel/operations/op_output_ray.h"
					break;
				}

				case OP_OUTPUT:
				{
#include "kernel/operations/op_output.h"
					break;
				}

				case OP_MIX:
				{
					uint mix_type = program_line.z;
					uint premultiply_alpha = program_line.w;
#include "kernel/operations/op_mix.h"
					break;
				}

				case OP_IMAGE_1:
				{
					uint ray_offset = program_line.z;
					uint texture_slot = program_line.w;
#include "kernel/operations/op_image_float1.h"
					break;
				}

				case OP_IMAGE_4:
				{
					uint ray_offset = program_line.z;
					uint texture_slot = program_line.w;
#include "kernel/operations/op_image_float4.h"
					break;
				}

				case OP_BLUR_RAY:
				{
					uint ray_offset = program_line.y;
					uint input_ray_offset = program_line.z;
#include "kernel/operations/op_blur_ray.h"
					break;
				}

				case OP_BLUR:
				{
#include "kernel/operations/op_blur.h"
					break;
				}

				case OP_ALPHA_OVER:
				{
#include "kernel/operations/op_alpha_over.h"
					break;
				}

				case OP_COLOR_MATTE:
				{
					uint input_color_offset = program_line.z;
					uint input_key_offset = program_line.w;
#include "kernel/operations/op_matte_color.h"
					break;
				}

				case OP_CHROMA_MATTE:
				{
					uint input_color_offset = program_line.z;
					uint input_key_offset = program_line.w;
#include "kernel/operations/op_matte_chroma.h"
					break;
				}

				case OP_CHANNEL_MATTE:
				{
					uint input_color_offset = program_line.z;
#include "kernel/operations/op_matte_channel.h"
					break;
				}

				case OP_DIFFERENCE_MATTE:
				{
					uint input_color_offset = program_line.z;
					uint input_key_offset = program_line.w;
#include "kernel/operations/op_matte_difference.h"
					break;
				}

				case OP_DISTANCE_MATTE_LINEAR:
				{
					uint input_color_offset = program_line.z;
					uint input_key_offset = program_line.w;
#include "kernel/operations/op_matte_distance_linear.h"
					break;
				}

				case OP_DISTANCE_MATTE_YCC:
				{
					uint input_color_offset = program_line.z;
					uint input_key_offset = program_line.w;
#include "kernel/operations/op_matte_distance_ycc.h"
					break;
				}

				case OP_LUMINANCE_MATTE:
				{
					uint input_color_offset = program_line.z;
#include "kernel/operations/op_matte_luminance.h"
					break;
				}

				case OP_COLOR_SPILL:
				{
					uint input_color_offset = program_line.z;
					uint input_value_offset = program_line.w;
#include "kernel/operations/op_color_spill.h"
					break;
				}

				case OP_SET_ALPHA:
				{
					uint input_color_offset = program_line.z;
					uint input_value_offset = program_line.w;
#include "kernel/operations/op_set_alpha.h"
					break;
				}

				case OP_MATH:
				{
					uint operation_type = program_line.z;
#include "kernel/operations/op_math.h"
					break;
				}

				/* CONVERTERS */
				case OP_VALUE_TO_COLOR:
				{
#include "kernel/operations/op_convert_value_to_color.h"
					break;
				}

				case OP_VALUE_TO_VECTOR:
				{
#include "kernel/operations/op_convert_value_to_vector.h"
					break;
				}

				case OP_VECTOR_TO_VALUE:
				{
#include "kernel/operations/op_convert_vector_to_value.h"
					break;
				}

				case OP_VECTOR_TO_COLOR:
				{
#include "kernel/operations/op_convert_vector_to_color.h"
					break;
				}

				case OP_COLOR_TO_VALUE:
				{
#include "kernel/operations/op_convert_color_to_value.h"
					break;
				}

				case OP_COLOR_TO_VECTOR:
				{
#include "kernel/operations/op_convert_color_to_vector.h"
					break;
				}


				case OP_SEPARATE_R:
				{
#include "kernel/operations/op_separate_r.h"
					break;
				}

				case OP_SEPARATE_G:
				{
#include "kernel/operations/op_separate_g.h"
					break;
				}

				case OP_SEPARATE_B:
				{
#include "kernel/operations/op_separate_b.h"
					break;
				}

				case OP_SEPARATE_A:
				{
#include "kernel/operations/op_separate_a.h"
					break;
				}

				case OP_SEPARATE_H:
				{
#include "kernel/operations/op_separate_h.h"
					break;
				}

				case OP_SEPARATE_S:
				{
#include "kernel/operations/op_separate_s.h"
					break;
				}

				case OP_SEPARATE_V:
				{
#include "kernel/operations/op_separate_v.h"
					break;
				}

				case OP_SEPARATE_YUV_Y:
				{
#include "kernel/operations/op_separate_yuv_y.h"
					break;
				}

				case OP_SEPARATE_YUV_U:
				{
#include "kernel/operations/op_separate_yuv_u.h"
					break;
				}

				case OP_SEPARATE_YUV_V:
				{
#include "kernel/operations/op_separate_yuv_v.h"
					break;
				}

				case OP_COMBINE_RGBA:
				{
#include "kernel/operations/op_combine_rgba.h"
					break;
				}

				case OP_COMBINE_HSVA:
				{
#include "kernel/operations/op_combine_hsva.h"
					break;
				}

				case OP_HSV:
				{
#include "kernel/operations/op_hsv.h"
					break;
				}

				case OP_BRIGHTNESS_CONTRAST:
				{
#include "kernel/operations/op_brightness_contrast.h"
					break;
				}

				case OP_COLORBALANCE_LGG:
				{
#include "kernel/operations/op_colorbalance_lgg.h"
					break;
				}

				case OP_COLORBALANCE_CDL:
				{
#include "kernel/operations/op_colorbalance_cdl.h"
					break;
				}

				default:
				{
					kernel_assert(!"Unknown operation type was read");
					return;
				}
			}
		}
	}
	comp_get_ref(result) /= total_weight;
}
COMP_NAMESPACE_END
#endif
