float4 color = stack_load_float4(input_color_offset);
float alpha = stack_load_float(input_value_offset);
color.w = alpha;
stack_store_float4(result_offset, make_float4(
	color.x, color.y, color.z, alpha
));