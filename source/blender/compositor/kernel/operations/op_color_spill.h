uint4 data;
float4 uspill;

read_program_line(data);
read_program_line_float(uspill);

float4 color = stack_load_float4(input_color_offset);
float value = min(1.0f, stack_load_float(input_value_offset));

float spill_value;
float channel2;
float channel3;
float lim_value;
float4 mut;
const float lim_scale = uspill.w;

/* Spill channel */
switch (data.y) {
	case 0: /* R */
	{
		mut = make_float4(-1.0f, 1.0f, 1.0f, 0.0f);
		spill_value = color.x;
		channel2 = color.y;
		channel3 = color.z;
		break;
	}
	case 1: /* G */
	{
		mut = make_float4(1.0f, -1.0f, 1.0f, 0.0f);
		spill_value = color.y;
		channel2 = color.x;
		channel3 = color.z;
		break;
	}
	case 2: /* B */
	{
		mut = make_float4(1.0f, 1.0f, -1.0f, 0.0f);
		spill_value = color.z;
		channel2 = color.x;
		channel3 = color.y;
		break;
	}
}

switch (data.x) { /* method */
	case 0: {
		switch (data.z) { /* lim channel */
			case 0: {
				lim_value = color.x;
				break;
			}
			case 1: {
				lim_value = color.y;
				break;
			}
			case 2: {
				lim_value = color.z;
				break;
			}
		}

		break;
	}
	case 1: /* average */
	{
		lim_value = (channel2 + channel3) / 2.0f;
		break;
	}
}
float map = value * (spill_value - (lim_scale * lim_value));

if (map > 0.0f) {
	float alpha = color.w;
	color = color + mut * uspill * map;
	color.w = alpha;
}

stack_store_float4(result_offset, color);