float4 data;
read_program_line_float(data);
const float high = data.x;
const float low = data.y;

float4 color = stack_load_float4(input_color_offset);
float4 color_yuv = linear_to_yuv(color);

color.w = clamp((color_yuv.x - low) / (high - low), 0.0f, color.w);

stack_store_float4(result_offset, color);
stack_store_float(result_offset, color.w);