uint4 data;
read_program_line(data);
int viewport_id = data.x;
comp_device_struct Ray ray = ray_stack[ray_offset];
float2 uv = ray_to_viewport(kg, &ray, viewport_id);
float4 color = kernel_tex_image_interp_4(&kg, texture_slot, uv.x, uv.y);

stack_store_float4(result_offset, color);
