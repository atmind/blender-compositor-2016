/*
 * Copyright 2011-2017 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_MATH_FLOAT2_H_
#define _UTIL_MATH_FLOAT2_H_

#ifndef _UTIL_MATH_H_
#  error "Do not include this file directly, include util_types.h instead."
#endif

COMP_NAMESPACE_BEGIN

/*******************************************************************************
 * Declaration.
 */

#ifdef _KERNEL_CPU_
comp_device_inline float2 operator-(const float2& a);
comp_device_inline float2 operator*(const float2& a, const float2& b);
comp_device_inline float2 operator*(const float2& a, float f);
comp_device_inline float2 operator*(float f, const float2& a);
comp_device_inline float2 operator/(float f, const float2& a);
comp_device_inline float2 operator/(const float2& a, float f);
comp_device_inline float2 operator/(const float2& a, const float2& b);
comp_device_inline float2 operator+(const float2& a, const float2& b);
comp_device_inline float2 operator+(const float2& a, const float b);
comp_device_inline float2 operator-(const float2& a, const float2& b);
comp_device_inline float2 operator+=(float2& a, const float2& b);
comp_device_inline float2 operator*=(float2& a, const float2& b);
comp_device_inline float2 operator*=(float2& a, float f);
comp_device_inline float2 operator/=(float2& a, const float2& b);
comp_device_inline float2 operator/=(float2& a, float f);

comp_device_inline bool operator==(const float2& a, const float2& b);
comp_device_inline bool operator!=(const float2& a, const float2& b);

comp_device_inline bool is_zero(const float2& a);
comp_device_inline float average(const float2& a);
comp_device_inline float dot(const float2& a, const float2& b);
comp_device_inline float cross(const float2& a, const float2& b);
comp_device_inline float len(const float2& a);
comp_device_inline float2 normalize(const float2& a);
comp_device_inline float2 normalize_len(const float2& a, float *t);
comp_device_inline float2 safe_normalize(const float2& a);
comp_device_inline float2 min(const float2& a, const float2& b);
comp_device_inline float2 max(const float2& a, const float2& b);
comp_device_inline float2 clamp(const float2& a, const float2& mn, const float2& mx);
comp_device_inline float2 fabs(const float2& a);
comp_device_inline float2 as_float2(const float4& a);
comp_device_inline float2 mix(const float2& a, const float2& b, float t);
#endif /* _KERNEL_CPU_ */

/*******************************************************************************
 * Definition.
 */

#define length_v2(a) sqrt(dot(a, a))

#ifdef _KERNEL_CPU_
comp_device_inline float2 operator-(const float2& a)
{
	return make_float2(-a.x, -a.y);
}

comp_device_inline float2 operator*(const float2& a, const float2& b)
{
	return make_float2(a.x*b.x, a.y*b.y);
}

comp_device_inline float2 operator*(const float2& a, float f)
{
	return make_float2(a.x*f, a.y*f);
}

comp_device_inline float2 operator*(float f, const float2& a)
{
	return make_float2(a.x*f, a.y*f);
}

comp_device_inline float2 operator/(float f, const float2& a)
{
	return make_float2(f/a.x, f/a.y);
}

comp_device_inline float2 operator/(const float2& a, float f)
{
	float invf = 1.0f/f;
	return make_float2(a.x*invf, a.y*invf);
}

comp_device_inline float2 operator/(const float2& a, const float2& b)
{
	return make_float2(a.x/b.x, a.y/b.y);
}

comp_device_inline float2 operator+(const float2& a, const float2& b)
{
	return make_float2(a.x+b.x, a.y+b.y);
}

comp_device_inline float2 operator+(const float2& a, float b)
{
	return make_float2(a.x+b, a.y+b);
}

comp_device_inline float2 operator-(const float2& a, const float2& b)
{
	return make_float2(a.x-b.x, a.y-b.y);
}

comp_device_inline float2 operator+=(float2& a, const float2& b)
{
	return a = a + b;
}

comp_device_inline float2 operator*=(float2& a, const float2& b)
{
	return a = a * b;
}

comp_device_inline float2 operator*=(float2& a, float f)
{
	return a = a * f;
}

comp_device_inline float2 operator/=(float2& a, const float2& b)
{
	return a = a / b;
}

comp_device_inline float2 operator/=(float2& a, float f)
{
	float invf = 1.0f/f;
	return a = a * invf;
}

comp_device_inline bool operator==(const float2& a, const float2& b)
{
	return (a.x == b.x && a.y == b.y);
}

comp_device_inline bool operator!=(const float2& a, const float2& b)
{
	return !(a == b);
}

comp_device_inline bool is_zero(const float2& a)
{
	return (a.x == 0.0f && a.y == 0.0f);
}

comp_device_inline float average(const float2& a)
{
	return (a.x + a.y)*(1.0f/2.0f);
}

comp_device_inline float dot(const float2& a, const float2& b)
{
	return a.x*b.x + a.y*b.y;
}

comp_device_inline float cross(const float2& a, const float2& b)
{
	return (a.x*b.y - a.y*b.x);
}

comp_device_inline float len(const float2& a)
{
	return sqrt(dot(a, a));
}

comp_device_inline float2 normalize(const float2& a)
{
	return a/len(a);
}

comp_device_inline float2 normalize_len(const float2& a, float *t)
{
	*t = len(a);
	return a/(*t);
}

comp_device_inline float2 safe_normalize(const float2& a)
{
	float t = len(a);
	return (t != 0.0f)? a/t: a;
}

comp_device_inline float2 min(const float2& a, const float2& b)
{
	return make_float2(min(a.x, b.x), min(a.y, b.y));
}

comp_device_inline float2 max(const float2& a, const float2& b)
{
	return make_float2(max(a.x, b.x), max(a.y, b.y));
}

comp_device_inline float2 clamp(const float2& a, const float2& mn, const float2& mx)
{
	return min(max(a, mn), mx);
}

comp_device_inline float2 fabs(const float2& a)
{
	return make_float2(abs(a.x), abs(a.y));
}

comp_device_inline float2 as_float2(const float4& a)
{
	return make_float2(a.x, a.y);
}

comp_device_inline float2 mix(const float2& a, const float2& b, float t)
{
	return a + t*(b - a);
}
#endif /* _KERNEL_CPU_ */

COMP_NAMESPACE_END

#endif /* __UTIL_MATH_FLOAT2_H__ */
