#ifndef _UTIL_VIEWPORT_H_
#define _UTIL_VIEWPORT_H_

#include "util/util_types.h"
COMP_NAMESPACE_BEGIN

// SYNC with DNA_camera_types
enum {
	// Blender Camera Emulation
	CAM_PERSP       = 0,
	CAM_ORTHO       = 1,
	CAM_PANO        = 2,

	// Viewport settings.
	CAM_PLANE       = 3,
};

struct Viewport {
	float3 position;
	float3 direction;
	float3 up_vector;
	float3 side_vector;
	float3 plane_position;

	int type;

	union{
		struct {
			float scale;
		};
		
		struct {
			float scale_x;
			float scale_y;
		};
	};

	union {
		// CAM_PERSP
		struct {
			float lens;
			float sensor_size;
			float field_of_view;
		};

		// CAM_ORTHO
		struct {
		};
	};
	float padding[3];
};

COMP_NAMESPACE_END
#endif
