/*
 * Copyright 2011-2017 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_TYPES_FLOAT4_H_
#define _UTIL_TYPES_FLOAT4_H_

#ifndef _UTIL_TYPES_H_
#  error "Do not include this file directly, include util_types.h instead."
#endif

COMP_NAMESPACE_BEGIN

#ifdef _KERNEL_CPU_
struct int4;

struct comp_try_align(16) float4 {
#ifdef __KERNEL_SSE__
	union {
		__m128 m128;
		struct { float x, y, z, w; };
	};

	__forceinline float4();
	__forceinline float4(const float4& a);
	__forceinline explicit float4(const __m128& a);

	__forceinline operator const __m128&(void) const;
	__forceinline operator __m128&(void);

	__forceinline float4& operator =(const float4& a);

#else  /* __KERNEL_SSE__ */
	float x, y, z, w;
#endif  /* __KERNEL_SSE__ */

	__forceinline float operator[](int i) const;
	__forceinline float& operator[](int i);
};

comp_device_inline float4 make_float4(float f);
comp_device_inline float4 make_float4(float x, float y, float z, float w);
comp_device_inline float4 make_float4(const int4& i);
comp_device_inline void print_float4(const char *label, const float4& a);
#endif  /* _KERNEL_GPU_ */

#ifdef _KERNEL_OPENCL_
#define make_float4(x, y, z, w) ((float4)(x, y, z, w))
#endif

COMP_NAMESPACE_END

#endif  /* _UTIL_TYPES_FLOAT4_H_ */
