/*
 * Copyright 2011-2017 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_MATH_FLOAT4_H_
#define _UTIL_MATH_FLOAT4_H_

#ifndef _UTIL_MATH_H_
#  error "Do not include this file directly, include util_types.h instead."
#endif

COMP_NAMESPACE_BEGIN

/*******************************************************************************
 * Declaration.
 */

#ifdef _KERNEL_CPU_
comp_device_inline float4 operator-(const float4& a);
comp_device_inline float4 operator*(const float4& a, const float4& b);
comp_device_inline float4 operator*(const float4& a, float f);
comp_device_inline float4 operator*(float f, const float4& a);
comp_device_inline float4 operator/(const float4& a, float f);
comp_device_inline float4 operator/(const float4& a, const float4& b);
comp_device_inline float4 operator+(const float4& a, const float4& b);
comp_device_inline float4 operator+(const float4& a, float f);
comp_device_inline float4 operator-(const float4& a, const float4& b);
comp_device_inline float4 operator-(const float4& a, const float b);
comp_device_inline float4 operator+=(float4& a, const float4& b);
comp_device_inline float4 operator*=(float4& a, const float4& b);
comp_device_inline float4 operator/=(float4& a, float f);

comp_device_inline int4 operator<(const float4& a, const float4& b);
comp_device_inline int4 operator>=(const float4& a, const float4& b);
comp_device_inline int4 operator<=(const float4& a, const float4& b);
comp_device_inline bool operator==(const float4& a, const float4& b);

comp_device_inline float dot(const float4& a, const float4& b);
comp_device_inline float4 fabs(const float4& a);
comp_device_inline float len_squared(const float4& a);
comp_device_inline float4 rcp(const float4& a);
comp_device_inline float4 cross(const float4& a, const float4& b);
comp_device_inline bool is_zero(const float4& a);
comp_device_inline float reduce_add(const float4& a);
comp_device_inline float average(const float4& a);
comp_device_inline float len(const float4& a);
comp_device_inline float4 normalize(const float4& a);
comp_device_inline float4 safe_normalize(const float4& a);
comp_device_inline float4 min(const float4& a, const float4& b);
comp_device_inline float4 max(const float4& a, const float4& b);
comp_device_inline float4 mix(const float4& a, const float4& b, float t);
#endif  /* _KERNEL_CPU_ */
#ifdef __KERNEL_SSE__
template<size_t index_0, size_t index_1, size_t index_2, size_t index_3>
__forceinline const float4 shuffle(const float4& b);

template<> __forceinline const float4 shuffle<0, 1, 0, 1>(const float4& b);

#  ifdef __KERNEL_SSE3__
template<> __forceinline const float4 shuffle<0, 0, 2, 2>(const float4& b);
template<> __forceinline const float4 shuffle<1, 1, 3, 3>(const float4& b);
#  endif
#endif  /* __KERNEL_SSE__ */

#ifdef _KERNEL_CPU_
comp_device_inline float4 select(const int4& mask,
                                const float4& a,
                                const float4& b);
comp_device_inline float4 reduce_min(const float4& a);
comp_device_inline float4 reduce_max(const float4& a);
#  if 0
comp_device_inline float4 reduce_add(const float4& a);
#  endif
#endif  /* !_KERNEL_GPU_ */

#define length_v4(a) sqrt(dot(a, a))

/*******************************************************************************
 * Definition.
 */

#ifdef _KERNEL_CPU_
comp_device_inline float4 operator-(const float4& a)
{
#ifdef __KERNEL_SSE__
	__m128 mask = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	return float4(_mm_xor_ps(a.m128, mask));
#else
	return make_float4(-a.x, -a.y, -a.z, -a.w);
#endif
}

comp_device_inline float4 operator*(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return float4(_mm_mul_ps(a.m128, b.m128));
#else
	return make_float4(a.x*b.x, a.y*b.y, a.z*b.z, a.w*b.w);
#endif
}

comp_device_inline float4 operator*(const float4& a, float f)
{
#if defined(__KERNEL_SSE__)
	return a * make_float4(f);
#else
	return make_float4(a.x*f, a.y*f, a.z*f, a.w*f);
#endif
}

comp_device_inline float4 operator*(float f, const float4& a)
{
	return a * f;
}

comp_device_inline float4 operator/(const float4& a, float f)
{
	return a * (1.0f/f);
}

comp_device_inline float4 operator/(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return a * rcp(b);
#else
	return make_float4(a.x/b.x, a.y/b.y, a.z/b.z, a.w/b.w);
#endif

}

comp_device_inline float4 operator+(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return float4(_mm_add_ps(a.m128, b.m128));
#else
	return make_float4(a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w);
#endif
}

comp_device_inline float4 operator+(const float4& a, const float b)
{
	return make_float4(a.x+b, a.y+b, a.z+b, a.w+b);
}

comp_device_inline float4 operator-(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return float4(_mm_sub_ps(a.m128, b.m128));
#else
	return make_float4(a.x-b.x, a.y-b.y, a.z-b.z, a.w-b.w);
#endif
}

comp_device_inline float4 operator-(const float4& a, const float b)
{
	return make_float4(a.x-b, a.y-b, a.z-b, a.w-b);
}

comp_device_inline float4 operator+=(float4& a, const float4& b)
{
	return a = a + b;
}

comp_device_inline float4 operator*=(float4& a, const float4& b)
{
	return a = a * b;
}

comp_device_inline float4 operator/=(float4& a, float f)
{
	return a = a / f;
}

comp_device_inline int4 operator<(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	/* TODO(sergey): avoid cvt. */
	return int4(_mm_cvtps_epi32(_mm_cmplt_ps(a.m128, b.m128)));
#else
	return make_int4(a.x < b.x, a.y < b.y, a.z < b.z, a.w < b.w);
#endif
}

comp_device_inline int4 operator>=(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	/* TODO(sergey): avoid cvt. */
	return int4(_mm_cvtps_epi32(_mm_cmpge_ps(a.m128, b.m128)));
#else
	return make_int4(a.x >= b.x, a.y >= b.y, a.z >= b.z, a.w >= b.w);
#endif
}

comp_device_inline int4 operator<=(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	/* TODO(sergey): avoid cvt. */
	return int4(_mm_cvtps_epi32(_mm_cmple_ps(a.m128, b.m128)));
#else
	return make_int4(a.x <= b.x, a.y <= b.y, a.z <= b.z, a.w <= b.w);
#endif
}

comp_device_inline bool operator==(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return (_mm_movemask_ps(_mm_cmpeq_ps(a.m128, b.m128)) & 15) == 15;
#else
	return (a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w);
#endif
}

comp_device_inline float dot(const float4& a, const float4& b)
{
#if defined(__KERNEL_SSE41__) && defined(__KERNEL_SSE__)
	return _mm_cvtss_f32(_mm_dp_ps(a, b, 0xFF));
#else
	return (a.x*b.x + a.y*b.y) + (a.z*b.z + a.w*b.w);
#endif
}

comp_device_inline float4 fabs(const float4& a)
{
	return make_float4(fabs(a.x), fabs(a.y), fabs(a.z), fabs(a.w));
}

comp_device_inline float len_squared(const float4& a)
{
	return dot(a, a);
}

comp_device_inline float4 rcp(const float4& a)
{
#ifdef __KERNEL_SSE__
	float4 r(_mm_rcp_ps(a.m128));
	return float4(_mm_sub_ps(_mm_add_ps(r, r),
	                         _mm_mul_ps(_mm_mul_ps(r, r), a)));
#else
	return make_float4(1.0f/a.x, 1.0f/a.y, 1.0f/a.z, 1.0f/a.w);
#endif
}

comp_device_inline float4 cross(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return (shuffle<1,2,0,0>(a)*shuffle<2,0,1,0>(b)) -
	       (shuffle<2,0,1,0>(a)*shuffle<1,2,0,0>(b));
#else
	return make_float4(a.y*b.z - a.z*b.y,
	                   a.z*b.x - a.x*b.z,
	                   a.x*b.y - a.y*b.x,
	                   0.0f);
#endif
}

comp_device_inline bool is_zero(const float4& a)
{
#ifdef __KERNEL_SSE__
	return a == make_float4(0.0f);
#else
	return (a.x == 0.0f && a.y == 0.0f && a.z == 0.0f && a.w == 0.0f);
#endif
}

comp_device_inline float reduce_add(const float4& a)
{
#ifdef __KERNEL_SSE__
	float4 h(shuffle<1,0,3,2>(a) + a);
	/* TODO(sergey): Investigate efficiency. */
	return _mm_cvtss_f32(shuffle<2,3,0,1>(h) + h);
#else
	return ((a.x + a.y) + (a.z + a.w));
#endif
}

comp_device_inline float average(const float4& a)
{
	return reduce_add(a) * 0.25f;
}

comp_device_inline float len(const float4& a)
{
	return sqrt(dot(a, a));
}

comp_device_inline float4 normalize(const float4& a)
{
	return a/len(a);
}

comp_device_inline float4 safe_normalize(const float4& a)
{
	float t = len(a);
	return (t != 0.0f)? a/t: a;
}

comp_device_inline float4 min(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return float4(_mm_min_ps(a.m128, b.m128));
#else
	return make_float4(min(a.x, b.x),
	                   min(a.y, b.y),
	                   min(a.z, b.z),
	                   min(a.w, b.w));
#endif
}

comp_device_inline float4 max(const float4& a, const float4& b)
{
#ifdef __KERNEL_SSE__
	return float4(_mm_max_ps(a.m128, b.m128));
#else
	return make_float4(max(a.x, b.x),
	                   max(a.y, b.y),
	                   max(a.z, b.z),
	                   max(a.w, b.w));
#endif
}
#endif  /* !__KERNEL_OPENCL__*/

#ifdef __KERNEL_SSE__
template<size_t index_0, size_t index_1, size_t index_2, size_t index_3>
__forceinline const float4 shuffle(const float4& b)
{
	return float4(_mm_castsi128_ps(
	        _mm_shuffle_epi32(_mm_castps_si128(b),
	                          _MM_SHUFFLE(index_3, index_2, index_1, index_0))));
}

template<> __forceinline const float4 shuffle<0, 1, 0, 1>(const float4& b)
{
	return float4(_mm_castpd_ps(_mm_movedup_pd(_mm_castps_pd(b))));
}

#  ifdef __KERNEL_SSE3__
template<> __forceinline const float4 shuffle<0, 0, 2, 2>(const float4& b)
{
	return float4(_mm_moveldup_ps(b));
}

template<> __forceinline const float4 shuffle<1, 1, 3, 3>(const float4& b)
{
	return float4(_mm_movehdup_ps(b));
}
#  endif  /* __KERNEL_SSE3__ */
#endif  /* __KERNEL_SSE__ */

#ifdef _KERNEL_CPU_
comp_device_inline float4 select(const int4& mask,
                                const float4& a,
                                const float4& b)
{
#ifdef __KERNEL_SSE__
	/* TODO(sergey): avoid cvt. */
	return float4(_mm_or_ps(_mm_and_ps(_mm_cvtepi32_ps(mask), a),
	                        _mm_andnot_ps(_mm_cvtepi32_ps(mask), b)));
#else
	return make_float4((mask.x)? a.x: b.x,
	                   (mask.y)? a.y: b.y,
	                   (mask.z)? a.z: b.z,
	                   (mask.w)? a.w: b.w);
#endif
}

comp_device_inline float4 reduce_min(const float4& a)
{
#ifdef __KERNEL_SSE__
	float4 h = min(shuffle<1,0,3,2>(a), a);
	return min(shuffle<2,3,0,1>(h), h);
#else
	return make_float4(min(min(a.x, a.y), min(a.z, a.w)));
#endif
}

comp_device_inline float4 reduce_max(const float4& a)
{
#ifdef __KERNEL_SSE__
	float4 h = max(shuffle<1,0,3,2>(a), a);
	return max(shuffle<2,3,0,1>(h), h);
#else
	return make_float4(max(max(a.x, a.y), max(a.z, a.w)));
#endif
}


comp_device_inline float4 mix(const float4& a, const float4& b, float t)
{
	return a + t*(b - a);
}

#if 0
comp_device_inline float4 reduce_add(const float4& a)
{
#ifdef __KERNEL_SSE__
	float4 h = shuffle<1,0,3,2>(a) + a;
	return shuffle<2,3,0,1>(h) + h;
#else
	return make_float4((a.x + a.y) + (a.z + a.w));
#endif
}
#endif
#endif  /* !_KERNEL_GPU_ */

COMP_NAMESPACE_END

#endif /* _UTIL_MATH_FLOAT4_H_ */
