/*
 * Copyright 2011-2017 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_TYPES_INT2_H_
#define _UTIL_TYPES_INT2_H_

#ifndef _UTIL_TYPES_H_
#  error "Do not include this file directly, include util_types.h instead."
#endif

COMP_NAMESPACE_BEGIN

#ifdef _KERNEL_CPU_
struct int2 {
	int x, y;

	__forceinline int operator[](int i) const;
	__forceinline int& operator[](int i);
};

comp_device_inline int2 make_int2(int x, int y);
#endif  /* _KERNEL_GPU_ */

#ifdef _KERNEL_OPENCL_
#define make_int2(x, y) ((ivec2)(x, y))
#endif

COMP_NAMESPACE_END

#endif  /* _UTIL_TYPES_INT2_H_ */
