/*
 * Copyright 2011-2017 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_MATH_FLOAT3_H_
#define _UTIL_MATH_FLOAT3_H_

#ifndef _UTIL_MATH_H_
#  error "Do not include this file directly, include util_types.h instead."
#endif

COMP_NAMESPACE_BEGIN

/*******************************************************************************
 * Declaration.
 */

#ifdef _KERNEL_CPU_
comp_device_inline float3 operator-(const float3& a);
comp_device_inline float3 operator*(const float3& a, const float3& b);
comp_device_inline float3 operator*(const float3& a, const float f);
comp_device_inline float3 operator*(const float f, const float3& a);
comp_device_inline float3 operator/(const float f, const float3& a);
comp_device_inline float3 operator/(const float3& a, const float f);
comp_device_inline float3 operator/(const float3& a, const float3& b);
comp_device_inline float3 operator+(const float3& a, const float3& b);
comp_device_inline float3 operator-(const float3& a, const float3& b);
comp_device_inline float3 operator+=(float3& a, const float3& b);
comp_device_inline float3 operator-=(float3& a, const float3& b);
comp_device_inline float3 operator*=(float3& a, const float3& b);
comp_device_inline float3 operator*=(float3& a, float f);
comp_device_inline float3 operator/=(float3& a, const float3& b);
comp_device_inline float3 operator/=(float3& a, float f);

comp_device_inline bool operator==(const float3& a, const float3& b);
comp_device_inline bool operator!=(const float3& a, const float3& b);

comp_device_inline float dot(const float3& a, const float3& b);
comp_device_inline float dot_xy(const float3& a, const float3& b);
comp_device_inline float3 cross(const float3& a, const float3& b);
comp_device_inline float3 normalize(const float3& a);
comp_device_inline float3 min(const float3& a, const float3& b);
comp_device_inline float3 max(const float3& a, const float3& b);
comp_device_inline float3 clamp(const float3& a, const float3& mn, const float3& mx);
comp_device_inline float3 clamp(const float3& a, float mn, float mx);
comp_device_inline float3 fabs(const float3& a);
comp_device_inline float3 mix(const float3& a, const float3& b, float t);
comp_device_inline float3 rcp(const float3& a);
#endif /* _KERNEL_CPU_ */

comp_device_inline float max3(float3 a);
comp_device_inline float len(const float3 a);
comp_device_inline float len_squared(const float3 a);

#define length_v3(a) sqrt(dot(a, a))

comp_device_inline float3 saturate3(float3 a);
comp_device_inline float3 safe_normalize(const float3 a);
comp_device_inline float3 normalize_len(const float3 a, comp_inout(float, t));
comp_device_inline float3 safe_normalize_len(const float3 a, comp_inout(float, t));

comp_device_inline bool is_zero(const float3 a);
comp_device_inline float reduce_add(const float3 a);
comp_device_inline float average(const float3 a);
comp_device_inline bool isequal_float3(const float3 a, const float3 b);

/*******************************************************************************
 * Definition.
 */

#ifdef _KERNEL_CPU_
comp_device_inline float3 operator-(const float3& a)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_xor_ps(a.m128, _mm_castsi128_ps(_mm_set1_epi32(0x80000000))));
#else
	return make_float3(-a.x, -a.y, -a.z);
#endif
}

comp_device_inline float3 operator*(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_mul_ps(a.m128,b.m128));
#else
	return make_float3(a.x*b.x, a.y*b.y, a.z*b.z);
#endif
}

comp_device_inline float3 operator*(const float3& a, const float f)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_mul_ps(a.m128,_mm_set1_ps(f)));
#else
	return make_float3(a.x*f, a.y*f, a.z*f);
#endif
}

comp_device_inline float3 operator*(const float f, const float3& a)
{
	/* TODO(sergey): Currently disabled, gives speedup but causes precision issues. */
#if defined(__KERNEL_SSE__) && 0
	return float3(_mm_mul_ps(_mm_set1_ps(f), a.m128));
#else
	return make_float3(a.x*f, a.y*f, a.z*f);
#endif
}

comp_device_inline float3 operator/(const float f, const float3& a)
{
	/* TODO(sergey): Currently disabled, gives speedup but causes precision issues. */
#if defined(__KERNEL_SSE__) && 0
	__m128 rc = _mm_rcp_ps(a.m128);
	return float3(_mm_mul_ps(_mm_set1_ps(f),rc));
#else
	return make_float3(f / a.x, f / a.y, f / a.z);
#endif
}

comp_device_inline float3 operator/(const float3& a, const float f)
{
	float invf = 1.0f/f;
	return a * invf;
}

comp_device_inline float3 operator/(const float3& a, const float3& b)
{
	/* TODO(sergey): Currently disabled, gives speedup but causes precision issues. */
#if defined(__KERNEL_SSE__) && 0
	__m128 rc = _mm_rcp_ps(b.m128);
	return float3(_mm_mul_ps(a, rc));
#else
	return make_float3(a.x / b.x, a.y / b.y, a.z / b.z);
#endif
}

comp_device_inline float3 operator+(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_add_ps(a.m128, b.m128));
#else
	return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
#endif
}

comp_device_inline float3 operator-(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_sub_ps(a.m128, b.m128));
#else
	return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
#endif
}

comp_device_inline float3 operator+=(float3& a, const float3& b)
{
	return a = a + b;
}

comp_device_inline float3 operator-=(float3& a, const float3& b)
{
	return a = a - b;
}

comp_device_inline float3 operator*=(float3& a, const float3& b)
{
	return a = a * b;
}

comp_device_inline float3 operator*=(float3& a, float f)
{
	return a = a * f;
}

comp_device_inline float3 operator/=(float3& a, const float3& b)
{
	return a = a / b;
}

comp_device_inline float3 operator/=(float3& a, float f)
{
	float invf = 1.0f/f;
	return a = a * invf;
}

comp_device_inline bool operator==(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return (_mm_movemask_ps(_mm_cmpeq_ps(a.m128, b.m128)) & 7) == 7;
#else
	return (a.x == b.x && a.y == b.y && a.z == b.z);
#endif
}

comp_device_inline bool operator!=(const float3& a, const float3& b)
{
	return !(a == b);
}

comp_device_inline float dot(const float3& a, const float3& b)
{
#if defined(__KERNEL_SSE41__) && defined(__KERNEL_SSE__)
	return _mm_cvtss_f32(_mm_dp_ps(a, b, 0x7F));
#else
	return a.x*b.x + a.y*b.y + a.z*b.z;
#endif
}

comp_device_inline float dot_xy(const float3& a, const float3& b)
{
#if defined(__KERNEL_SSE41__) && defined(__KERNEL_SSE__)
	return _mm_cvtss_f32(_mm_hadd_ps(_mm_mul_ps(a,b),b));
#else
	return a.x*b.x + a.y*b.y;
#endif
}

comp_device_inline float3 cross(const float3& a, const float3& b)
{
	float3 r = make_float3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
	return r;
}

comp_device_inline float3 normalize(const float3& a)
{
#if defined(__KERNEL_SSE41__) && defined(__KERNEL_SSE__)
	__m128 norm = _mm_sqrt_ps(_mm_dp_ps(a.m128, a.m128, 0x7F));
	return float3(_mm_div_ps(a.m128, norm));
#else
	return a/len(a);
#endif
}

comp_device_inline float3 min(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_min_ps(a.m128, b.m128));
#else
	return make_float3(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
#endif
}

comp_device_inline float3 max(const float3& a, const float3& b)
{
#ifdef __KERNEL_SSE__
	return float3(_mm_max_ps(a.m128, b.m128));
#else
	return make_float3(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
#endif
}

comp_device_inline float3 clamp(const float3& a, const float3& mn, const float3& mx)
{
	return min(max(a, mn), mx);
}

comp_device_inline float3 clamp(const float3& a, float mn, float mx)
{
	return clamp(a, make_float3(mn,mn,mn), make_float3(mx,mx,mx));
}

comp_device_inline float3 fabs(const float3& a)
{
#ifdef __KERNEL_SSE__
	__m128 mask = _mm_castsi128_ps(_mm_set1_epi32(0x7fffffff));
	return float3(_mm_and_ps(a.m128, mask));
#else
	return make_float3(fabs(a.x), fabs(a.y), fabs(a.z));
#endif
}

comp_device_inline float3 mix(const float3& a, const float3& b, float t)
{
	return a + t*(b - a);
}

comp_device_inline float3 rcp(const float3& a)
{
#ifdef __KERNEL_SSE__
	const float4 r(_mm_rcp_ps(a.m128));
	return float3(_mm_sub_ps(_mm_add_ps(r, r),
	                         _mm_mul_ps(_mm_mul_ps(r, r), a)));
#else
	return make_float3(1.0f/a.x, 1.0f/a.y, 1.0f/a.z);
#endif
}
#endif /* _KERNEL_CPU_ */

comp_device_inline float max3(float3 a)
{
	return max(max(a.x, a.y), a.z);
}

comp_device_inline float len(const float3 a)
{
#if defined(__KERNEL_SSE41__) && defined(__KERNEL_SSE__)
	return _mm_cvtss_f32(_mm_sqrt_ss(_mm_dp_ps(a.m128, a.m128, 0x7F)));
#else
	return sqrt(dot(a, a));
#endif
}

comp_device_inline float len_squared(const float3 a)
{
	return dot(a, a);
}

comp_device_inline float3 saturate3(float3 a)
{
	return make_float3(saturate(a.x), saturate(a.y), saturate(a.z));
}

comp_device_inline float3 normalize_len(const float3 a, comp_inout(float, t))
{
	comp_get_ref(t) = len(a);
	float x = 1.0f / comp_get_ref(t);
	return a*x;
}

comp_device_inline float3 safe_normalize(const float3 a)
{
	float t = len(a);
	return (t != 0.0f)? a * (1.0f/t) : a;
}

comp_device_inline float3 safe_normalize_len(const float3 a, comp_inout(float, t))
{
	comp_get_ref(t) = len(a);
	return (comp_get_ref(t) != 0.0f)? a/(comp_get_ref(t)): a;
}

comp_device_inline bool is_zero(const float3 a)
{
#ifdef __KERNEL_SSE__
	return a == make_float3(0.0f);
#else
	return (a.x == 0.0f && a.y == 0.0f && a.z == 0.0f);
#endif
}

comp_device_inline float reduce_add(const float3 a)
{
	return (a.x + a.y + a.z);
}

comp_device_inline float average(const float3 a)
{
	return reduce_add(a)*(1.0f/3.0f);
}

comp_device_inline bool isequal_float3(const float3 a, const float3 b)
{
#ifdef _KERNEL_OPENCL_
	return all(a == b);
#else
	return a == b;
#endif
}

COMP_NAMESPACE_END

#endif /* _UTIL_MATH_FLOAT3_H_ */
