#ifndef _UTIL_MATH_INTERSECT_H_
#define _UTIL_MATH_INTERSECT_H_
COMP_NAMESPACE_BEGIN

/* Ray Plane Intersection */
comp_device_inline bool ray_plane_intersect(comp_device_struct Ray* ray, float3 planePoint, float3 planeNormal, float* length) {
	// assume ray->D and planeNormal are unit vectors
	float denom = dot(planeNormal, ray->D);
	if (denom > 1e-6) {
		*length = dot(planePoint - ray->P, planeNormal) / denom;
		return true;
	}
	return false;
}

COMP_NAMESPACE_END
#endif
