/*
 * Copyright 2011-2013 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_MATH_H_
#define _UTIL_MATH_H_

/* Math
 *
 * Basic math functions on scalar and vector types. This header is used by
 * both the kernel code when compiled as C++, and other C++ non-kernel code. */

#ifdef _KERNEL_CPU_
#  include <cmath>
#  include <float.h>
#  include <math.h>
#  include <stdio.h>
#endif  /* _KERNEL_CPU_ */

#include "util/util_types.h"

COMP_NAMESPACE_BEGIN

/* Float Pi variations */

/* Division */
#ifndef M_PI_F
#  define M_PI_F    (3.1415926535897932f)  /* pi */
#endif
#ifndef M_PI_2_F
#  define M_PI_2_F  (1.5707963267948966f)  /* pi/2 */
#endif
#ifndef M_PI_4_F
#  define M_PI_4_F  (0.7853981633974830f)  /* pi/4 */
#endif
#ifndef M_1_PI_F
#  define M_1_PI_F  (0.3183098861837067f)  /* 1/pi */
#endif
#ifndef M_2_PI_F
#  define M_2_PI_F  (0.6366197723675813f)  /* 2/pi */
#endif

/* Multiplication */
#ifndef M_2PI_F
#  define M_2PI_F   (6.2831853071795864f)  /* 2*pi */
#endif
#ifndef M_4PI_F
#  define M_4PI_F   (12.566370614359172f)  /* 4*pi */
#endif

/* Float sqrt variations */
#ifndef M_SQRT2_F
#  define M_SQRT2_F (1.4142135623730950f)  /* sqrt(2) */
#endif
#ifndef M_LN2_F
#  define M_LN2_F   (0.6931471805599453f)  /* ln(2) */
#endif
#ifndef M_LN10_F
#  define M_LN10_F  (2.3025850929940457f)  /* ln(10) */
#endif

/* Scalar */

#ifdef _WIN32
#  ifndef __KERNEL_OPENCL__
comp_device_inline float fmaxf(float a, float b)
{
	return (a > b)? a: b;
}

comp_device_inline float fminf(float a, float b)
{
	return (a < b)? a: b;
}
#  endif  /* !__KERNEL_OPENCL__ */
#endif  /* _WIN32 */

#ifdef _KERNEL_CPU_
using std::isfinite;
using std::isnan;
using std::fabs;
#define floor(var) floorf(var)
#define ceil(var) ceilf(var)
#define cos(var) cosf(var)
#define sin(var) sinf(var)
#define sqrt(var) sqrtf(var)
#define asin(var) asinf(var)
#define acos(var) acos(var)
#define mod(var1, var2) fmodf(var1, var2)
#define pow(var1, var2) powf(var1, var2)
#define log(var) logf(var)

comp_device_inline int max(int a, int b)
{
	return (a > b)? a: b;
}

comp_device_inline int min(int a, int b)
{
	return (a < b)? a: b;
}

comp_device_inline float max(float a, float b)
{
	return (a > b)? a: b;
}

comp_device_inline float min(float a, float b)
{
	return (a < b)? a: b;
}

comp_device_inline double max(double a, double b)
{
	return (a > b)? a: b;
}

comp_device_inline double min(double a, double b)
{
	return (a < b)? a: b;
}

/* These 2 guys are templated for usage with registers data.
 *
 * NOTE: Since this is CPU-only functions it is ok to use references here.
 * But for other devices we'll need to be careful about this.
 */

template<typename T>
comp_device_inline T min4(const T& a, const T& b, const T& c, const T& d)
{
	return min(min(a,b),min(c,d));
}

template<typename T>
comp_device_inline T max4(const T& a, const T& b, const T& c, const T& d)
{
	return max(max(a,b),max(c,d));
}
#endif /* _KERNEL_CPU_ */

#ifdef _KERNEL_OPENCL_
#define abs(x) fabs(x)
#define mod(x, y) fmod(x, y)
#endif /* _KERNEL_OPENCL_ */

comp_device_inline float min4(float a, float b, float c, float d)
{
	return min(min(a, b), min(c, d));
}

comp_device_inline float max4(float a, float b, float c, float d)
{
	return max(max(a, b), max(c, d));
}

#ifdef _KERNEL_CPU_
/* Int/Float conversion */

comp_device_inline int uint_as_int(uint i)
{
	union { uint ui; int i; } u;
	u.ui = i;
	return u.i;
}

comp_device_inline uint int_as_uint(int i)
{
	union { uint ui; int i; } u;
	u.i = i;
	return u.ui;
}

comp_device_inline int float_as_int(float f)
{
	union { int i; float f; } u;
	u.f = f;
	return u.i;
}

comp_device_inline float int_as_float(int i)
{
	union { int i; float f; } u;
	u.i = i;
	return u.f;
}

comp_device_inline uint float_as_uint(float f)
{
	union { uint i; float f; } u;
	u.f = f;
	return u.i;
}

comp_device_inline float uint_as_float(uint i)
{
	union { uint i; float f; } u;
	u.i = i;
	return u.f;
}
#endif /* _KERNEL_CPU_ */

#ifdef _KERNEL_OPENCL_
#define uint_as_int(i) as_int(i)
#define int_as_uint(i) as_uint(i)
#define float_as_int(f) as_int(f)
#define int_as_float(i) as_float(i)
#define float_as_uint(f) as_uint(f)
#define uint_as_float(i) as_float(i)
#endif


/* Versions of functions which are safe for fast math. */
comp_device_inline bool isnan_safe(float f)
{
	uint x = float_as_uint(f);
	return (x << 1) > 0xff000000u;
}

// comp_device_inline bool isfinite_safe(float f)
// {
// 	/* By IEEE 754 rule, 2*Inf equals Inf */
// 	uint x = float_as_uint(f);
// 	return (f == f) && (x == 0 || (f != 2.0f*f)) && !((x << 1) > 0xff000000u);
// }

// comp_device_inline float ensure_finite(float v)
// {
// 	return isfinite_safe(v)? v : 0.0f;
// }

#ifdef _KERNEL_CPU_
comp_device_inline int clamp(int a, int mn, int mx)
{
	return min(max(a, mn), mx);
}

comp_device_inline float clamp(float a, float mn, float mx)
{
	return min(max(a, mn), mx);
}

comp_device_inline float mix(float a, float b, float t)
{
    return a + t*(b - a);
}
#endif  /* __KERNEL_OPENCL__ */

comp_device_inline float saturate(float a)
{
	return clamp(a, 0.0f, 1.0f);
}

#ifdef _KERNEL_CPU_
comp_device_inline int float_to_int(float f)
{
	return (int)f;
}
#endif

#ifdef _KERNEL_OPENCL_
comp_device_inline int float_to_int(float f)
{
	return convert_int(f);
}
#endif /* _KERNEL_OPENCL_ */

comp_device_inline int floor_to_int(float f)
{
	return float_to_int(floor(f));
}

comp_device_inline int ceil_to_int(float f)
{
	return float_to_int(ceil(f));
}

comp_device_inline float signf(float f)
{
	return (f < 0.0f)? -1.0f: 1.0f;
}

comp_device_inline float nonzerof(float f, float eps)
{
	if(abs(f) < eps)
		return signf(f)*eps;
	else
		return f;
}

comp_device_inline float smoothstepf(float f)
{
	float ff = f*f;
	return (3.0f*ff - 2.0f*ff*f);
}

comp_device_inline int modulo(int x, int m)
{
	return (x % m + m) % m;
}

comp_device_inline float3 float2_to_float3(const float2 a)
{
	return make_float3(a.x, a.y, 0.0f);
}

comp_device_inline float3 float4_to_float3(const float4 a)
{
	return make_float3(a.x, a.y, a.z);
}

comp_device_inline float4 float3_to_float4(const float3 a)
{
	return make_float4(a.x, a.y, a.z, 1.0f);
}

COMP_NAMESPACE_END

#include "util/util_math_int2.h"
#include "util/util_math_int3.h"
#include "util/util_math_int4.h"

#include "util/util_math_float2.h"
#include "util/util_math_float3.h"
#include "util/util_math_float4.h"

COMP_NAMESPACE_BEGIN

#ifdef _KERNEL_CPU_
/* Interpolation */

template<class A, class B> A lerp(const A& a, const A& b, const B& t)
{
	return (A)(a * ((B)1 - t) + b * t);
}

/* Triangle */

comp_device_inline float triangle_area(const float3& v1,
                                      const float3& v2,
                                      const float3& v3)
{
	return len(cross(v3 - v2, v1 - v2))*0.5f;
}
#endif  /* _KERNEL_CPU_ */

/* Orthonormal vectors */

comp_device_inline void make_orthonormals(const float3 N, comp_inout(float3, a), comp_inout(float3, b))
{
	if(N.x != N.y || N.x != N.z)
		comp_get_ref(a) = make_float3(N.z-N.y, N.x-N.z, N.y-N.x);  //(1,1,1)x N
	else
		comp_get_ref(a) = make_float3(N.z-N.y, N.x+N.z, -N.y-N.x);  //(-1,1,1)x N

	comp_get_ref(a) = normalize(comp_get_ref(a));
	comp_get_ref(b) = cross(N, comp_get_ref(a));
}

/* Color division */

comp_device_inline float3 safe_invert_color(float3 a)
{
	float x, y, z;

	x = (a.x != 0.0f)? 1.0f/a.x: 0.0f;
	y = (a.y != 0.0f)? 1.0f/a.y: 0.0f;
	z = (a.z != 0.0f)? 1.0f/a.z: 0.0f;

	return make_float3(x, y, z);
}

comp_device_inline float3 safe_divide_color(float3 a, float3 b)
{
	float x, y, z;

	x = (b.x != 0.0f)? a.x/b.x: 0.0f;
	y = (b.y != 0.0f)? a.y/b.y: 0.0f;
	z = (b.z != 0.0f)? a.z/b.z: 0.0f;

	return make_float3(x, y, z);
}

comp_device_inline float3 safe_divide_even_color(float3 a, float3 b)
{
	float x, y, z;

	x = (b.x != 0.0f)? a.x/b.x: 0.0f;
	y = (b.y != 0.0f)? a.y/b.y: 0.0f;
	z = (b.z != 0.0f)? a.z/b.z: 0.0f;

	/* try to get gray even if b is zero */
	if(b.x == 0.0f) {
		if(b.y == 0.0f) {
			x = z;
			y = z;
		}
		else if(b.z == 0.0f) {
			x = y;
			z = y;
		}
		else
			x = 0.5f*(y + z);
	}
	else if(b.y == 0.0f) {
		if(b.z == 0.0f) {
			y = x;
			z = x;
		}
		else
			y = 0.5f*(x + z);
	}
	else if(b.z == 0.0f) {
		z = 0.5f*(x + y);
	}

	return make_float3(x, y, z);
}

/* Rotation of point around axis and angle */

comp_device_inline float3 rotate_around_axis(float3 p, float3 axis, float angle)
{
	float costheta = cos(angle);
	float sintheta = sin(angle);
	float3 r;

	r.x = ((costheta + (1 - costheta) * axis.x * axis.x) * p.x) +
	      (((1 - costheta) * axis.x * axis.y - axis.z * sintheta) * p.y) +
	      (((1 - costheta) * axis.x * axis.z + axis.y * sintheta) * p.z);

	r.y = (((1 - costheta) * axis.x * axis.y + axis.z * sintheta) * p.x) +
	      ((costheta + (1 - costheta) * axis.y * axis.y) * p.y) +
	     (((1 - costheta) * axis.y * axis.z - axis.x * sintheta) * p.z);

	r.z = (((1 - costheta) * axis.x * axis.z - axis.y * sintheta) * p.x) +
	      (((1 - costheta) * axis.y * axis.z + axis.x * sintheta) * p.y) +
	      ((costheta + (1 - costheta) * axis.z * axis.z) * p.z);

	return r;
}

/* NaN-safe math ops */

comp_device_inline float safe_sqrt(float f)
{
	return sqrt(max(f, 0.0f));
}

comp_device float safe_asin(float a)
{
	return asin(clamp(a, -1.0f, 1.0f));
}

comp_device float safe_acos(float a)
{
	return acos(clamp(a, -1.0f, 1.0f));
}

comp_device float compatible_pow(float x, float y)
{
#ifdef _KERNEL_GPU_
	if(y == 0.0f) /* x^0 -> 1, including 0^0 */
		return 1.0f;

	/* GPU pow doesn't accept negative x, do manual checks here */
	if(x < 0.0f) {
		if(mod(-y, 2.0f) == 0.0f)
			return pow(-x, y);
		else
			return -pow(-x, y);
	}
	else if(x == 0.0f)
		return 0.0f;
#endif
	return pow(x, y);
}

comp_device float safe_pow(float a, float b)
{
	if(UNLIKELY(a < 0.0f && b != float_to_int(b)))
		return 0.0f;

	return compatible_pow(a, b);
}

comp_device float safe_divide(float a, float b)
{
	return (b != 0.0f)? a/b: 0.0f;
}

comp_device float safe_log(float a, float b)
{
	if(UNLIKELY(a <= 0.0f || b <= 0.0f))
		return 0.0f;

	return safe_divide(log(a),log(b));
}

comp_device float safe_modulo(float a, float b)
{
	return (b != 0.0f)? mod(a, b): 0.0f;
}

comp_device_inline float xor_signmask(float x, int y)
{
	return int_as_float(float_as_int(x) ^ y);
}

COMP_NAMESPACE_END

#endif /* _UTIL_MATH_H_ */
