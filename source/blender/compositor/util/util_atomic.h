/*
 * Copyright 2014 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_ATOMIC_H_
#define _UTIL_ATOMIC_H_

#ifdef _KERNEL_CPU_

/* Using atomic ops header from Blender. */
#include "atomic_ops.h"

ATOMIC_INLINE void atomic_update_max_z(size_t *maximum_value, size_t value)
{
	size_t prev_value = *maximum_value;
	while(prev_value < value) {
		if(atomic_cas_z(maximum_value, prev_value, value) != prev_value) {
			break;
		}
	}
}

#define atomic_add_and_fetch_float(p, x) atomic_add_and_fetch_fl((p), (x))

#define atomic_fetch_and_inc_uint32(p) atomic_fetch_and_add_uint32((p), 1)

#define COMP_LOCAL_MEM_FENCE 0
#define comp_barrier(flags) (void)0

#else  /* _KERNEL_GPU_ */

#ifdef __KERNEL_OPENCL__

/* Float atomics implementation credits:
 *   http://suhorukov.blogspot.in/2011/12/opencl-11-atomic-operations-on-floating.html
 */
comp_device_inline float atomic_add_and_fetch_float(volatile comp_global float *source,
                                        const float operand)
{
	union {
		unsigned int int_value;
		float float_value;
	} new_value;
	union {
		unsigned int int_value;
		float float_value;
	} prev_value;
	do {
		prev_value.float_value = *source;
		new_value.float_value = prev_value.float_value + operand;
	} while(atomic_cmpxchg((volatile comp_global unsigned int *)source,
	                       prev_value.int_value,
	                       new_value.int_value) != prev_value.int_value);
	return new_value.float_value;
}

#define atomic_fetch_and_add_uint32(p, x) atomic_add((p), (x))
#define atomic_fetch_and_inc_uint32(p) atomic_inc((p))

#define COMP_LOCAL_MEM_FENCE CLK_LOCAL_MEM_FENCE
#define comp_barrier(flags) barrier(flags)

#endif  /* __KERNEL_OPENCL__ */

#ifdef __KERNEL_CUDA__

#define atomic_add_and_fetch_float(p, x) (atomicAdd((float*)(p), (float)(x)) + (float)(x))

#define atomic_fetch_and_add_uint32(p, x) atomicAdd((unsigned int*)(p), (unsigned int)(x))
#define atomic_fetch_and_inc_uint32(p) atomic_fetch_and_add_uint32((p), 1)

#define COMP_LOCAL_MEM_FENCE
#define comp_barrier(flags) __syncthreads()

#endif  /* __KERNEL_CUDA__ */

#endif  /* _KERNEL_GPU_ */

#endif /* __UTIL_ATOMIC_H__ */
