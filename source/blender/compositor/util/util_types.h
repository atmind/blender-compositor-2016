/*
 * Copyright 2011-2013 Blender Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTIL_TYPES_H_
#define _UTIL_TYPES_H_

#ifdef _KERNEL_CPU_
#include <stdlib.h>
#endif

/* Bitness */

#if defined(__ppc64__) || defined(__PPC64__) || defined(__x86_64__) || defined(__ia64__) || defined(_M_X64)
#  define __KERNEL_64_BIT__
#endif

/* Qualifiers for kernel code shared by CPU and GPU */

#ifdef _KERNEL_CPU_
#  define comp_device_inline static inline
#  define comp_device_noinline static
#  define comp_global
#  define comp_constant
#  define comp_local
#  define comp_local_param
#  define comp_private
#  define comp_restrict __restrict
#  define __KERNEL_WITH_SSE_ALIGN__

#  if defined(_WIN32) && !defined(FREE_WINDOWS)
#    define comp_device_inline static __forceinline
#    define comp_device_forceinline static __forceinline
#    define comp_align(...) __declspec(align(__VA_ARGS__))
#    ifdef __KERNEL_64_BIT__
#      define comp_try_align(...) __declspec(align(__VA_ARGS__))
#    else  /* __KERNEL_64_BIT__ */
#      undef __KERNEL_WITH_SSE_ALIGN__
/* No support for function arguments (error C2719). */
#      define comp_try_align(...)
#    endif  /* __KERNEL_64_BIT__ */
#    define comp_may_alias
#    define comp_always_inline __forceinline
#    define comp_never_inline __declspec(noinline)
#    define comp_maybe_unused
#  else  /* _WIN32 && !FREE_WINDOWS */
//#    define comp_device_inline static inline __attribute__((always_inline))
#    define comp_device_forceinline static inline __attribute__((always_inline))
#    define comp_align(...) __attribute__((aligned(__VA_ARGS__)))
#    ifndef FREE_WINDOWS64
#      define __forceinline inline __attribute__((always_inline))
#    endif
#    define comp_try_align(...) __attribute__((aligned(__VA_ARGS__)))
#    define comp_may_alias __attribute__((__may_alias__))
#    define comp_always_inline __attribute__((always_inline))
#    define comp_never_inline __attribute__((noinline))
#    define comp_maybe_unused __attribute__((used))
#  endif  /* _WIN32 && !FREE_WINDOWS */
#endif  /* _KERNEL_GPU_ */

/* Standard Integer Types */

#ifdef _KERNEL_CPU_
/* int8_t, uint16_t, and friends */
#  ifndef _WIN32
#    include <stdint.h>
#  endif
/* SIMD Types */
// #  include "util/util_optimization.h"
#endif  /* _KERNEL_GPU_ */

COMP_NAMESPACE_BEGIN

/* Types
 *
 * Define simpler unsigned type names, and integer with defined number of bits.
 * Also vector types, named to be compatible with OpenCL builtin types, while
 * working for CUDA and C++ too. */

/* Shorter Unsigned Names */

#ifdef _KERNEL_CPU_
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned short ushort;
#endif
// #ifndef __KERNEL_OPENCL__
// typedef unsigned char uchar;
// typedef unsigned int uint;
// typedef unsigned short ushort;
// #endif

/* Fixed Bits Types */

// #ifdef __KERNEL_OPENCL__
// typedef ulong uint64_t;
// #endif

#ifdef _KERNEL_CPU_
#  ifdef _WIN32
typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef signed short int16_t;
typedef unsigned short uint16_t;

typedef signed int int32_t;
typedef unsigned int uint32_t;

typedef long long int64_t;
typedef unsigned long long uint64_t;
#    ifdef __KERNEL_64_BIT__
typedef int64_t ssize_t;
#    else
typedef int32_t ssize_t;
#    endif
#  endif  /* _WIN32 */

/* Generic Memory Pointer */

typedef uint64_t device_ptr;
#endif  /* _KERNEL_GPU_ */

// comp_device_inline size_t align_up(size_t offset, size_t alignment)
// {
// 	return (offset + alignment - 1) & ~(alignment - 1);
// }
//
// comp_device_inline size_t divide_up(size_t x, size_t y)
// {
// 	return (x + y - 1) / y;
// }
//
// comp_device_inline size_t round_up(size_t x, size_t multiple)
// {
// 	return ((x + multiple - 1) / multiple) * multiple;
// }
//
// comp_device_inline size_t round_down(size_t x, size_t multiple)
// {
// 	return (x / multiple) * multiple;
// }

/* Interpolation types for textures
 * cuda also use texture space to store other objects */
#if defined(_KERNEL_CPU_) || defined(_KERNEL_OPENCL_)
enum InterpolationType {
	INTERPOLATION_LINEAR = 0,
	INTERPOLATION_CLOSEST = 1,
	INTERPOLATION_CUBIC = 2,
	INTERPOLATION_SMART = 3
};

enum ExtensionType {
	/* Cause the image to repeat horizontally and vertically. */
	EXTENSION_REPEAT = 0,
	/* Extend by repeating edge pixels of the image. */
	EXTENSION_EXTEND = 1,
	/* Clip to image size and set exterior pixels as transparent. */
	EXTENSION_CLIP = 2,
};
#endif

/* Extension types for textures.
 *
 * Defines how the image is extrapolated past its original bounds.
 */

/* macros */

/* hints for branch prediction, only use in code that runs a _lot_ */
#if defined(__GNUC__) && defined(_KERNEL_CPU_)
#  define LIKELY(x)       __builtin_expect(!!(x), 1)
#  define UNLIKELY(x)     __builtin_expect(!!(x), 0)
#else
#  define LIKELY(x)       (x)
#  define UNLIKELY(x)     (x)
#endif

// #if defined(__GNUC__) || defined(__clang__) && defined(_KERNEL_CPU_)
// /* Some magic to be sure we don't have reference in the type. */
// template<typename T> static inline T decltype_helper(T x) { return x; }
// #  define TYPEOF(x) decltype(decltype_helper(x))
// #endif

COMP_NAMESPACE_END

#ifdef _KERNEL_CPU_
#  include <cassert>
#  define util_assert(statement)  assert(statement)
#else
#  define util_assert(statement)
#endif

/* Vectorized types declaration. */
#include "util/util_types_int2.h"
#include "util/util_types_int3.h"
#include "util/util_types_int4.h"

#include "util/util_types_uint2.h"
#include "util/util_types_uint3.h"
#include "util/util_types_uint4.h"

#include "util/util_types_float2.h"
#include "util/util_types_float3.h"
#include "util/util_types_float4.h"

/* Vectorized types implementation. */
#include "util/util_types_int2_impl.h"
#include "util/util_types_int3_impl.h"
#include "util/util_types_int4_impl.h"

#include "util/util_types_uint2_impl.h"
#include "util/util_types_uint3_impl.h"
#include "util/util_types_uint4_impl.h"

#include "util/util_types_float2_impl.h"
#include "util/util_types_float3_impl.h"
#include "util/util_types_float4_impl.h"

#endif /* _UTIL_TYPES_H_ */
