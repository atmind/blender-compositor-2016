#include "device_task.hpp"

namespace Compositor {
namespace Device {
ComputeTask::ComputeTask(Compositor::Node *node, int x_min, int y_min, int x_max, int y_max, Compositor::Output *output) {
	this->node = node;
	this->output = output;
	this->x_min = x_min;
	this->y_min = y_min;
	this->x_max = x_max;
	this->y_max = y_max;
	this->num_samples = 8;
	this->type = EXECUTE;
}

ComputeTask::ComputeTask(Compositor::Output *output, compute_task_types type){
	this->type = type;
	this->output = output;
}

ComputeTask::ComputeTask(compute_task_types type){
	this->type = type;
}

std::ostream& operator<<(std::ostream& stream, const ComputeTask& task) {
	stream << "Compositor::ComputeTask(x_min=" << task.x_min << ",y_min=" << task.y_min << ",x_max=" << task.x_max << ",y_max=" << task.y_max << ")";
	return stream;
}

}
}
