#ifndef CMP_DEVICE_DEVICE_OPENCL_MEMORY_MANAGER_HPP
#define CMP_DEVICE_DEVICE_OPENCL_MEMORY_MANAGER_HPP
#include "clew.h"
#include <vector>
#include <list>
#include "kernel/kernels/cpu/kernel_cpu_defines.h"
#include "util/util_types.h"
#include "kernel/kernel_globals.h"

namespace Compositor {
namespace Device {

enum AllocationFlags {
	AF_NO_FLAGS = 0,
	AF_FREE_AFTER_UPLOAD_BLENDER_ALLOC = 1
};

struct Allocation {
	ulong size;
	void *data;
	int flags;
	Allocation(cl_ulong size, void *data, int flags);
	void upload(void *target);
};

class FMMDeviceCallback {
public:
	virtual int get_num_buffers() = 0;
	virtual ulong get_max_buffer_size() = 0;
	virtual void *upload(void *data, ulong size, int buffer_index) = 0;
	virtual void release_buffer(void *buffer) = 0;
	virtual ~FMMDeviceCallback() {
	}
};

struct Buffer {
public:
	/**
	 * The index of the buffer
	 */
	int index;

	/**
	 * Max size of a mem object for the device.
	 */
	ulong max_size;

	/**
	 * the device mem object when allocated on the device. hold locally in the buffer.
	 */
	void *buffer;

	/**
	 * the host pointer allocated for this buffer
	 */
	void *data;

	/**
	 * List of all allocations in the buffer
	 */
	std::list<Allocation *> allocations;

public:
	Buffer(int index, cl_ulong max_size);
	cl_ulong allocated_size();
	cl_ulong free_size();
	bool alloc(size_t size, void *data, MemoryPTR *ptr, int allocation_flags);

	void upload(FMMDeviceCallback *device_callback);
	void release_buffer(FMMDeviceCallback *device_callback);
	void clear();
};


struct FlatMemoryManager {
private:
	std::vector<Buffer *> buffers;
	FMMDeviceCallback *device_callback;

public:
	FlatMemoryManager(FMMDeviceCallback *device_callback);
	~FlatMemoryManager();

	/**
	 * Allocate a memory
	 */
	bool alloc(size_t size, void *data, MemoryPTR *ptr, int allocation_flags);

	void upload_to_device();
	void release_buffers();
	void clear();
};

}
}
#endif
