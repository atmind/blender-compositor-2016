#include "device_opencl.hpp"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <regex>
#include "MEM_guardedalloc.h"
#include "PIL_time.h"


extern "C" {
	#include "BKE_node.h"
	#include "BKE_appdir.h"
}
// Reuse the allocated result buffer.
#define OPENCL_KERNEL_NAME "compositor_sample"


namespace Compositor {
namespace Device {
struct OpenCLPlatform {
	cl_platform_id platform_id;

	std::vector<OpenCLPlatformDevice *> devices;

	~OpenCLPlatform() {
		for (auto device: devices) {
			delete(device);
		}
	}
};
static void CL_CALLBACK clContextError(const char *errinfo,
                                       const void * /*private_info*/,
                                       size_t /*cb*/,
                                       void * /*user_data*/)
{
	std::cout << "OPENCL error: \n" << errinfo << "\n";
}

OpenCLPlatformDevice::OpenCLPlatformDevice(OpenCLPlatform* platform, cl_device_id device_id, const char ** sourcecode) {
	this->platform = platform;
	this->device_id = device_id;
	this->device = NULL;

	this->build_context();
	this->build_program(sourcecode);
	this->build_kernel();
	this->build_command_queue();

	this->mem_manager_cb = new FMMOpenCLCallback(this);
	this->mem_manager = new FlatMemoryManager(this->mem_manager_cb);
	this->result_buffer = NULL;
}
OpenCLPlatformDevice::~OpenCLPlatformDevice()  {
			#ifdef OPENCL_REUSE_RESULT_BUFFER
	if (result_buffer) {
		clReleaseMemObject(result_buffer);
		result_buffer = NULL;
	}
			#endif

	clReleaseCommandQueue(command_queue);
	// clReleaseDevice(device_id);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseContext(context);

	delete(mem_manager);
	mem_manager = NULL;
	delete(mem_manager_cb);
	mem_manager_cb = NULL;
	device = NULL;
}

void OpenCLPlatformDevice::build_context() {
	cl_int error;
	this->context = clCreateContext(NULL, 1, &this->device_id, clContextError, NULL, &error);
	if (error != CL_SUCCESS) { printf("CLERROR[%d]: %s\n", error, clewErrorString(error));  }
}
void OpenCLPlatformDevice::build_program(const char **sourcecode) {
	cl_int error;
	this->program = clCreateProgramWithSource(this->context, 1, sourcecode, 0, &error);
	error = clBuildProgram(this->program, 1, &this->device_id, "", 0, 0);
	if (error != CL_SUCCESS) {
		cl_int error2;
		size_t ret_val_size = 0;
		printf("CLERROR[%d]: %s\n", error, clewErrorString(error));
		error2 = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &ret_val_size);
		if (error2 != CL_SUCCESS) { printf("CLERROR[%d]: %s\n", error, clewErrorString(error)); }
		char *build_log = (char *)MEM_mallocN(sizeof(char) * ret_val_size + 1, __func__);
		error2 = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, ret_val_size, build_log, NULL);
		if (error2 != CL_SUCCESS) { printf("CLERROR[%d]: %s\n", error, clewErrorString(error)); }
		printf("%s", *sourcecode);
		build_log[ret_val_size] = '\0';
		printf("%s", build_log);
		MEM_freeN(build_log);
	}
}
void OpenCLPlatformDevice::build_kernel() {
	cl_int error;
	std::cout << "creating kernel\n";
	this->kernel = clCreateKernel(this->program, OPENCL_KERNEL_NAME, &error);
	if (error != CL_SUCCESS) {
		std::cout << "clCreateKernel " << clewErrorString(error) << "\n";
	}

}
void OpenCLPlatformDevice::build_command_queue() {
	cl_int error;
	std::cout << "creating command queue\n";
	this->command_queue = clCreateCommandQueue(this->context, this->device_id, 0, &error);
	if (error != CL_SUCCESS) {
		std::cout << "clCreateCommandQueue " << clewErrorString(error) << "\n";
	}
}

void OpenCLPlatformDevice::init_result_buffer(Output *output) {
	cl_int error;
			#ifdef OPENCL_REUSE_RESULT_BUFFER
	// check same size
	if (this->result_buffer) {
		if (this->result_resolution.x != output->width || this->result_resolution.y != output->height) {
			clReleaseMemObject(this->result_buffer);
			this->result_buffer = NULL;
		}
	}
			#endif
	if (!this->result_buffer) {
		cl_image_format image_format = {CL_RGBA, CL_FLOAT};
		cl_image_desc image_desc = {CL_MEM_OBJECT_IMAGE2D, output->width, output->height, 1, 1, 4 * sizeof(float) * output->width, 0, 0, 0};
		this->result_buffer = clCreateImage(this->context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, &image_format, &image_desc, output->buffer, &error);
		if (error != CL_SUCCESS) {std::cout << "clCreateImage " << clewErrorString(error) << "\n";}

				#ifdef OPENCL_REUSE_RESULT_BUFFER
		this->result_resolution.x = output->width;
		this->result_resolution.y = output->height;
		// Mark object to be persistent
		clRetainMemObject(this->result_buffer);
				#endif
	}
	error = clSetKernelArg(this->kernel, 0, sizeof(cl_mem), &this->result_buffer);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}
}

void OpenCLPlatformDevice::init_program(Program *program) {
	cl_int error;
	cl_int program_size = program->size();

	this->composite_program = clCreateBuffer(this->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, 4 * sizeof(uint) * program_size, program->as_uint4_array(), &error);
	if (error != CL_SUCCESS) {std::cout << "clCreateBuffer[program] " << clewErrorString(error) << "\n";}

	error = clSetKernelArg(this->kernel, 2, sizeof(cl_mem), &this->composite_program);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}

	error = clSetKernelArg(this->kernel, 3, sizeof(cl_uint), &program_size);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}

	// allocate local program memory
	error = clSetKernelArg(this->kernel, 4, 4 * sizeof(uint) * program_size, NULL);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}
}

void OpenCLPlatformDevice::init_kernel_globals(Output *output) {
	// kernel globals
	this->kernel_globals.texture_ptr.index = 0;
	this->kernel_globals.texture_ptr.offset = 0;
	this->kernel_globals.num_samples = 0;
	this->kernel_globals.frame_number = 0;
	this->kernel_globals.output_dimension.x = output->width;
	this->kernel_globals.output_dimension.y = output->height;
	this->kernel_globals.aspect_ratio = output->width / float(output->height);
}


static std::vector<OpenCLPlatform *> g_platforms;


static double g_start_time;


void DeviceOpenCL::load_source(std::stringstream &opencl_source, std::string path) {
	std::string source_path = std::string(BKE_appdir_folder_id(BLENDER_SYSTEM_SCRIPTS, "compositor/source/"));
	std::string line;
	std::regex include_regex("(#include)([\\s]+)\\\"([a-z0-9\\/_\\.]*)\\\"");
	std::ifstream fh_opencl_source(source_path + path);
	if (!fh_opencl_source.is_open()) {
		opencl_source << "// WARNING Cannot include file " << path << " reason, cannot open file.\n";
		return;
	}
	while (getline(fh_opencl_source, line)) {
		if (regex_match(line, include_regex)) {
			opencl_source << "// --------------------------------------------------------\n";
			opencl_source << "// START " << line << "\n";
			opencl_source << "// --------------------------------------------------------\n";
			int sindex = line.find("\"") + 1;
			int eindex = line.find("\"", sindex);
			std::string include_name = line.substr(sindex, eindex - sindex);
			load_source(opencl_source, include_name);
			opencl_source << "// END " << line << "\n\n";
		}
		else {
			opencl_source << line << "\n";
		}
	}
}

void DeviceOpenCL::init_opencl() {
	if (clewInit() != CLEW_SUCCESS)         /* this will check for errors and skip if already initialized */
		return;
	cl_uint numberOfPlatforms = 0;
	cl_int error;
	error = clGetPlatformIDs(0, 0, &numberOfPlatforms);
	if (error == -1001) { }           /* GPU not supported */
	else if (error != CL_SUCCESS) { printf("CLERROR[%d]: %s\n", error, clewErrorString(error));  }

	std::stringstream program_source;
	load_source(program_source, "kernel/kernels/opencl/kernel_opencl.cl");
	// NOTE: is needed as program_source.str() is not valid for the opencl compiler
	std::string sourceCode(std::istreambuf_iterator<char>(program_source), (std::istreambuf_iterator<char>()));
	const char *cl_str = sourceCode.c_str();
	const char **cl_strs = {&cl_str};

	cl_platform_id *platforms = (cl_platform_id *)MEM_mallocN(sizeof(cl_platform_id) * numberOfPlatforms, __func__);
	error = clGetPlatformIDs(numberOfPlatforms, platforms, 0);
	unsigned int indexPlatform;
	for (indexPlatform = 0; indexPlatform < numberOfPlatforms; indexPlatform++) {
		cl_platform_id platform_id = platforms[indexPlatform];
		cl_uint numberOfDevices = 0;
		clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 0, 0, &numberOfDevices);
		if (numberOfDevices <= 0) {
			continue;
		}
		cl_device_id *cldevices = (cl_device_id *)MEM_mallocN(sizeof(cl_device_id) * numberOfDevices, __func__);
		clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, numberOfDevices, cldevices, 0);

		if (numberOfDevices > 0) {
			OpenCLPlatform *platform = new OpenCLPlatform();
			platform->platform_id = platform_id;

			for (int device_index = 0; device_index < numberOfDevices; device_index++) {
				platform->devices.push_back(new OpenCLPlatformDevice(platform, cldevices[device_index], cl_strs));
				// break;
			}
			g_platforms.push_back(platform);
		}

		MEM_freeN(cldevices);
		MEM_freeN(platforms);
	}
}

void DeviceOpenCL::deinit_opencl() {
	for (auto platform: g_platforms) {
		delete platform;
	}
}

void DeviceOpenCL::init(Program *program, Compositor::Output *output) {
	Device::init(program, output);

	g_start_time = PIL_check_seconds_timer();

	for (auto platform: g_platforms) {
		for (auto device: platform->devices) {
			device->init_result_buffer(output);
			device->init_program(this->program);
			device->init_kernel_globals(output);
			device->mem_manager->clear();
			this->add_program_to_mem_manager(device->mem_manager, &device->kernel_globals.texture_ptr, &device->kernel_globals.viewport_ptr);
		}
	}
}

void DeviceOpenCL::end_task(ComputeTask *UNUSED(task), void *data) {
	double end_time;
	OpenCLPlatformDevice *device = (OpenCLPlatformDevice *)data;

#ifndef OPENCL_WAIT_FOR_NDRANGE_COMPLETION
	clFlush(g_command_queue);
	if (error != CL_SUCCESS) {std::cout << "clFlush " << clewErrorString(error) << "\n";}
	clFinish(g_command_queue);
	if (error != CL_SUCCESS) {std::cout << "clFinish " << clewErrorString(error) << "\n";}
#endif

	end_time = PIL_check_seconds_timer();
	std::cout << "Compositor::DeviceOpenCL Execution time: " << (end_time - g_start_time) << "\n";

	for (auto platform: g_platforms) {
		for (auto device: platform->devices) {
			clReleaseMemObject(device->composite_program);
			device->mem_manager->release_buffers();
					#ifndef OPENCL_REUSE_RESULT_BUFFER
			clReleaseMemObject(device->result_buffer);
			device->result_buffer = NULL;
					#endif
		}
	}
}


void DeviceOpenCL::start() {
	Device::start();
	int num_devices = 0;
	for (auto platform: g_platforms) {
		num_devices += platform->devices.size();
	}
	BLI_init_threads(&this->threads, thread_execute, num_devices);
	for (auto platform: g_platforms) {
		for (auto device: platform->devices) {
			device->device = this;
			BLI_insert_thread(&this->threads, device);
		}
	}
}

void *DeviceOpenCL::thread_execute(void *data) {
	OpenCLPlatformDevice *device = (OpenCLPlatformDevice *)data;
	ComputeTask *task;
	while ((task = (ComputeTask *)BLI_thread_queue_pop(device->device->get_queue()))) {
		handle_single_task(device->device, task, data);
	}
	return NULL;
}
void DeviceOpenCL::stop() {
	BLI_thread_queue_nowait(this->get_queue());
	BLI_end_threads(&this->threads);

	for (auto platform: g_platforms) {
		for (auto device: platform->devices) {
			device->device = NULL;
		}
	}

	Device::stop();
}

void DeviceOpenCL::execute_task(ComputeTask *task, void *data) {
	if (task->is_cancelled()) { return; }
	OpenCLPlatformDevice *device = (OpenCLPlatformDevice *)data;
	std::cout << *task << device << "\n";
	cl_int error;
	// TODO determine strategy: if user is editting schedule tasks 1 by 1 otherwise overload the gpu.
#ifdef OPENCL_WAIT_FOR_NDRANGE_COMPLETION
	cl_event kernel_event;
#endif
	const size_t work_offset[] = {task->x_min, task->y_min, 0};
	const size_t work_size[] = {task->x_max - task->x_min, task->y_max - task->y_min, 1};

	// Update kernel globals
	device->kernel_globals.num_samples = task->num_samples;
	device->kernel_globals.frame_number = 0;
	device->kernel_globals.seed = 1234567 + task->x_min + task->y_min;
	error = clSetKernelArg(device->kernel, 1, sizeof(HostKernelGlobals), &device->kernel_globals);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}

#ifdef OPENCL_WAIT_FOR_NDRANGE_COMPLETION
	error = clEnqueueNDRangeKernel(device->command_queue, device->kernel, 2, work_offset, work_size, NULL, 0, NULL, &kernel_event);
#else
	error = clEnqueueNDRangeKernel(device->command_queue, device->kernel, 2, work_offset, work_size, NULL, 0, NULL, NULL);
#endif
	if (error != CL_SUCCESS) {std::cout << "clEnqueueNDRangeKernel " << clewErrorString(error) << "\n";}


#ifdef OPENCL_DOWNLOAD_INTERMEDIATE_RESULT
	cl_event download_event;
	float *result_buffer = (task->output->buffer) + (task->x_min + task->y_min * task->output->width) * 4;
	error = clEnqueueReadImage(device->command_queue, device->result_buffer, CL_FALSE, work_offset, work_size, 4 * sizeof(float) * task->output->width, 0, result_buffer, 1, &kernel_event, &download_event);
	if (error != CL_SUCCESS) {std::cout << "clEnqueueReadImage " << clewErrorString(error) << "\n";}
	error = clWaitForEvents(1, &download_event);
	if (error != CL_SUCCESS) {std::cout << "clWaitForEvents " << clewErrorString(error) << "\n";}
	task->output->update_subimage(task->x_min, task->y_min, task->x_max, task->y_max);
#else
#ifdef OPENCL_WAIT_FOR_NDRANGE_COMPLETION
	error = clWaitForEvents(1, &kernel_event);
	if (error != CL_SUCCESS) {std::cout << "clWaitForEvents " << clewErrorString(error) << "\n";}
#endif
#endif
}

void DeviceOpenCL::task_finished(ComputeTask *UNUSED(task), void *UNUSED(data)) {
}

// FMMOPENCLCallback
FMMOpenCLCallback::FMMOpenCLCallback(OpenCLPlatformDevice *device) {
	this->device = device;
}

int FMMOpenCLCallback::get_num_buffers() {
	return 1;
}
ulong FMMOpenCLCallback::get_max_buffer_size() {
	cl_ulong max_size;
	cl_int error;

	error = clGetDeviceInfo(this->device->device_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &max_size, NULL);
	if (error != CL_SUCCESS) {std::cout << "clGetDeviceInfo " << clewErrorString(error) << "\n";}

	return max_size;
}
void *FMMOpenCLCallback::upload(void *data, ulong size, int buffer_index) {
	const int kernel_arg_offset = 5;
	cl_int error;
	cl_mem buffer = NULL;
	if (size) {
		buffer = clCreateBuffer(this->device->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size, data, &error);
		if (error != CL_SUCCESS) {std::cout << "clCreateBuffer[buffer" << buffer_index <<  "] " << clewErrorString(error) << "\n";}
	}
	// set kernel arg

	error = clSetKernelArg(this->device->kernel, buffer_index + kernel_arg_offset, sizeof(cl_mem), &buffer);
	if (error != CL_SUCCESS) {std::cout << "clSetKernelArg " << clewErrorString(error) << "\n";}
	return buffer;
}


void FMMOpenCLCallback::release_buffer(void *buffer) {
	cl_mem clBuffer = (cl_mem)buffer;
	clReleaseMemObject(clBuffer);
}
}
}
