#include "device.hpp"

#include "device_cpu.hpp"
#include "device_opencl.hpp"
#include "COM_defines.h"
#include <iostream>
#include "BLI_string.h"
#include "BLT_translation.h"
#include "MEM_guardedalloc.h"

namespace Compositor {
namespace Device {
ThreadQueue *Device::get_queue() {
	return this->queue;
}

void Device::start_task(ComputeTask *UNUSED(task), void *UNUSED(data)) {
}

void Device::handle_single_task(Device *device, ComputeTask *task, void *data) {
	char buf[128];

	switch (task->type) {
		case START:
			device->start_task(task, data);
			break;
		case END:
			device->end_task(task, data);
			break;
		case EXECUTE:
			device->execute_task(task, data);
			device->task_finished(task, data);
			break;
	}

	/* update the stats */
	int num_tasks_left = BLI_thread_queue_size(device->queue);
	bNodeTree *node_tree = task->output->node_tree;
	float progress = 1.0-((float)num_tasks_left/(float)device->num_tasks);
	node_tree->progress(node_tree->prh, progress);
	BLI_snprintf(buf, sizeof(buf), IFACE_("Compositing"));
	node_tree->stats_draw(node_tree->sdh, buf);
	/* finish updating the stats */

}

void Device::end_task(ComputeTask *UNUSED(task), void *UNUSED(data)) {
}

void Device::init(Program *program, Compositor::Output *output) {
	this->program = program;
	this->output = output;
	this->num_tasks = 0;
}

void Device::add_tasks(std::list<Compositor::Device::ComputeTask *> &tasks) {
	for (auto task: tasks) {
		this->add_task(task);
	}
}

void Device::add_task(ComputeTask *task) {
	BLI_thread_queue_push(this->queue, task);
	this->num_tasks ++ ;
}

void Device::add_program_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *texture_ptr, MemoryPTR *viewport_ptr) {
	this->alloc_buffers_to_mem_manager(mem_manager, texture_ptr);
	this->alloc_viewports_to_mem_manager(mem_manager, viewport_ptr);
	mem_manager->upload_to_device();
}

void Device::alloc_buffers_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *ptr) {
	std::vector<Compositor::Buffer *> buffers = this->program->get_buffers();
	std::vector<Compositor::BufferUser *> buffer_users = this->program->get_buffer_users();
	ulong total_buffer_size = 0;
	float *buffer_data = NULL;
	std::cout << "Compositor::Device::num_buffers = " << buffers.size() << "\n";
	std::cout << "Compositor::Device::num_buffer_users = " << buffer_users.size() << "\n";
	
	// UPLOAD the data buffers
	for (auto buffer: buffers) {
		buffer_data = buffer->data;
		total_buffer_size = buffer->width * buffer->height * 4 * sizeof(float);
		mem_manager->alloc(total_buffer_size, buffer_data, &buffer->data_ptr, AF_NO_FLAGS);
	}

	TextureInfo *textures = (TextureInfo *)MEM_mallocN(sizeof(TextureInfo) * buffer_users.size(), "Compositor-TextureInfo");
	int index = 0;

	for (auto buffer_user: buffer_users) {
		Compositor::Buffer* buffer = buffer_user->buffer;
		switch (buffer->components) {
			case 4:
				textures[index].components = buffer->components;
				textures[index].width = buffer->width;
				textures[index].height = buffer->height;
				textures[index].data_ptr.index = buffer->data_ptr.index;
				textures[index].data_ptr.offset = buffer->data_ptr.offset;

				textures[index].interpolation = buffer_user->interpolation;
				textures[index].extension = buffer_user->extension;
				break;
		}

		index++;
	}

	mem_manager->alloc(buffer_users.size() * sizeof(TextureInfo), textures, ptr, AF_FREE_AFTER_UPLOAD_BLENDER_ALLOC);
}

void Device::alloc_viewports_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *ptr) {
	std::vector<Compositor::Viewport> viewports = this->program->get_viewports();
	std::cout << "Compositor::Device::num_viewports = " << viewports.size() << "\n";
	mem_manager->alloc(viewports.size() * sizeof(Compositor::Viewport), &viewports[0], ptr, AF_NO_FLAGS);
}


// scheduling
void Device::start() {
	this->queue  = BLI_thread_queue_init();
}
void Device::stop() {
	BLI_thread_queue_free(this->queue);
}
void Device::wait() {
	BLI_thread_queue_wait_finish(this->queue);
}

// FACTORY methods
Device *Device::create_device(Program *program, Compositor::Output *output, Compositor::RenderContext *render_context) {
	Device *device;
	if (render_context->use_open_cl) {
		device = new DeviceOpenCL();
	}
	else {
		device = new DeviceCPU();
	}

	device->init(program, output);
	return device;
}

void Device::destroy_device(Device *device) {
	delete device;
}
}
}
