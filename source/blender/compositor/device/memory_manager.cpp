#include "device/memory_manager.hpp"
#include <iostream>
#include <cassert>
#include <cstring>
#include "MEM_guardedalloc.h"

namespace Compositor {
namespace Device {

// Allocation
Allocation::Allocation(cl_ulong size, void *data, int flags) {
	this->size = size;
	this->data = data;
	this->flags = flags;
}
void Allocation::upload(void *target) {
	memcpy(target, this->data, this->size);
	if (this->flags & AF_FREE_AFTER_UPLOAD_BLENDER_ALLOC) {
		MEM_freeN(this->data);
		this->data = NULL;
	}
}

// BUFFER
Buffer::Buffer(int index, cl_ulong max_size) {
	this->index = index;
	this->max_size = max_size;
	this->buffer = NULL;
	this->data = NULL;
}

cl_ulong Buffer::allocated_size() {
	cl_ulong result = 0;
	for (auto allocation : this->allocations) {
		result += allocation->size;
	}
	return result;
}

cl_ulong Buffer::free_size() {
	return max_size - this->allocated_size();
}

bool Buffer::alloc(size_t size, void *data, MemoryPTR *ptr, int allocation_flags) {
	if (size < this->free_size()) {
		ptr->index = this->index;
		ptr->offset = this->allocated_size();
		this->allocations.push_back(new Allocation(size, data, allocation_flags));

		return true;
	}
	return false;
}

void Buffer::upload(FMMDeviceCallback *device_callback) {
	this->data = NULL;
	this->buffer = NULL;

	int alloc_size = this->allocated_size();
	if (allocations.size() != 0) {
		// combine all allocations to a single buffer
		// allocate a memory object to hold the buffer
		this->data = MEM_mallocN(alloc_size, "Compositor-flatmemorybuffer");
		cl_ulong offset = 0;
		for (auto allocation: allocations) {
			allocation->upload(((unsigned char *)this->data) + offset);
			offset += allocation->size;
		}
		this->buffer = device_callback->upload(this->data, alloc_size, this->index);
	}
}

void Buffer::release_buffer(FMMDeviceCallback *device_callback) {
	if (this->buffer) {
		device_callback->release_buffer(this->buffer);
		this->buffer = NULL;
	}

	if (this->data) {
		MEM_freeN(this->data);
		this->data = NULL;
	}
}

void Buffer::clear() {
	assert(this->buffer == NULL);
	assert(this->data == NULL);
	for (auto allocation: this->allocations) {
		delete allocation;
	}
	this->allocations.clear();
}

// MEMORYMANAGER
FlatMemoryManager::FlatMemoryManager(FMMDeviceCallback *device_callback) {
	this->device_callback = device_callback;
	uint num_buffers = device_callback->get_num_buffers();
	ulong max_size = device_callback->get_max_buffer_size();

	for (int i = 0; i < num_buffers; i++) {
		this->buffers.push_back(new Buffer(i, max_size));
	}
}
FlatMemoryManager::~FlatMemoryManager() {
	for (auto buffer : this->buffers) {
		delete(buffer);
	}
}

bool FlatMemoryManager::alloc(size_t size, void *data, MemoryPTR *ptr, int allocation_flags) {
	for (auto buffer: this->buffers) {
		if (buffer->alloc(size, data, ptr, allocation_flags)) {
			return true;
		}
	}
	// Could not allocate buffer on device.
	return false;
}

void FlatMemoryManager::upload_to_device() {
	for (auto buffer: this->buffers) {
		buffer->upload(this->device_callback);
	}
}

void FlatMemoryManager::release_buffers() {
	for (auto buffer: this->buffers) {
		buffer->release_buffer(this->device_callback);
	}
}

void FlatMemoryManager::clear() {
	for (auto buffer: this->buffers) {
		buffer->clear();
	}
}



}
}
