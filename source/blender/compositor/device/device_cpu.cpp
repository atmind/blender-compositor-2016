#include "device_cpu.hpp"
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <climits>

extern "C" {
	#include "BKE_node.h"
}

#include "kernel/kernels/cpu/kernels.h"
#include "util/util_system.h"

#define _KERNEL_CPU_


namespace Compositor {
namespace Device {
DeviceCPU::DeviceCPU() {
	this->mem_manager_cb = new FMMCPUCallback(this);
	this->mem_manager = new FlatMemoryManager(mem_manager_cb);
}

DeviceCPU::~DeviceCPU() {
	mem_manager->release_buffers();
	delete mem_manager;
	mem_manager = NULL;
	delete mem_manager_cb;
	mem_manager_cb = NULL;
}

void DeviceCPU::init(Program *program, Compositor::Output *output) {
	Device::init(program, output);
	if (system_cpu_support_avx2()) {
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu_avx2);
	}
	else if (system_cpu_support_avx()) {
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu_avx);
	}
	else if (system_cpu_support_sse41()) {
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu_sse41);
	}
	else if (system_cpu_support_sse3()) {
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu_sse3);
	}
	else if (system_cpu_support_sse2()) {
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu_sse2);
	}
	else {
		// fallback to x86
		this->compute_kernel_fp = &Compositor::KERNEL_NAME_EVAL(cpu);
	}
	this->kernel_globals._program = this->program->as_uint4_array();
	this->add_program_to_mem_manager(mem_manager, &this->kernel_globals.texture_ptr, &this->kernel_globals.viewport_ptr);
}

void DeviceCPU::start() {
	Device::start();
	uint num_workers = BLI_system_thread_count();
	BLI_init_threads(&this->threads, thread_execute, num_workers);
	for (uint i = 0; i < num_workers; i++) {
		BLI_insert_thread(&this->threads, this);
	}
}
void *DeviceCPU::thread_execute(void *data) {
	Device *device = (Device *)data;
	ComputeTask *task;
	while ((task = (ComputeTask *)BLI_thread_queue_pop(device->get_queue()))) {
		handle_single_task(device, task, data);
	}
	return NULL;
}
void DeviceCPU::stop() {
	BLI_thread_queue_nowait(this->get_queue());
	BLI_end_threads(&this->threads);
	Device::stop();
}

void DeviceCPU::execute_task(ComputeTask *task, void *UNUSED(data)) {
	if (task->is_cancelled()) { return; }

	Compositor::Output *output = task->output;

	Compositor::KernelGlobals kg;
	kg._program = this->kernel_globals._program;
	kg.buffers[0] = this->kernel_globals.buffers[0];
	kg.texture_ptr.index = this->kernel_globals.texture_ptr.index;
	kg.texture_ptr.offset = this->kernel_globals.texture_ptr.offset;
	kg.viewport_ptr.index = this->kernel_globals.viewport_ptr.index;
	kg.viewport_ptr.offset = this->kernel_globals.viewport_ptr.offset;
	kg.num_samples = task->num_samples;
	kg.aspect_ratio = output->width / float(output->height);
	kg.seed = 1234567 + task->y_min + task->x_max;
	// TODO: kg.frame_number = task
	kg.pixel_size = make_float2(
		(1.0/output->width),
		(1.0/output->height)
	);
	auto compute_kernel = this->compute_kernel_fp;

	int offset = (task->y_min * output->width + task->x_min) * 4;
	for (int y = task->y_min; y < task->y_max; y++) {
		float ry = y / (float)output->height;
		for (int x = task->x_min; x < task->x_max; x++) {
			float rx = x / (float)output->width;
			
			
			compute_kernel(kg, rx, ry, &output->buffer[offset]);
			offset += 4;
		}
		offset += (output->width - (task->x_max - task->x_min)) * 4;
	}
}
void DeviceCPU::task_finished(ComputeTask *task, void *UNUSED(data)) {
	task->output->update_subimage(task->x_min, task->y_min, task->x_max, task->y_max);
}

// FMMCPUCallback
FMMCPUCallback::FMMCPUCallback(DeviceCPU *device) {
	this->device = device;
}

int FMMCPUCallback::get_num_buffers() {
	return 1;
}
ulong FMMCPUCallback::get_max_buffer_size() {
	return LLONG_MAX;
}

void *FMMCPUCallback::upload(void *data, ulong UNUSED(size), int buffer_index) {
	device->kernel_globals.buffers[buffer_index] = (char *)data;
	return NULL;
}

void FMMCPUCallback::release_buffer(void *UNUSED(buffer)) {
}

}
}
