namespace Compositor {
namespace Device {
class Device;
}
}

#ifndef CMP_DEVICE_DEVICE_HPP
#define CMP_DEVICE_DEVICE_HPP

#include "device_task.hpp"
#include "cmp_node.hpp"
#include "cmp_compiler.hpp"
#include "cmp_output.hpp"

#include <list>

#include "DNA_listBase.h"
extern "C" {
#include "BLI_threads.h"
}
#include "device/memory_manager.hpp"


namespace Compositor {
namespace Device {

class Device {
private:
	ThreadQueue *queue;
	int num_tasks;
	void alloc_buffers_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *ptr);
	void alloc_viewports_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *ptr);

protected:

	Program *program;
	Compositor::Output *output;
	static void handle_single_task(Device *device, ComputeTask *task, void *data);
	void add_program_to_mem_manager(FlatMemoryManager *mem_manager, MemoryPTR *texture_ptr, MemoryPTR *viewport_ptr);

public:
	virtual ~Device() {
	}
	ThreadQueue *get_queue();
	virtual void init(Program *program, Compositor::Output *output);

	/**
	 * Add all tasks to the queue.
	 */
	void add_tasks(std::list<Compositor::Device::ComputeTask *> &tasks);

	/**
	 * Add a single task to the queue. This is an internal method.
	 */
	void add_task(ComputeTask *task);

	virtual void start_task(ComputeTask *task, void *data);
	virtual void execute_task(ComputeTask *task, void *data) = 0;
	virtual void end_task(ComputeTask *task, void *data);
	virtual void task_finished(ComputeTask *task, void *data) = 0;

	virtual void start();
	virtual void stop();
	virtual void wait();

	/**
	 * Create a device that is capable to calculate the given node(tree).
	 */
	static Device *create_device(Program *program, Compositor::Output *output, Compositor::RenderContext *render_context);
	static void destroy_device(Device *device);
};
}
}
#endif
