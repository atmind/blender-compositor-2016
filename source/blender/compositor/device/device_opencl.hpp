#ifndef CMP_DEVICE_DEVICE_OPENCL_HPP
#define CMP_DEVICE_DEVICE_OPENCL_HPP

#include "device.hpp"
#include "clew.h"
#include <sstream>

namespace Compositor {
namespace Device {
class DeviceOpenCL : public Device {
private:
	/**
	 * list of worker threads
	 */
	ListBase threads;

	/**
	 * Main loop of the worker thread.
	 */
	static void *thread_execute(void *data);

	static void load_source(std::stringstream &shader_source, std::string path);

public:
	static void init_opencl();
	static void deinit_opencl();
	void end_task(ComputeTask *task, void *data);

	/**
	 * Initialize the device.
	 */
	void init(Program *program, Compositor::Output *output);

	/**
	 * Execute a task.
	 */
	void execute_task(ComputeTask *task, void *data);

	/**
	 * When a task is finished this is called to store the result
	 * in Blender's imagespace.
	 */
	void task_finished(ComputeTask *task, void *data);

	/**
	 * Start executing the ComputeTasks on the device
	 */
	void start();

	/**
	 * Stop executing the ComputeTasks on the device.
	 */
	void stop();
};

struct OpenCLPlatform;
struct OpenCLPlatformDevice {
	OpenCLPlatform *platform;

	cl_device_id device_id;
	cl_command_queue command_queue;
	FMMDeviceCallback *mem_manager_cb;
	FlatMemoryManager *mem_manager;
	cl_context context;
	cl_program program;
	cl_kernel kernel;
	DeviceOpenCL *device;

	/**
	 * Buffer where the result is stored
	 */
	cl_mem result_buffer;
	#ifdef OPENCL_REUSE_RESULT_BUFFER
	// store the resolution of the last allocated g_result.
	uint2 result_resolution;
	#endif
	cl_mem composite_program;
	HostKernelGlobals kernel_globals;

	OpenCLPlatformDevice(OpenCLPlatform* platform, cl_device_id device_id, const char ** sourcecode);
	~OpenCLPlatformDevice();
	void build_context();
	void build_program(const char **sourcecode);
	void build_kernel();
	void build_command_queue();
	void init_result_buffer(Output *output);
	void init_program(Program *program);
	void init_kernel_globals(Output *output);
};

class FMMOpenCLCallback : public FMMDeviceCallback {
private:
	OpenCLPlatformDevice *device;

public:
	FMMOpenCLCallback(OpenCLPlatformDevice *device);
	virtual int get_num_buffers();
	virtual ulong get_max_buffer_size();
	virtual void *upload(void *data, ulong size, int buffer_index);
	virtual void release_buffer(void *buffer);
};
}
}
#endif
