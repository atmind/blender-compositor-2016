namespace Compositor {
namespace Device {
struct ComputeTask;
}
}
#ifndef CMP_DEVICE_DEVICE_TASK_HPP
#define CMP_DEVICE_DEVICE_TASK_HPP

#include "device.hpp"
#include "cmp_output.hpp"
#include "BLI_math.h"
#include <ostream>

namespace Compositor {
namespace Device {
enum compute_task_types {START, EXECUTE, END};
struct ComputeTask {
	Compositor::Node *node;
	unsigned int x_min;
	unsigned int y_min;
	unsigned int x_max;
	unsigned int y_max;
	Compositor::Output *output;
	compute_task_types type;

	int num_samples;

	ComputeTask(Compositor::Node *node, int x_min, int y_min, int x_max, int y_max, Compositor::Output *output);
	ComputeTask(Compositor::Output *output, compute_task_types type);
	ComputeTask(compute_task_types type);

	bool is_cancelled() {
		bNodeTree *node_tree = this->output->node_tree;
		return (node_tree->test_break(node_tree->tbh));
	}

	/**
	 * @brief Retrieve the current xy subsamples.
	 */
	int get_current_xy_subsamples() {
		return this->num_samples;
	}
	friend std::ostream& operator<<(std::ostream& stream, const ComputeTask& task);
};
}
}
#endif
