#ifndef CMP_DEVICE_DEVICE_CPU_HPP
#define CMP_DEVICE_DEVICE_CPU_HPP

#include "device.hpp"
#include "kernel/kernels/cpu/kernels.h"
#include <functional>

namespace Compositor {
namespace Device {
// Forward declarations
class DeviceCPU;

class FMMCPUCallback : public FMMDeviceCallback {
private:
	DeviceCPU *device;
public:
	FMMCPUCallback(DeviceCPU *device);
	virtual int get_num_buffers();
	virtual ulong get_max_buffer_size();
	virtual void *upload(void *data, ulong size, int buffer_index);
	virtual void release_buffer(void *buffer);
};

class DeviceCPU : public Device {
private:
	/**
	 * list of worker threads
	 */
	ListBase threads;

	/**
	 * Function pointer to the (optimized) compute kernel that has been
	 * selected for the CPU of the user.
	 */
	std::function<void(KernelGlobals &kg, float x, float y, float *output)> compute_kernel_fp;

	/**
	 * Main loop of the worker thread.
	 */
	static void *thread_execute(void *data);

	FMMCPUCallback *mem_manager_cb;
	FlatMemoryManager *mem_manager;

public:
	KernelGlobals kernel_globals;
	DeviceCPU();
	virtual ~DeviceCPU();
	/**
	 * Initialize the device.
	 */
	void init(Program *program, Compositor::Output *output);

	/**
	 * Execute a task.
	 */
	void execute_task(ComputeTask *task, void *data);

	/**
	 * When a task is finished this is called to store the result
	 * in Blender's imagespace.
	 */
	void task_finished(ComputeTask *task, void *data);

	/**
	 * Start executing the ComputeTasks on the device
	 */
	void start();

	/**
	 * Stop executing the ComputeTasks on the device.
	 */
	void stop();
};
}
}
#endif
