/*
 * ***** BEGIN GPL LICENSE BLOCK *****
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software  Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Contributor(s): Jeroen Bakker, Monique Dewanchand
 *
 * ***** END GPL LICENSE BLOCK *****
 *
 */

/** \file DNA_viewport_types.h
 *  \ingroup DNA
 */

#ifndef __DNA_VIEWPORT_TYPES_H__
#define __DNA_VIEWPORT_TYPES_H__

#include "DNA_defs.h"
#include "DNA_listBase.h"
#include "DNA_ID.h"

#ifdef __cplusplus
extern "C" {
#endif

struct Object;
struct AnimData;

typedef struct Viewport {
	ID id;
	struct AnimData *adt;		/* animation data (must be immediately after id for utilities to use it) */

	float radius; // Spherical

	char type;

	char padding[3];
} Viewport;

/* Viewport->type */
enum {
	VIEWPORT_TYPE_PLANE  = 0,
	VIEWPORT_TYPE_SPHERE = 1,
};


#ifdef __cplusplus
}
#endif

#endif /* __DNA_VIEWPORT_TYPES_H__ */
